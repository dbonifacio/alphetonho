<%
'BindEvents Method @1-F7C8BF35
Sub BindEvents(Level)
    If Level="Page" Then
        Set CCSEvents("BeforeInitialize") = GetRef("Page_BeforeInitialize")
    Else
        Set Grid2.CCSEvents("BeforeShow") = GetRef("Grid2_BeforeShow")
        Set price_change_status.CCSEvents("BeforeShow") = GetRef("price_change_status_BeforeShow")
        Set price_send.CCSEvents("BeforeShow") = GetRef("price_send_BeforeShow")
        Set price_send.CCSEvents("BeforeShowRow") = GetRef("price_send_BeforeShowRow")
        Set Grid3.CCSEvents("BeforeShow") = GetRef("Grid3_BeforeShow")
        Set Search_Sites.s_SiteName.CCSEvents("BeforeShow") = GetRef("Search_Sites_s_SiteName_BeforeShow")
    End If
End Sub
'End BindEvents Method

Function Grid2_BeforeShow(Sender) 'Grid2_BeforeShow @39-23BBE975

'Custom Code @149-73254650
' -------------------------
		Grid2.Visible = false
		If(CCGetFromGet("site","") = "1") Then
 			Grid2.Visible = true
		end if
' -------------------------
'End Custom Code

End Function 'Close Grid2_BeforeShow @39-54C34B28
dim RowNumber
Function price_change_status_BeforeShow(Sender) 'price_change_status_BeforeShow @64-B2A12D48

'Custom Code @121-73254650
' -------------------------
dim FusionHO
Dim SQL
Dim rsGroups
Dim strGroups
dim id
dim strXML

	  	If price_change_status.Recordset.EOF Then
			price_change_status.Visible = False
 		End if

		If(CCGetFromGet("var","") = "1") Then
 			price_change_status.Visible = true
		end if

		rsGroups = ""
		strXML = ""
		id = price_change_status.price_change_id.Value 
		Set FusionHO = Server.CreateObject("ADODB.Connection") 
		FusionHO.open "PROVIDER=SQLNCLI10;DATA SOURCE=MICROSTR-0A74B0;UID=sa;PWD=serbiznet;DATABASE=FusionHO " 
		SQL = "select status_code from price_change_status where price_change_id = '"& id &"'"
 		Set rsGroups = FusionHO.Execute(SQL)
		While not rsGroups.EOF
			strXML= strXML & rsGroups("status_code") 
			rsGroups.MoveNext
		Wend
		if strXML = "1" then
			
			price_change_status.Button_Update.visible = false
			price_change_status.Button_delete.visible = false
		end if
' -------------------------
'End Custom Code

End Function 'Close price_change_status_BeforeShow @64-54C34B28

Function price_send_BeforeShow(Sender) 'price_send_BeforeShow @76-0FF5D4A2

'Custom Code @120-73254650
' -------------------------
dim FusionHO
Dim SQL
Dim rsGroups
Dim strGroups
dim id
dim strXML
  	If price_change_status.Recordset.EOF Then
		price_send.Visible = False
	else
		rsGroups = ""
		strXML = ""
		id = price_change_status.price_change_id.Value 
		Set FusionHO = Server.CreateObject("ADODB.Connection") 
		FusionHO.open "PROVIDER=SQLNCLI10;DATA SOURCE=MICROSTR-0A74B0;UID=sa;PWD=serbiznet;DATABASE=FusionHO " 
		SQL = "select status_code from price_change_status where price_change_id = '"& id &"'"
 		Set rsGroups = FusionHO.Execute(SQL)
		While not rsGroups.EOF
			strXML= strXML & rsGroups("status_code") 
			rsGroups.MoveNext
		Wend
		if strXML = "1" then
			price_send.Visible = False
		else
			price_send.Visible = true
		end if
 	End if

' -------------------------
'End Custom Code

End Function 'Close price_send_BeforeShow @76-54C34B28

Function price_send_BeforeShowRow(Sender) 'price_send_BeforeShowRow @76-BEB574AB

'Custom Code @129-73254650
' -------------------------
  RowNumber = RowNumber + 1
  price_send.RowIDAttribute.Value = RowNumber
' -------------------------
'End Custom Code

End Function 'Close price_send_BeforeShowRow @76-54C34B28

Function Grid3_BeforeShow(Sender) 'Grid3_BeforeShow @100-46DCD233

'Custom Code @119-73254650
' -------------------------
dim FusionHO
Dim SQL
Dim rsGroups
Dim strGroups
dim id
dim strXML
  	If price_change_status.Recordset.EOF Then
		Grid3.Visible = False
	else
		rsGroups = ""
		strXML = ""
		id = price_change_status.price_change_id.Value 
		Set FusionHO = Server.CreateObject("ADODB.Connection") 
		FusionHO.open "PROVIDER=SQLNCLI10;DATA SOURCE=MICROSTR-0A74B0;UID=sa;PWD=serbiznet;DATABASE=FusionHO " 
		SQL = "select status_code from price_change_status where price_change_id = '"& id &"'"
 		Set rsGroups = FusionHO.Execute(SQL)
		While not rsGroups.EOF
			strXML= strXML & rsGroups("status_code") 
			rsGroups.MoveNext
		Wend
		if strXML = "1" then
			Grid3.Visible = True
			price_change_status.Button_Update.visible = false
			price_change_status.Button_delete.visible = false

		else
			Grid3.Visible = False
		end if
 	End if
' -------------------------
'End Custom Code

End Function 'Close Grid3_BeforeShow @100-54C34B28

Function Search_Sites_s_SiteName_BeforeShow(Sender) 'Search_Sites_s_SiteName_BeforeShow @152-953B4CE0

'PTAutocomplete1 BeforeShow @161-AF4E12E4
    Sender.Attributes("id") = "Search_Sitess_SiteName"
'End PTAutocomplete1 BeforeShow

End Function 'Close Search_Sites_s_SiteName_BeforeShow @152-54C34B28

Function Page_BeforeInitialize(Sender) 'Page_BeforeInitialize @1-A73C9435

'PTAutocomplete1 ServiceCall @161-E5949120
    If CCGetFromGet("callbackControl", Empty) = "Search_Sitess_SiteNamePTAutocomplete1" Then _
        ServiceSearch_Sitess_SiteNamePTAutocomplete1
'End PTAutocomplete1 ServiceCall

End Function 'Close Page_BeforeInitialize @1-54C34B28

Function ServiceSearch_Sitess_SiteNamePTAutocomplete1() 'Function ServiceSearch_Sitess_SiteNamePTAutocomplete1 @161-EE52CB42

'PTAutocomplete1 Initialization @161-C9CF6B18
    Dim Service,  DBServiceFusionHO, ServiceDS, Fields, AdditionalWhere
    Set DBServiceFusionHO = New clsDBFusionHO
'End PTAutocomplete1 Initialization

'PTAutocomplete1 DataSource @161-D98F2761
    Set ServiceDS = CCCreateDataSource(dsTable,DBServiceFusionHO, Array("SELECT *  " & _
"FROM Sites {SQL_Where} {SQL_OrderBy}", "", ""))
    With ServiceDS.WhereParameters
        Set .ParameterSources = Server.CreateObject("Scripting.Dictionary")
        .ParameterSources("posts_SiteName") = CCGetRequestParam("s_SiteName", ccsPOST)
        .AddParameter 1, "posts_SiteName", ccsText, Empty, Empty, -1, False
        .Criterion(1) = .Operation(opBeginsWith, False, "SiteName", .getParamByID(1))
        .AssembledWhere = .Criterion(1)
    End With
    ServiceDS.Where = ServiceDS.WhereParameters.AssembledWhere
    Set Service = CCCreateService("PTAutocomplete", "Search_Sitess_SiteNamePTAutocomplete1", ServiceDS,Nothing, Nothing)
'End PTAutocomplete1 DataSource

'PTAutocomplete1 DataFields @161-5612690B
    Service.AddDatabaseField("SiteName")
'End PTAutocomplete1 DataFields

'PTAutocomplete1 Execution @161-0FED32A8
    Service.OutputFormatter = New clsListFormatter
    Response.Write  Service.Execute
    Set Service  = Nothing
    Response.End
'End PTAutocomplete1 Execution

End Function 'End Function ServiceSearch_Sitess_SiteNamePTAutocomplete1 @161-54C34B28


%>
