<%
'BindEvents Method @1-DABF2F44
Sub BindEvents(Level)
    If Level="Page" Then
    Else
        Set City.Navigator.CCSEvents("BeforeShow") = GetRef("City_Navigator_BeforeShow")
        Set City1.CCSEvents("BeforeShow") = GetRef("City1_BeforeShow")
    End If
End Sub
'End BindEvents Method

Function City_Navigator_BeforeShow(Sender) 'City_Navigator_BeforeShow @22-B74F2CC3

'Hide-Show Component @23-AEAEEAD2
    Dim TotalPages_23_1 : TotalPages_23_1 = CCSConverter.VBSConvert(ccsInteger, City.DataSource.Recordset.PageCount)
    Dim Param2_23_2 : Param2_23_2 = CCSConverter.VBSConvert(ccsInteger, 2)
    If  (Not IsEmpty(TotalPages_23_1) And Not IsEmpty(Param2_23_2) And TotalPages_23_1 < Param2_23_2) Then _
        City.Navigator.Visible = False
'End Hide-Show Component

End Function 'Close City_Navigator_BeforeShow @22-54C34B28

Function City1_BeforeShow(Sender) 'City1_BeforeShow @28-2A32C189

'Custom Code @37-73254650
' -------------------------
  	If City1.Recordset.EOF Then
		City1.Visible = False
 	End if

 	If(CCGetFromGet("var","") = "1") Then
 		City1.Visible = true
	end if
' -------------------------
'End Custom Code

End Function 'Close City1_BeforeShow @28-54C34B28


%>
