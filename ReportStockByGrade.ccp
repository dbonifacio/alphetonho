<Page id="1" templateExtension="html" relativePath="." fullRelativePath="." secured="False" urlType="Relative" isIncluded="False" SSLAccess="False" isService="False" cachingEnabled="False" cachingDuration="1 minutes" wizardTheme="FusionHO1" wizardThemeVersion="3.0" pasteActions="pasteActions" needGeneration="0">
	<Components>
		<IncludePage id="2" name="Header" PathID="Header" page="Header.ccp">
			<Components/>
			<Events/>
			<Features/>
		</IncludePage>
		<Report id="3" secured="False" enablePrint="True" showMode="Web" sourceType="SQL" returnValueType="Number" linesPerWebPage="40" linesPerPhysicalPage="50" connection="FusionHO" dataSource="select Site_Name, Grade, Tank_Time as Hour, sum(Volume) as Grade_Volume, sum(Water_Volume) as Water_Volume from Grade_table 
where tank_DATE ='{s_DAY_DATE}' and Grade like '%{s_Grade_Name}%' and Site_Name like '%{s_Site_Name}%' and tank_time &gt;= '{s_DAY_TIME}' and tank_time &lt;= '{s_DAY_TIME1}' 
group by site_Name, Grade, tank_time
		" name="Report1" orderBy="Hour" pageSizeLimit="100" wizardCaption="{res:CCS_ReportFormPrefix} {res:Report1} {res:CCS_ReportFormSuffix}" wizardLayoutType="GroupLeftAbove" activeCollection="SQLParameters" parameterTypeListName="ParameterTypeList">
			<Components>
				<Section id="7" visible="True" lines="0" name="Report_Header" wizardSectionType="ReportHeader">
					<Components>
						<ReportLabel id="19" fieldSourceType="DBColumn" dataType="Text" html="False" hideDuplicates="False" resetAt="Report" name="Report_TotalRecords" function="Count" wizardUseTemplateBlock="False" PathID="Report1Report_HeaderReport_TotalRecords">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</ReportLabel>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Section>
				<Section id="8" visible="True" lines="1" name="Page_Header" wizardSectionType="PageHeader">
					<Components>
						<Sorter id="26" visible="True" name="Sorter_Hour" column="Hour" wizardCaption="{res:Hour}" wizardSortingType="SimpleDir" wizardControl="Hour">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
						<Sorter id="28" visible="True" name="Sorter_Grade_Volume" column="Grade_Volume" wizardCaption="{res:Grade_Volume}" wizardSortingType="SimpleDir" wizardControl="Grade_Volume">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
						<Sorter id="30" visible="True" name="Sorter_Water_Volume" column="Water_Volume" wizardCaption="{res:Water_Volume}" wizardSortingType="SimpleDir" wizardControl="Water_Volume">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Sorter>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Section>
				<Section id="10" visible="True" lines="1" name="Site_Name_Header">
					<Components>
						<ReportLabel id="20" fieldSourceType="DBColumn" dataType="Text" html="False" hideDuplicates="False" resetAt="Report" name="Site_Name" fieldSource="Site_Name" wizardCaption="Site_Name" wizardSize="30" wizardMaxLength="30" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" PathID="Report1Site_Name_HeaderSite_Name">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</ReportLabel>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Section>
				<Section id="12" visible="True" lines="1" name="Grade_Header">
					<Components>
						<ReportLabel id="21" fieldSourceType="DBColumn" dataType="Text" html="False" hideDuplicates="False" resetAt="Report" name="Grade" fieldSource="Grade" wizardCaption="Grade" wizardSize="50" wizardMaxLength="50" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" PathID="Report1Grade_HeaderGrade">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</ReportLabel>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Section>
				<Section id="13" visible="True" lines="1" name="Detail">
					<Components>
						<ReportLabel id="27" fieldSourceType="DBColumn" dataType="Integer" html="False" hideDuplicates="False" resetAt="Report" name="Hour1" fieldSource="Hour" wizardCaption="Hour" wizardSize="5" wizardMaxLength="5" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" wizardAlign="right" PathID="Report1DetailHour1">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<ReportLabel id="29" fieldSourceType="DBColumn" dataType="Float" html="False" hideDuplicates="False" resetAt="Report" name="Grade_Volume" fieldSource="Grade_Volume" wizardCaption="Grade_Volume" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" wizardAlign="right" PathID="Report1DetailGrade_Volume" format="#,##0.00">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<ReportLabel id="31" fieldSourceType="DBColumn" dataType="Float" html="False" hideDuplicates="False" resetAt="Report" name="Water_Volume" fieldSource="Water_Volume" wizardCaption="Water_Volume" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="False" wizardAlign="right" PathID="Report1DetailWater_Volume" format="#,##0.00">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</ReportLabel>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Section>
				<Section id="14" visible="True" lines="0" name="Grade_Footer">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Section>
				<Section id="15" visible="True" lines="0" name="Site_Name_Footer">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Section>
				<Section id="16" visible="True" lines="0" name="Report_Footer" wizardSectionType="ReportFooter">
					<Components>
						<Panel id="17" visible="True" name="NoRecords" wizardNoRecords="{res:CCS_NoRecords}">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Panel>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Section>
				<Section id="18" visible="True" lines="2" name="Page_Footer" wizardSectionType="PageFooter" pageBreakAfter="True">
					<Components>
						<Panel id="22" visible="True" name="PageBreak">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</Panel>
						<ReportLabel id="23" fieldSourceType="SpecialValue" dataType="Date" html="False" hideDuplicates="False" resetAt="Report" name="Report_CurrentDate" fieldSource="CurrentDate" wizardUseTemplateBlock="False" wizardAddNbsp="False" wizardInsertToDateTD="True" PathID="Report1Page_FooterReport_CurrentDate">
							<Components/>
							<Events/>
							<Attributes/>
							<Features/>
						</ReportLabel>
						<Navigator id="24" size="10" type="Centered" pageSizes="1;5;10;25;50" name="Navigator" wizardPagingType="Centered" wizardFirst="True" wizardFirstText="{res:CCS_First}" wizardPrev="True" wizardPrevText="{res:CCS_Previous}" wizardNext="True" wizardNextText="{res:CCS_Next}" wizardLast="True" wizardLastText="{res:CCS_Last}" wizardPageNumbers="Centered" wizardSize="10" wizardTotalPages="True" wizardHideDisabled="False" wizardOfText="{res:CCS_Of}" wizardImagesScheme="Fusionho1">
							<Components/>
							<Events>
								<Event name="BeforeShow" type="Server">
									<Actions>
										<Action actionName="Hide-Show Component" actionCategory="General" id="25" action="Hide" conditionType="Parameter" dataType="Integer" condition="LessThan" name1="TotalPages" sourceType1="SpecialValue" name2="2" sourceType2="Expression" eventType="Server"/>
									</Actions>
								</Event>
							</Events>
							<Attributes/>
							<Features/>
						</Navigator>
					</Components>
					<Events/>
					<Attributes/>
					<Features/>
				</Section>
			</Components>
			<Events/>
			<TableParameters/>
			<JoinTables/>
			<JoinLinks/>
			<Fields/>
			<SPParameters/>
			<SQLParameters>
				<SQLParameter id="43" variable="s_DAY_DATE" parameterType="Expression" defaultValue="date()" dataType="Date" DBFormat="mm/dd/yyyy" format="mm/dd/yyyy" parameterSource="Report2.s_DAY_DATE.value"/>
				<SQLParameter id="44" variable="s_DAY_TIME" parameterType="URL" defaultValue="0" dataType="Integer" parameterSource="s_DAY_TIME"/>
				<SQLParameter id="45" variable="s_DAY_TIME1" parameterType="URL" defaultValue="23" dataType="Integer" parameterSource="s_DAY_TIME1"/>
				<SQLParameter id="46" variable="s_Site_Name" parameterType="URL" dataType="Text" parameterSource="s_Site_Name"/>
				<SQLParameter id="47" variable="s_Grade_Name" parameterType="URL" dataType="Text" parameterSource="s_Grade_Name"/>
			</SQLParameters>
			<ReportGroups>
				<ReportGroup id="9" name="Site_Name" field="Site_Name" sqlField="Site_Name" sortOrder="asc"/>
				<ReportGroup id="11" name="Grade" field="Grade" sqlField="Grade" sortOrder="asc"/>
			</ReportGroups>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</Report>
		<Link id="4" visible="Dynamic" fieldSourceType="DBColumn" dataType="Text" html="False" hrefType="Page" urlType="Relative" preserveParameters="GET" name="Report_Print" hrefSource="ReportStockByGrade.ccp" wizardTheme="Fusionho1" wizardThemeType="File" wizardDefaultValue="{res:CCS_ReportPrintLink}" wizardUseTemplateBlock="True" wizardBeforeHTML="&lt;p align=&quot;right&quot;&gt;" wizardAfterHTML="&lt;/p&gt;" wizardLinkTarget="_blank" PathID="Report_Print">
			<Components/>
			<Events>
				<Event name="BeforeShow" type="Server">
					<Actions>
						<Action actionName="Hide-Show Component" actionCategory="General" id="6" action="Hide" conditionType="Parameter" dataType="Text" condition="Equal" parameter1="Print" name1="ViewMode" sourceType1="URL" name2="&quot;Print&quot;" sourceType2="Expression"/>
					</Actions>
				</Event>
			</Events>
			<LinkParameters>
				<LinkParameter id="5" sourceType="Expression" format="yyyy-mm-dd" name="ViewMode" source="&quot;Print&quot;"/>
			</LinkParameters>
			<Attributes/>
			<Features/>
		</Link>
		<Record id="32" sourceType="Table" urlType="Relative" secured="False" allowInsert="False" allowUpdate="False" allowDelete="False" validateData="True" preserveParameters="None" returnValueType="Number" returnValueTypeForDelete="Number" returnValueTypeForInsert="Number" returnValueTypeForUpdate="Number" name="Report2" wizardCaption="{res:CCS_SearchFormPrefix} {res:Report1} {res:CCS_SearchFormSuffix}" wizardOrientation="Vertical" wizardFormMethod="post" returnPage="ReportStockByGrade.ccp" PathID="Report2" pasteActions="pasteActions">
			<Components>
				<Button id="33" urlType="Relative" enableValidation="True" isDefault="False" name="Button_DoSearch" operation="Search" wizardCaption="{res:CCS_Search}" wizardThemeItem="FooterIMG" wizardButtonImage="ButtonSearchOn" PathID="Report2Button_DoSearch">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Button>
				<TextBox id="34" visible="Yes" fieldSourceType="DBColumn" dataType="Date" name="s_DAY_DATE" wizardCaption="{res:DAY_DATE}" wizardSize="8" wizardMaxLength="100" wizardIsPassword="False" PathID="Report2s_DAY_DATE" format="mm/dd/yyyy">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
				<DatePicker id="35" name="DatePicker_s_DAY_DATE" control="s_DAY_DATE" wizardSatellite="True" wizardControl="s_DAY_DATE" wizardDatePickerType="Image" wizardPicture="Styles/FusionHO1/Images/DatePicker.gif" style="Styles/FusionHO1/Style.css" PathID="Report2DatePicker_s_DAY_DATE">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</DatePicker>
				<ListBox id="36" visible="Yes" fieldSourceType="DBColumn" dataType="Integer" name="s_DAY_TIME" wizardCaption="{res:DAY_TIME}" wizardSize="5" wizardMaxLength="5" wizardIsPassword="False" PathID="Report2s_DAY_TIME" sourceType="ListOfValues" dataSource="0;0:00Hs;1;1:00 Hs;2;2:00 Hs;3;3:00 Hs;4;4:00 Hs;5;5:00 Hs;6;6:00 Hs;7;7:00 Hs;8;8:00 Hs;9;9:00 Hs;10;10:00 Hs;11;11:00 Hs;12;12:00 Hs;13;13:00 Hs;14;14:00 Hs;15;15:00 Hs;16;16:00 Hs;17;17:00 Hs;18;18:00 Hs;19;19:00 Hs;20;20:00 Hs;21;21:00 Hs;22;22:00 Hs;23;23:00 Hs">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables/>
					<JoinLinks/>
					<Fields/>
				</ListBox>
				<ListBox id="37" visible="Yes" fieldSourceType="DBColumn" sourceType="Table" dataType="Text" returnValueType="Number" name="s_Grade_Name" wizardEmptyCaption="{res:CCS_SelectValue}" PathID="Report2s_Grade_Name" connection="FusionHO" dataSource="Grades" boundColumn="grade_name" textColumn="grade_name">
					<Components/>
					<Events/>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables>
						<JoinTable id="38" tableName="Grades" schemaName="dbo" posLeft="10" posTop="10" posWidth="95" posHeight="104"/>
					</JoinTables>
					<JoinLinks/>
					<Fields/>
					<Attributes/>
					<Features/>
				</ListBox>
				<ListBox id="39" visible="Yes" fieldSourceType="DBColumn" sourceType="Table" dataType="Text" returnValueType="Number" name="s_Site_Name" wizardEmptyCaption="{res:CCS_SelectValue}" PathID="Report2s_Site_Name" connection="FusionHO" dataSource="Sites" boundColumn="SiteName" textColumn="SiteName">
					<Components/>
					<Events/>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables>
						<JoinTable id="40" tableName="Sites" schemaName="dbo" posLeft="10" posTop="10" posWidth="115" posHeight="180"/>
					</JoinTables>
					<JoinLinks/>
					<Fields/>
					<Attributes/>
					<Features/>
				</ListBox>
				<ListBox id="41" visible="Yes" fieldSourceType="DBColumn" sourceType="ListOfValues" dataType="Text" returnValueType="Number" name="s_DAY_TIME1" wizardEmptyCaption="{res:CCS_SelectValue}" PathID="Report2s_DAY_TIME1" dataSource="0;0:00Hs;1;1:00 Hs;2;2:00 Hs;3;3:00 Hs;4;4:00 Hs;5;5:00 Hs;6;6:00 Hs;7;7:00 Hs;8;8:00 Hs;9;9:00 Hs;10;10:00 Hs;11;11:00 Hs;12;12:00 Hs;13;13:00 Hs;14;14:00 Hs;15;15:00 Hs;16;16:00 Hs;17;17:00 Hs;18;18:00 Hs;19;19:00 Hs;20;20:00 Hs;21;21:00 Hs;22;22:00 Hs;23;23:00 Hs">
					<Components/>
					<Events/>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables/>
					<JoinLinks/>
					<Fields/>
					<Attributes/>
					<Features/>
				</ListBox>
				<Link id="42" visible="Yes" fieldSourceType="DBColumn" dataType="Text" html="False" hrefType="Page" urlType="Relative" preserveParameters="GET" name="ClearParameters" hrefSource="ReportStockByGrade.ccp" removeParameters="s_DAY_DATE;s_DAY_TIME;s_DAY_TIME1;s_Site_Name;s_Grade_Name" wizardThemeItem="SorterLink" wizardDefaultValue="{res:CCS_Clear}" PathID="Report2ClearParameters" wizardUseTemplateBlock="False">
					<Components/>
					<Events/>
					<LinkParameters/>
					<Attributes/>
					<Features/>
				</Link>
			</Components>
			<Events/>
			<TableParameters/>
			<SPParameters/>
			<SQLParameters/>
			<JoinTables/>
			<JoinLinks/>
			<Fields/>
			<ISPParameters/>
			<ISQLParameters/>
			<IFormElements/>
			<USPParameters/>
			<USQLParameters/>
			<UConditions/>
			<UFormElements/>
			<DSPParameters/>
			<DSQLParameters/>
			<DConditions/>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</Record>
	</Components>
	<CodeFiles>
		<CodeFile id="Events" language="ASPTemplates" name="ReportStockByGrade_events.asp" forShow="False" comment="'" codePage="windows-1252"/>
		<CodeFile id="Code" language="ASPTemplates" name="ReportStockByGrade.asp" forShow="True" url="ReportStockByGrade.asp" comment="'" codePage="windows-1252"/>
	</CodeFiles>
	<SecurityGroups/>
	<CachingParameters/>
	<Attributes/>
	<Features/>
	<Events/>
</Page>
