<%
'BindEvents Method @1-72A425FB
Sub BindEvents(Level)
    If Level="Page" Then
        Set CCSEvents("BeforeInitialize") = GetRef("Page_BeforeInitialize")
    Else
        Set Sites.Navigator.CCSEvents("BeforeShow") = GetRef("Sites_Navigator_BeforeShow")
        Set Sites1.City_id.CCSEvents("BeforeShow") = GetRef("Sites1_City_id_BeforeShow")
        Set Sites1.Region_id.CCSEvents("BeforeShow") = GetRef("Sites1_Region_id_BeforeShow")
        Set Sites1.CCSEvents("BeforeShow") = GetRef("Sites1_BeforeShow")
    End If
End Sub
'End BindEvents Method

Function Sites_Navigator_BeforeShow(Sender) 'Sites_Navigator_BeforeShow @67-7A1D3913

'Hide-Show Component @68-DC35DE82
    Dim TotalPages_68_1 : TotalPages_68_1 = CCSConverter.VBSConvert(ccsInteger, Sites.DataSource.Recordset.PageCount)
    Dim Param2_68_2 : Param2_68_2 = CCSConverter.VBSConvert(ccsInteger, 2)
    If  (Not IsEmpty(TotalPages_68_1) And Not IsEmpty(Param2_68_2) And TotalPages_68_1 < Param2_68_2) Then _
        Sites.Navigator.Visible = False
'End Hide-Show Component

End Function 'Close Sites_Navigator_BeforeShow @67-54C34B28

Function Sites1_City_id_BeforeShow(Sender) 'Sites1_City_id_BeforeShow @86-797C8019

End Function 'Close Sites1_City_id_BeforeShow @86-54C34B28

Function Sites1_Region_id_BeforeShow(Sender) 'Sites1_Region_id_BeforeShow @99-19336E91

End Function 'Close Sites1_Region_id_BeforeShow @99-54C34B28

Function Sites1_BeforeShow(Sender) 'Sites1_BeforeShow @73-0B3C3FAA

'Custom Code @111-73254650
' -------------------------
  	If Sites1.Recordset.EOF Then
		Sites1.Visible = False
 	End if

 	If(CCGetFromGet("var","") = "1") Then
 		Sites1.Visible = true
	end if

' -------------------------
'End Custom Code

End Function 'Close Sites1_BeforeShow @73-54C34B28



Function Page_BeforeInitialize(Sender) 'Page_BeforeInitialize @1-A73C9435

End Function 'Close Page_BeforeInitialize @1-54C34B28


%>
