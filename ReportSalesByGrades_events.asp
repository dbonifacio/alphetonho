<%
'BindEvents Method @1-F2FAF23D
Sub BindEvents(Level)
    If Level="Page" Then
    Else
        Set Grades_pump_sales_Sites.Navigator.CCSEvents("BeforeShow") = GetRef("Grades_pump_sales_Sites_Page_Footer_Navigator_BeforeShow")
        Set Grades_Sites_pump_sales.CCSEvents("BeforeShow") = GetRef("Grades_Sites_pump_sales_BeforeShow")
        Set Report_Print.CCSEvents("BeforeShow") = GetRef("Report_Print_BeforeShow")
    End If
End Sub
'End BindEvents Method

Function Grades_pump_sales_Sites_Page_Footer_Navigator_BeforeShow(Sender) 'Grades_pump_sales_Sites_Page_Footer_Navigator_BeforeShow @106-DAD75110

'Hide-Show Component @107-DE5057B1
    Dim TotalPages_107_1 : TotalPages_107_1 = CCSConverter.VBSConvert(ccsInteger, Grades_pump_sales_Sites.DataSource.Recordset.PageCount)
    Dim Param2_107_2 : Param2_107_2 = CCSConverter.VBSConvert(ccsInteger, 2)
    If  (Not IsEmpty(TotalPages_107_1) And Not IsEmpty(Param2_107_2) And TotalPages_107_1 < Param2_107_2) Then _
        Grades_pump_sales_Sites.Navigator.Visible = False
'End Hide-Show Component

End Function 'Close Grades_pump_sales_Sites_Page_Footer_Navigator_BeforeShow @106-54C34B28

Function Grades_Sites_pump_sales_BeforeShow(Sender) 'Grades_Sites_pump_sales_BeforeShow @72-4853A079

'Hide-Show Component @82-A4D1853D
    Dim ViewMode_82_1 : ViewMode_82_1 = CCSConverter.VBSConvert(ccsText, CCGetFromGet("ViewMode", Empty))
    Dim Param2_82_2 : Param2_82_2 = CCSConverter.VBSConvert(ccsText, "Print")
    If (IsEmpty(ViewMode_82_1) And IsEmpty(Param2_82_2)) Or  (Not IsEmpty(ViewMode_82_1) And Not IsEmpty(Param2_82_2) And ViewMode_82_1 = Param2_82_2) Then _
        Grades_Sites_pump_sales.Visible = False
'End Hide-Show Component

End Function 'Close Grades_Sites_pump_sales_BeforeShow @72-54C34B28

Function Report_Print_BeforeShow(Sender) 'Report_Print_BeforeShow @79-D2FE8D6A

'Hide-Show Component @81-484882B0
    Dim ViewMode_81_1 : ViewMode_81_1 = CCSConverter.VBSConvert(ccsText, CCGetFromGet("ViewMode", Empty))
    Dim Param2_81_2 : Param2_81_2 = CCSConverter.VBSConvert(ccsText, "Print")
    If (IsEmpty(ViewMode_81_1) And IsEmpty(Param2_81_2)) Or  (Not IsEmpty(ViewMode_81_1) And Not IsEmpty(Param2_81_2) And ViewMode_81_1 = Param2_81_2) Then _
        Report_Print.Visible = False
'End Hide-Show Component

End Function 'Close Report_Print_BeforeShow @79-54C34B28


%>
