<Page id="1" templateExtension="html" relativePath="." fullRelativePath="." secured="False" urlType="Relative" isIncluded="False" SSLAccess="False" isService="False" cachingEnabled="False" cachingDuration="1 minutes" wizardTheme="Compact" wizardThemeVersion="3.0" pasteActions="pasteActions" needGeneration="0">
	<Components>
		<IncludePage id="103" name="Header" PathID="Header" page="Header.ccp">
			<Components/>
			<Events/>
			<Features/>
		</IncludePage>
		<Grid id="104" secured="False" sourceType="Table" returnValueType="Number" defaultPageSize="10" connection="FusionHO" dataSource="price_change_status, Sites" name="Sites_price_change_status" pageSizeLimit="100" wizardCaption="{res:CCS_GridFormPrefix} {res:Sitesprice_change_status} {res:CCS_GridFormSuffix}" wizardGridType="Tabular" wizardSortingType="SimpleDir" wizardAllowInsert="False" wizardAltRecord="False" wizardAltRecordType="Style" wizardRecordSeparator="False" wizardNoRecords="{res:CCS_NoRecords}">
			<Components>
				<Sorter id="110" visible="True" name="Sorter_nuevo" column="nuevo" wizardCaption="{res:nuevo}" wizardSortingType="SimpleDir" wizardControl="nuevo" wizardAddNbsp="False" PathID="Sites_price_change_statusSorter_nuevo">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="111" visible="True" name="Sorter_SiteName" column="SiteName" wizardCaption="{res:SiteName}" wizardSortingType="SimpleDir" wizardControl="SiteName" wizardAddNbsp="False" PathID="Sites_price_change_statusSorter_SiteName">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Label id="112" fieldSourceType="DBColumn" dataType="Text" html="False" name="nuevo" fieldSource="nuevo" wizardCaption="{res:nuevo}" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" PathID="Sites_price_change_statusnuevo">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="113" fieldSourceType="DBColumn" dataType="Text" html="False" name="SiteName" fieldSource="SiteName" wizardCaption="{res:SiteName}" wizardSize="30" wizardMaxLength="30" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" PathID="Sites_price_change_statusSiteName">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Navigator id="114" size="10" type="Centered" pageSizes="1;5;10;25;50" name="Navigator" wizardPagingType="Centered" wizardFirst="True" wizardFirstText="{res:CCS_First}" wizardPrev="True" wizardPrevText="{res:CCS_Previous}" wizardNext="True" wizardNextText="{res:CCS_Next}" wizardLast="True" wizardLastText="{res:CCS_Last}" wizardPageNumbers="Centered" wizardSize="10" wizardTotalPages="True" wizardHideDisabled="False" wizardOfText="{res:CCS_Of}" wizardPageSize="True" wizardImagesScheme="Compact">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Navigator>
			</Components>
			<Events/>
			<TableParameters/>
			<JoinTables>
				<JoinTable id="105" tableName="price_change_status" schemaName="dbo" posLeft="323" posTop="72" posWidth="140" posHeight="180"/>
				<JoinTable id="106" tableName="Sites" schemaName="dbo" posLeft="172" posTop="20" posWidth="115" posHeight="180"/>
			</JoinTables>
			<JoinLinks>
				<JoinTable2 id="107" tableLeft="Sites" tableRight="price_change_status" fieldLeft="Sites.ss_id" fieldRight="price_change_status.ss_id" joinType="inner" conditionType="Equal"/>
			</JoinLinks>
			<Fields>
				<Field id="108" fieldName="(Cast (aplication_date as varchar(10))  + '  ' + aplication_time) " isExpression="True" alias="nuevo"/>
				<Field id="109" tableName="Sites" fieldName="SiteName"/>
			</Fields>
			<SPParameters/>
			<SQLParameters/>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</Grid>
		<Grid id="39" secured="False" sourceType="Table" returnValueType="Number" defaultPageSize="10" connection="FusionHO" dataSource="Sites, price_change_status" name="Grid2" orderBy="price_change_id desc" pageSizeLimit="100" wizardCaption="{res:CCS_GridFormPrefix} {res:Grid2} {res:CCS_GridFormSuffix}" wizardGridType="Tabular" wizardSortingType="SimpleDir" wizardAllowInsert="False" wizardAltRecord="False" wizardAltRecordType="Style" wizardRecordSeparator="False" wizardNoRecords="{res:CCS_NoRecords}" activeCollection="TableParameters" parameterTypeListName="ParameterTypeList">
			<Components>
				<Sorter id="40" visible="True" name="Sorter_price_change_id" column="price_change_id" wizardCaption="{res:price_change_id}" wizardSortingType="SimpleDir" wizardControl="price_change_id" wizardAddNbsp="False" PathID="Grid2Sorter_price_change_id">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="41" visible="True" name="Sorter_SiteName" column="SiteName" wizardCaption="{res:SiteName}" wizardSortingType="SimpleDir" wizardControl="SiteName" wizardAddNbsp="False" PathID="Grid2Sorter_SiteName">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="42" visible="True" name="Sorter_Aplication_Date" column="Aplication_Date" wizardCaption="{res:Aplication_Date}" wizardSortingType="SimpleDir" wizardControl="Aplication_Date" wizardAddNbsp="False" PathID="Grid2Sorter_Aplication_Date">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>

				</Sorter>
				<Sorter id="43" visible="True" name="Sorter_Aplication_Time" column="Aplication_Time" wizardCaption="{res:Aplication_Time}" wizardSortingType="SimpleDir" wizardControl="Aplication_Time" wizardAddNbsp="False" PathID="Grid2Sorter_Aplication_Time">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="44" visible="True" name="Sorter_Reception_Date" column="Reception_Date" wizardCaption="{res:Reception_Date}" wizardSortingType="SimpleDir" wizardControl="Reception_Date" wizardAddNbsp="False" PathID="Grid2Sorter_Reception_Date">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="45" visible="True" name="Sorter_Reception_time" column="Reception_time" wizardCaption="{res:Reception_time}" wizardSortingType="SimpleDir" wizardControl="Reception_time" wizardAddNbsp="False" PathID="Grid2Sorter_Reception_time">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="46" visible="True" name="Sorter_File_Delivery_Date" column="File_Delivery_Date" wizardCaption="{res:File_Delivery_Date}" wizardSortingType="SimpleDir" wizardControl="File_Delivery_Date" wizardAddNbsp="False" PathID="Grid2Sorter_File_Delivery_Date">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="47" visible="True" name="Sorter_File_Delivery_Time" column="File_Delivery_Time" wizardCaption="{res:File_Delivery_Time}" wizardSortingType="SimpleDir" wizardControl="File_Delivery_Time" wizardAddNbsp="False" PathID="Grid2Sorter_File_Delivery_Time">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="48" visible="True" name="Sorter_Status" column="Status" wizardCaption="{res:Status}" wizardSortingType="SimpleDir" wizardControl="Status" wizardAddNbsp="False" PathID="Grid2Sorter_Status">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="49" visible="True" name="Sorter_Status_Description" column="Status_Description" wizardCaption="{res:Status_Description}" wizardSortingType="SimpleDir" wizardControl="Status_Description" wizardAddNbsp="False" PathID="Grid2Sorter_Status_Description">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Link id="50" fieldSourceType="DBColumn" dataType="Integer" html="False" name="price_change_id" fieldSource="price_change_id" wizardCaption="{res:price_change_id}" wizardSize="10" wizardMaxLength="10" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAlign="right" wizardAddNbsp="True" PathID="Grid2price_change_id" visible="Yes" hrefType="Page" urlType="Relative" preserveParameters="GET" hrefSource="send_price.ccp">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
					<LinkParameters>
						<LinkParameter id="63" sourceType="DataField" name="price_change_id" source="price_change_id"/>
					</LinkParameters>
				</Link>
				<Label id="51" fieldSourceType="DBColumn" dataType="Text" html="False" name="SiteName" fieldSource="SiteName" wizardCaption="{res:SiteName}" wizardSize="30" wizardMaxLength="30" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" PathID="Grid2SiteName">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="52" fieldSourceType="DBColumn" dataType="Date" html="False" name="Aplication_Date" fieldSource="Aplication_Date" wizardCaption="{res:Aplication_Date}" wizardSize="8" wizardMaxLength="100" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" PathID="Grid2Aplication_Date">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="53" fieldSourceType="DBColumn" dataType="Text" html="False" name="Aplication_Time" fieldSource="Aplication_Time" wizardCaption="{res:Aplication_Time}" wizardSize="8" wizardMaxLength="8" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" PathID="Grid2Aplication_Time">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="54" fieldSourceType="DBColumn" dataType="Date" html="False" name="Reception_Date" fieldSource="Reception_Date" wizardCaption="{res:Reception_Date}" wizardSize="8" wizardMaxLength="100" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" PathID="Grid2Reception_Date">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="55" fieldSourceType="DBColumn" dataType="Text" html="False" name="Reception_time" fieldSource="Reception_time" wizardCaption="{res:Reception_time}" wizardSize="8" wizardMaxLength="8" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" PathID="Grid2Reception_time">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="56" fieldSourceType="DBColumn" dataType="Date" html="False" name="File_Delivery_Date" fieldSource="File_Delivery_Date" wizardCaption="{res:File_Delivery_Date}" wizardSize="8" wizardMaxLength="100" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" PathID="Grid2File_Delivery_Date">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="57" fieldSourceType="DBColumn" dataType="Text" html="False" name="File_Delivery_Time" fieldSource="File_Delivery_Time" wizardCaption="{res:File_Delivery_Time}" wizardSize="8" wizardMaxLength="8" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" PathID="Grid2File_Delivery_Time">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="58" fieldSourceType="DBColumn" dataType="Integer" html="False" name="Status" fieldSource="Status" wizardCaption="{res:Status}" wizardSize="10" wizardMaxLength="10" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAlign="right" wizardAddNbsp="True" PathID="Grid2Status">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="59" fieldSourceType="DBColumn" dataType="Text" html="False" name="Status_Description" fieldSource="Status_Description" wizardCaption="{res:Status_Description}" wizardSize="50" wizardMaxLength="50" wizardIsPassword="False" wizardUseTemplateBlock="False" wizardAddNbsp="True" PathID="Grid2Status_Description">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Navigator id="60" size="10" type="Centered" pageSizes="1;5;10;25;50" name="Navigator" wizardPagingType="Centered" wizardFirst="True" wizardFirstText="{res:CCS_First}" wizardPrev="True" wizardPrevText="{res:CCS_Previous}" wizardNext="True" wizardNextText="{res:CCS_Next}" wizardLast="True" wizardLastText="{res:CCS_Last}" wizardPageNumbers="Centered" wizardSize="10" wizardTotalPages="True" wizardHideDisabled="False" wizardOfText="{res:CCS_Of}" wizardPageSize="True" wizardImagesScheme="Fusionho1">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Navigator>
				<Link id="115" visible="Yes" fieldSourceType="DBColumn" dataType="Text" html="False" hrefType="Page" urlType="Relative" preserveParameters="GET" name="Link1" PathID="Grid2Link1" hrefSource="send_price.ccp" wizardUseTemplateBlock="False" removeParameters="price_change_id;aplication_date;aplication_time">
					<Components/>
					<Events/>
					<LinkParameters>
						<LinkParameter id="116" sourceType="Expression" name="var" source="1"/>
					</LinkParameters>
					<Attributes/>
					<Features/>
				</Link>
			</Components>
			<Events>
				<Event name="BeforeShow" type="Server">
					<Actions>
						<Action actionName="Custom Code" actionCategory="General" id="117"/>
					</Actions>
				</Event>
			</Events>
			<TableParameters>
				<TableParameter id="118" conditionType="Parameter" useIsNull="False" field="price_change_status.ss_id" dataType="Integer" searchConditionType="Equal" parameterType="URL" logicOperator="And" parameterSource="ss_id"/>
			</TableParameters>
			<JoinTables>
				<JoinTable id="119" tableName="Sites" schemaName="dbo" posLeft="10" posTop="10" posWidth="115" posHeight="180"/>
				<JoinTable id="120" tableName="price_change_status" schemaName="dbo" posLeft="146" posTop="10" posWidth="140" posHeight="180"/>
			</JoinTables>
			<JoinLinks>
				<JoinTable2 id="121" tableLeft="Sites" tableRight="price_change_status" fieldLeft="Sites.ss_id" fieldRight="price_change_status.ss_id" joinType="inner" conditionType="Equal"/>
			</JoinLinks>
			<Fields>
				<Field id="122" tableName="price_change_status" fieldName="price_change_id"/>
				<Field id="123" tableName="Sites" fieldName="SiteName"/>
				<Field id="124" fieldName="(Cast (aplication_date as varchar(10))  + '  ' + aplication_time)" alias="Aplication_Date" isExpression="True"/>
				<Field id="125" tableName="price_change_status" fieldName="aplication_time" alias="Aplication_Time"/>
				<Field id="126" tableName="price_change_status" fieldName="reception_date" alias="Reception_Date"/>
				<Field id="127" tableName="price_change_status" fieldName="reception_time" alias="Reception_Time"/>
				<Field id="128" tableName="price_change_status" fieldName="file_delivery_date" alias="File_Delivery_Date"/>
				<Field id="129" tableName="price_change_status" fieldName="file_delivery_time" alias="File_Delivery_Time"/>
				<Field id="130" tableName="price_change_status" fieldName="status_code" alias="Status"/>
				<Field id="131" tableName="price_change_status" fieldName="status_description" alias="Status_Description"/>
			</Fields>
			<SPParameters/>
			<SQLParameters>
				<SQLParameter id="62" variable="ss_id" parameterType="URL" dataType="Text" parameterSource="ss_id"/>
			</SQLParameters>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</Grid>
	</Components>
	<CodeFiles>
		<CodeFile id="Code" language="ASPTemplates" name="change_price.asp" forShow="True" url="change_price.asp" comment="'" codePage="windows-1252"/>
		<CodeFile id="Events" language="ASPTemplates" name="change_price_events.asp" forShow="False" comment="'" codePage="windows-1252"/>
	</CodeFiles>
	<SecurityGroups/>
	<CachingParameters/>
	<Attributes/>
	<Features/>
	<Events/>
</Page>
