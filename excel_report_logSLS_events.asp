<%
'BindEvents Method @1-4F9CB759
Sub BindEvents(Level)
    If Level="Page" Then
        Set CCSEvents("OnInitializeView") = GetRef("Page_OnInitializeView")
    Else
        Set Report1.CCSEvents("BeforeSelect") = GetRef("Report1_BeforeSelect")
    End If
End Sub
'End BindEvents Method

Function Report1_BeforeSelect(Sender) 'Report1_BeforeSelect @2-7FD7438B

'Custom Code @22-73254650
' -------------------------
Report1.PageSize = 10000
' -------------------------
'End Custom Code

End Function 'Close Report1_BeforeSelect @2-54C34B28

Function Page_OnInitializeView(Sender) 'Page_OnInitializeView @1-1F930D9C

'Custom Code @21-73254650
' -------------------------
Response.Buffer = True
Response.ContentType = "application/vnd.ms-excel"
response.addHeader "Content-Disposition","inline; filename=Report_logSLS.xls"
' -------------------------
'End Custom Code

End Function 'Close Page_OnInitializeView @1-54C34B28


%>
