<%
'BindEvents Method @1-6720AB0C
Sub BindEvents(Level)
    If Level="Page" Then
    Else
        Set Service.Navigator.CCSEvents("BeforeShow") = GetRef("Service_Navigator_BeforeShow")
        Set Service1.CCSEvents("BeforeShow") = GetRef("Service1_BeforeShow")
    End If
End Sub
'End BindEvents Method

Function Service_Navigator_BeforeShow(Sender) 'Service_Navigator_BeforeShow @17-9DE52AE9

'Hide-Show Component @18-DDD1FDD3
    Dim TotalPages_18_1 : TotalPages_18_1 = CCSConverter.VBSConvert(ccsInteger, Service.DataSource.Recordset.PageCount)
    Dim Param2_18_2 : Param2_18_2 = CCSConverter.VBSConvert(ccsInteger, 2)
    If  (Not IsEmpty(TotalPages_18_1) And Not IsEmpty(Param2_18_2) And TotalPages_18_1 < Param2_18_2) Then _
        Service.Navigator.Visible = False
'End Hide-Show Component

End Function 'Close Service_Navigator_BeforeShow @17-54C34B28

Function Service1_BeforeShow(Sender) 'Service1_BeforeShow @21-397164C5

'Custom Code @29-73254650
' -------------------------
  	If Service1.Recordset.EOF Then
		Service1.Visible = False
 	End if

 	If(CCGetFromGet("var","") = "1") Then
 		Service1.Visible = true
	end if
' -------------------------
'End Custom Code

End Function 'Close Service1_BeforeShow @21-54C34B28


%>
