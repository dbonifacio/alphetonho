<Page id="1" templateExtension="html" relativePath=".." fullRelativePath=".\services" secured="False" urlType="Relative" isIncluded="False" SSLAccess="False" isService="True" cachingEnabled="False" cachingDuration="1 minutes" needGeneration="0">
	<Components>
		<Grid id="2" secured="False" sourceType="Table" returnValueType="Number" defaultPageSize="10" connection="FusionHO" dataSource="Moso" name="Moso" orderBy="Moso_id" pageSizeLimit="100" wizardCaption="List of Moso " wizardAllowInsert="False">
<Components>
<Label id="62" fieldSourceType="DBColumn" dataType="Integer" html="False" name="Moso_id" fieldSource="Moso_id">
<Components/>
<Events/>
<Attributes/>
<Features/>
</Label>
<Label id="63" fieldSourceType="DBColumn" dataType="Text" html="False" name="Moso_name" fieldSource="Moso_name">
<Components/>
<Events/>
<Attributes/>
<Features/>
</Label>
</Components>
<Events/>
<TableParameters/>
<JoinTables/>
<JoinLinks/>
<Fields/>
<SPParameters/>
<SQLParameters/>
<SecurityGroups/>
<Attributes/>
<Features/>
</Grid>
</Components>
	<CodeFiles>
		<CodeFile id="Code" language="ASPTemplates" name="moso.asp" forShow="True" url="moso.asp" comment="'" codePage="windows-1252"/>
</CodeFiles>
	<SecurityGroups/>
	<CachingParameters/>
	<Attributes/>
	<Features/>
	<Events/>
</Page>
