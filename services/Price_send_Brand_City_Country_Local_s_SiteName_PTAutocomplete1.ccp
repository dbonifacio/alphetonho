<Page id="1" templateExtension="html" relativePath=".." fullRelativePath=".\services" secured="False" urlType="Relative" isIncluded="False" SSLAccess="False" isService="True" cachingEnabled="False" cachingDuration="1 minutes" needGeneration="0">
	<Components>
		<Grid id="2" secured="False" sourceType="Table" returnValueType="Number" defaultPageSize="10" connection="FusionHO" dataSource="Sites" orderBy="SiteName" name="Sites" pageSizeLimit="100" wizardCaption="{res:CCS_GridFormPrefix} {res:Sites} {res:CCS_GridFormSuffix}">
<Components>
<Label id="240" fieldSourceType="DBColumn" dataType="Text" html="False" name="SiteName" fieldSource="SiteName">
<Components/>
<Events/>
<Attributes/>
<Features/>
</Label>
</Components>
<Events/>
<TableParameters>
<TableParameter id="239" conditionType="Parameter" useIsNull="False" field="SiteName" dataType="Text" logicOperator="And" searchConditionType="BeginsWith" parameterType="Form" parameterSource="s_SiteName"/>
</TableParameters>
<JoinTables>
<JoinTable id="236" tableName="Sites" schemaName="dbo" posLeft="10" posTop="10" posWidth="115" posHeight="180"/>
</JoinTables>
<JoinLinks/>
<Fields>
<Field id="237" tableName="Sites" fieldName="SiteName"/>
</Fields>
<SPParameters/>
<SQLParameters/>
<SecurityGroups/>
<Attributes/>
<Features/>
</Grid>
</Components>
	<CodeFiles>
		<CodeFile id="Code" language="ASPTemplates" name="Price_send_Brand_City_Country_Local_s_SiteName_PTAutocomplete1.asp" forShow="True" url="Price_send_Brand_City_Country_Local_s_SiteName_PTAutocomplete1.asp" comment="'" codePage="windows-1252"/>
</CodeFiles>
	<SecurityGroups/>
	<CachingParameters/>
	<Attributes/>
	<Features/>
	<Events/>
</Page>
