<Page id="1" templateExtension="html" relativePath=".." fullRelativePath=".\services" secured="False" urlType="Relative" isIncluded="False" SSLAccess="False" isService="True" cachingEnabled="False" cachingDuration="1 minutes" needGeneration="0">
	<Components>
		<Grid id="2" secured="False" sourceType="Table" returnValueType="Number" connection="FusionHO" dataSource="tank_actual_info, Sites, Grades" activeCollection="TableParameters" name="tank_actual_info_Sites_Gr" pageSizeLimit="100" wizardCaption="{res:CCS_GridFormPrefix} {res:tank_actual_info_Sites_Gr} {res:CCS_GridFormSuffix}">
<Components>
<Label id="34" fieldSourceType="DBColumn" dataType="Text" html="False" name="grade_name" fieldSource="grade_name">
<Components/>
<Events/>
<Attributes/>
<Features/>
</Label>
<Label id="35" fieldSourceType="DBColumn" dataType="Text" html="False" name="grade_name1" fieldSource="grade_name">
<Components/>
<Events/>
<Attributes/>
<Features/>
</Label>
</Components>
<Events/>
<TableParameters>
<TableParameter id="33" conditionType="Parameter" useIsNull="True" field="grade_id" dataType="Integer" logicOperator="And" searchConditionType="Equal" parameterType="URL" parameterSource="keyword"/>
</TableParameters>
<JoinTables>
<JoinTable id="24" tableName="tank_actual_info" posLeft="10" posTop="10" posWidth="160" posHeight="180"/>
<JoinTable id="25" tableName="Sites" schemaName="dbo" posLeft="191" posTop="10" posWidth="115" posHeight="180"/>
<JoinTable id="27" tableName="Grades" schemaName="dbo" posLeft="327" posTop="10" posWidth="95" posHeight="104"/>
</JoinTables>
<JoinLinks>
<JoinTable2 id="26" tableLeft="tank_actual_info" tableRight="Sites" fieldLeft="tank_actual_info.ss_id" fieldRight="Sites.ss_id" joinType="inner" conditionType="Equal"/>
<JoinTable2 id="28" tableLeft="tank_actual_info" tableRight="Grades" fieldLeft="tank_actual_info.grade_id" fieldRight="Grades.grade_id" joinType="inner" conditionType="Equal"/>
</JoinLinks>
<Fields>
<Field id="29" tableName="tank_actual_info" fieldName="tank_actual_info.*"/>
<Field id="30" tableName="Sites" fieldName="Sites.*"/>
<Field id="31" tableName="Grades" fieldName="grade_name"/>
</Fields>
<SPParameters/>
<SQLParameters/>
<SecurityGroups/>
<Attributes/>
<Features/>
</Grid>
</Components>
	<CodeFiles>
		<CodeFile id="Code" language="ASPTemplates" name="NewPage1_tank_actual_info_prod_PTDependentListBox1.asp" forShow="True" url="NewPage1_tank_actual_info_prod_PTDependentListBox1.asp" comment="'" codePage="windows-1252"/>
</CodeFiles>
	<SecurityGroups/>
	<CachingParameters/>
	<Attributes/>
	<Features/>
	<Events/>
</Page>
