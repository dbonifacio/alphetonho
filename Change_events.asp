<%
'BindEvents Method @1-FEAA48D1
Sub BindEvents(Level)
    If Level="Page" Then
    Else
        Set changePass.usuario.CCSEvents("BeforeShow") = GetRef("changePass_usuario_BeforeShow")
        Set changePass.pass.CCSEvents("BeforeShow") = GetRef("changePass_pass_BeforeShow")
        Set changePass.CCSEvents("OnValidate") = GetRef("changePass_OnValidate")
    End If
End Sub
'End BindEvents Method

Function changePass_usuario_BeforeShow(Sender) 'changePass_usuario_BeforeShow @6-F2E1AB96

'Custom Code @10-73254650
' -------------------------
changePass.usuario.Value = CCGetUserLogin()

' -------------------------
'End Custom Code

End Function 'Close changePass_usuario_BeforeShow @6-54C34B28

Function changePass_pass_BeforeShow(Sender) 'changePass_pass_BeforeShow @24-6A47062D

'Custom Code @25-73254650
' -------------------------
changePass.pass.Value = CCGetUserID()
' -------------------------
'End Custom Code

End Function 'Close changePass_pass_BeforeShow @24-54C34B28



'DEL  ' -------------------------
'DEL  changePass.usuario.Value = CCGetUserID()
'DEL  ' -------------------------




Function changePass_OnValidate(Sender) 'changePass_OnValidate @2-982AFD27

'Custom Code @13-73254650
' -------------------------
If (changePass.cpass.Value <> changePass.npass.Value) Then
         changePass.Errors.addError("Password and confirmation password doesn't match")
   End If
     dim c
   c = CCDLookUp("COUNT(*)","Users","User_id="&DBFusionHO.ToSQL(changePass.pass.Value,ccsInteger)&" AND Password="&DBFusionHO.ToSQL(changePass.opass.Value,ccsText),DBFusionHO)
     If (c < 1) Then
         changePass.Errors.addError("Current password and entered password doesn't match")
   End If
   If (Len(changePass.npass.Value) < 5) Then
       changePass.Errors.AddError "Password length must be more than 5 symbols"
   End If

' -------------------------
'End Custom Code

End Function 'Close changePass_OnValidate @2-54C34B28


%>
