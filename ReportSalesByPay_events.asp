<%
'BindEvents Method @1-9509886D
Sub BindEvents(Level)
    If Level="Page" Then
    Else
        Set addin_payments_data_pump.Navigator.CCSEvents("BeforeShow") = GetRef("addin_payments_data_pump_Page_Footer_Navigator_BeforeShow")
        Set Sites_addin_payments_data.CCSEvents("BeforeShow") = GetRef("Sites_addin_payments_data_BeforeShow")
        Set Report_Print.CCSEvents("BeforeShow") = GetRef("Report_Print_BeforeShow")
    End If
End Sub
'End BindEvents Method

Function addin_payments_data_pump_Page_Footer_Navigator_BeforeShow(Sender) 'addin_payments_data_pump_Page_Footer_Navigator_BeforeShow @39-65B9CC49

'Hide-Show Component @40-F66AAFF0
    Dim TotalPages_40_1 : TotalPages_40_1 = CCSConverter.VBSConvert(ccsInteger, addin_payments_data_pump.DataSource.Recordset.PageCount)
    Dim Param2_40_2 : Param2_40_2 = CCSConverter.VBSConvert(ccsInteger, 2)
    If  (Not IsEmpty(TotalPages_40_1) And Not IsEmpty(Param2_40_2) And TotalPages_40_1 < Param2_40_2) Then _
        addin_payments_data_pump.Navigator.Visible = False
'End Hide-Show Component

End Function 'Close addin_payments_data_pump_Page_Footer_Navigator_BeforeShow @39-54C34B28

Function Sites_addin_payments_data_BeforeShow(Sender) 'Sites_addin_payments_data_BeforeShow @14-5833CBB5

'Hide-Show Component @22-F03873D0
    Dim ViewMode_22_1 : ViewMode_22_1 = CCSConverter.VBSConvert(ccsText, CCGetFromGet("ViewMode", Empty))
    Dim Param2_22_2 : Param2_22_2 = CCSConverter.VBSConvert(ccsText, "Print")
    If (IsEmpty(ViewMode_22_1) And IsEmpty(Param2_22_2)) Or  (Not IsEmpty(ViewMode_22_1) And Not IsEmpty(Param2_22_2) And ViewMode_22_1 = Param2_22_2) Then _
        Sites_addin_payments_data.Visible = False
'End Hide-Show Component

End Function 'Close Sites_addin_payments_data_BeforeShow @14-54C34B28

Function Report_Print_BeforeShow(Sender) 'Report_Print_BeforeShow @19-D2FE8D6A

'Hide-Show Component @21-541DDF71
    Dim ViewMode_21_1 : ViewMode_21_1 = CCSConverter.VBSConvert(ccsText, CCGetFromGet("ViewMode", Empty))
    Dim Param2_21_2 : Param2_21_2 = CCSConverter.VBSConvert(ccsText, "Print")
    If (IsEmpty(ViewMode_21_1) And IsEmpty(Param2_21_2)) Or  (Not IsEmpty(ViewMode_21_1) And Not IsEmpty(Param2_21_2) And ViewMode_21_1 = Param2_21_2) Then _
        Report_Print.Visible = False
'End Hide-Show Component

End Function 'Close Report_Print_BeforeShow @19-54C34B28


%>
