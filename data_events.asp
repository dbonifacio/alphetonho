<%
'BindEvents Method @1-7C8C0B75
Sub BindEvents(Level)
    If Level="Page" Then
        Set CCSEvents("BeforeShow") = GetRef("Page_BeforeShow")
    Else
    End If
End Sub
'End BindEvents Method

Function Page_BeforeShow(Sender) 'Page_BeforeShow @1-A1547E8B

'Custom Code @2-73254650
' -------------------------
' Write your own code here.
' -------------------------
'End Custom Code

dim FusionHO
Dim SQL
Dim SQL2
Dim SQL3
Dim rsGroups
Dim rGroups
Dim strGroups
Dim strXML

dim strCategories, strtank1, strtank2, strtank3, strtank4
dim i
dim var
dim var1
dim var2
dim var5
dim aux
dim b
dim fecha
dim cant_tanks
dim k
dim j

b = false
strGroups = ""
strXML = ""
Set FusionHO = Server.CreateObject("ADODB.Connection") 
FusionHO.open "PROVIDER=SQLNCLI10;DATA SOURCE=MICROSTR-0A74B0;UID=sa;PWD=serbiznet;DATABASE=FusionHO " 
var = ""
var = CCGetRequestParam("site", ccsGet)
var2 = CCGetRequestParam("prod", ccsGet)

var1 = CCGetRequestParam("date", ccsGet)
fecha = mid(var1,7,4)&"-"& mid(var1,1,2)&"-"& mid(var1,4,2)


SQL = "	select s_tank as tank, tank_time as hour, round(volume,0,1) as vol  from tank_table where tank_DATE ='"&fecha&"' and site_name = '"&var&"'"
SQL2 = "select count(distinct(t.tank_id)) as cantidad from tank_actual_info as t inner join Sites as s on t.ss_id = s.ss_id where s.SiteName = '"&var&"'"
SQL3 = "select distinct(t.tank_id) as tanques from tank_actual_info as t inner join Sites as s on t.ss_id = s.ss_id where s.SiteName = '"&var&"'"

Set rGroups = FusionHO.Execute(SQL2)
While not rGroups.EOF
	cant_tanks = rGroups("cantidad")
rGroups.MoveNext 
wend

dim arrData()
dim fila
dim col
fila = 24
col = cant_tanks + 1
ReDim arrData(fila, col)

dim algo																																																												
dim vec()
Redim vec(cant_tanks)
dim tanks()
Redim tanks(cant_tanks)
dim color(16) 
color(1) = "AF00F8"
color(2) = "F6000F"
color(3) = "AFBBF8"
color(4) = "FBFF00"
color(5) = "C75045"
color(6) = "00CC00"
color(7) = "FF6600"
color(8) = "0066B3"
color(9) = "8FB200"
color(10) = "80FE80"
color(11) = "B24700"
color(12) = "0033CC"
color(13) = "007D47"
color(14) = "9CD4A2"
color(15) = "26C7E3"
color(16) = "FFB300"

k=1
Set rGroups = FusionHO.Execute(SQL3)
While not rGroups.EOF
	tanks(k) = rGroups("tanques")
	k = k + 1
rGroups.MoveNext 
wend




	
For i=0 to UBound(arrData)-1  
arrData(i,1) = i
arrData(i,2) = 0
arrData(i,3) = 0
arrData(i,4) = 0
arrData(i,5) = 0
next


'/////////// leo de la base, y guardo los datos en un array con las horas como indices
 Set rsGroups = FusionHO.Execute(SQL)
While not rsGroups.EOF
			for j= 1 to cant_tanks
 			if rsGroups("tank") = tanks(j) then
				arrData(rsGroups("hour"),j+1) = rsGroups("vol")
			end if
			next
  rsGroups.MoveNext

'Initialize <graph> element
strXML = "<graph caption='        Stock By Tank in " & var & "' subcaption='"&fecha&"             ' hovercapbg='FFECAA' chartBottomMargin='0' hovercapborder='F47E00' formatNumberScale='0' decimalPrecision='2' showvalues='0' numdivlines='5' numVdivlines='0' yaxisminvalue='0' yaxismaxvalue='7000'  rotateNames='0' xAxisName='Hours' yAxisName='Volume'>"

'Initialize <categories> element - necessary to generate a multi-series chart
strCategories = "<categories>"


	 
'Initiate <dataset> elements

 for j = 1 to cant_tanks
vec(j) = "<dataset seriesName=' Tanque "
vec(j) = vec(j) & tanks(j)
vec(j) = vec(j) & "' color='"
vec(j) = vec(j) & color(j)
vec(j) = vec(j) & "' >"
next

'//////// armo el xml llenando los campos vacios hacia atras
 For i=0 to UBound(arrData)-1
		'Append <category name='...' /> to strCategories
		strCategories = strCategories & "<category name='" & arrData(i,1) & "' />"
		'Add <set value='...' /> to both the datasets

			for j = 1 to cant_tanks

 				if arrData(i,j+1) = 0 then
					arrData(i,j+1) =  arrData(i + 1,j+1)
					vec(j)= vec(j) & "<set value='" & arrData(i,j+1) & "' />"
				else 
					vec(j)= vec(j) &  "<set value='" & arrData(i,j+1) & "' />"
				end if

			next
   Next
 

  

Wend
   'Close <categories> element
   strCategories = strCategories & "</categories>"
   
      'Close <dataset> elements

	  for j = 1 to cant_tanks
	  vec(j) = vec(j) & "</dataset>"
	  next
   'Assemble the entire XML now
'   ///////////// definir esto cuando capturo los datos del form ////////////

var5 = Cint(var2)
if var2 = "" then
	aux = ""
	for j = 1 to cant_tanks
    	aux = aux & vec(j)
	next
	strXML = strXML & strCategories & aux & "</graph>"'
else

	for j = 1 to cant_tanks	
		if (tanks(j) = var5) then
			strXML = strXML & strCategories & vec(j) & "</graph>"'
		end if
next
end if

Response.ContentType = "text/xml"
'Just write out the XML data
'NOTE THAT THIS PAGE DOESN'T CONTAIN ANY HTML TAG, WHATSOEVER
 Response.Write(strXML)
'end if


End Function 'Close Page_BeforeShow @1-54C34B28


%>
