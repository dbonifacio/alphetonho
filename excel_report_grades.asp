<%@ CodePage=1252 %>
<%
'Include Common Files @1-502698DC
%>
<!-- #INCLUDE VIRTUAL="/Alpheton/Common.asp"-->
<!-- #INCLUDE VIRTUAL="/Alpheton/Cache.asp" -->
<!-- #INCLUDE VIRTUAL="/Alpheton/Template.asp" -->
<!-- #INCLUDE VIRTUAL="/Alpheton/Sorter.asp" -->
<!-- #INCLUDE VIRTUAL="/Alpheton/Navigator.asp" -->
<%
'End Include Common Files

'Initialize Page @1-8AD4A22B
' Variables
Dim PathToRoot, ScriptPath, TemplateFilePath
Dim FileName
Dim Redirect
Dim IsService
Dim Tpl, HTMLTemplate
Dim TemplateFileName
Dim ComponentName
Dim PathToCurrentPage
Dim Attributes

' Events
Dim CCSEvents
Dim CCSEventResult

' Connections
Dim DBFusionHO

' Page controls
Dim Report1
Dim ChildControls

Session.CodePage = CCSLocales.Locale.CodePage
Response.Charset = CCSLocales.Locale.Charset
Response.ContentType = CCSContentType
IsService = False
Redirect = ""
TemplateFileName = "excel_report_grades.html"
Set CCSEvents = CreateObject("Scripting.Dictionary")
PathToCurrentPage = "./"
FileName = "excel_report_grades.asp"
PathToRoot = "./"
ScriptPath = Left(Request.ServerVariables("PATH_TRANSLATED"), Len(Request.ServerVariables("PATH_TRANSLATED")) - Len(FileName))
TemplateFilePath = ScriptPath
'End Initialize Page

'Initialize Objects @1-D21DBB1C
BindEvents "Page"
CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeInitialize", Nothing)

Set DBFusionHO = New clsDBFusionHO
DBFusionHO.Open
Set Attributes = New clsAttributes
Attributes("pathToRoot") = PathToRoot

' Controls
Set Report1 = New clsReportReport1
Report1.Initialize DBFusionHO

' Events
%>
<!-- #INCLUDE VIRTUAL="/Alpheton/excel_report_grades_events.asp" -->
<%
BindEvents Empty

CCSEventResult = CCRaiseEvent(CCSEvents, "AfterInitialize", Nothing)
'End Initialize Objects

'Go to destination page @1-6D35F4FD
If NOT ( Redirect = "" ) Then
    UnloadPage
    Response.Redirect Redirect
End If
'End Go to destination page

'Initialize HTML Template @1-2E9DB4BC
CCSEventResult = CCRaiseEvent(CCSEvents, "OnInitializeView", Nothing)
Set HTMLTemplate = new clsTemplate
Set HTMLTemplate.Cache = TemplatesRepository
HTMLTemplate.LoadTemplate TemplateFilePath & TemplateFileName
HTMLTemplate.SetVar "@CCS_PathToRoot", PathToRoot
Set Tpl = HTMLTemplate.Block("main")
CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShow", Nothing)
'End Initialize HTML Template

'Show Page @1-CC301232
Attributes.Show HTMLTemplate, "page:"
Set ChildControls = CCCreateCollection(Tpl, Null, ccsParseOverwrite, _
    Array(Report1))
ChildControls.Show
Dim MainHTML
HTMLTemplate.Parse "main", False
If IsEmpty(MainHTML) Then MainHTML = HTMLTemplate.GetHTML("main")
CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeOutput", Nothing)
If CCSEventResult Then Response.Write MainHTML
'End Show Page

'Unload Page @1-CB210C62
UnloadPage
Set Tpl = Nothing
Set HTMLTemplate = Nothing
'End Unload Page

'UnloadPage Sub @1-9551D464
Sub UnloadPage()
    CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeUnload", Nothing)
    If DBFusionHO.State = adStateOpen Then _
        DBFusionHO.Close
    Set DBFusionHO = Nothing
    Set CCSEvents = Nothing
    Set Attributes = Nothing
    Set Report1 = Nothing
End Sub
'End UnloadPage Sub

'Report1 clsReportGroup @2-3CAA01B8
Class clsReportGroupReport1
    Public GroupType
    Private mOpen
    Private mClose
    Public Report_TotalRecords
    Public Site_Name
    Public Grade_Name
    Public Sale_Date
    Public Sum_money
    Public Sum_Volume
    Public Count_Sales
    Public Sum_Sum_money1
    Public Sum_Sum_Volume1
    Public Sum_Count_Sales1
    Public Sum_Sum_money
    Public Sum_Sum_Volume
    Public Sum_Count_Sales
    Public TotalSum_Sum_money
    Public TotalSum_Sum_Volume
    Public TotalSum_Count_Sales
    Public Report_CurrentDate
    Public ReportTotalIndex, PageTotalIndex
    Public PageNumber
    Public RowNumber
    Public IsDSEmpty
    Public Site_NameTotalIndex
    Public Grade_NameTotalIndex

    Public Sub SetControls()
        Me.Site_Name = Report1.Site_Name.Value
        Me.Grade_Name = Report1.Grade_Name.Value
        Me.Sale_Date = Report1.Sale_Date.Value
        Me.Sum_money = Report1.Sum_money.Value
        Me.Sum_Volume = Report1.Sum_Volume.Value
        Me.Count_Sales = Report1.Count_Sales.Value
        Me.Report_CurrentDate = Report1.Report_CurrentDate.Value
    End Sub

    Public Sub  SyncWithHeader(HeaderGrp)
        HeaderGrp.SetTotalControls False
        Me.Site_Name = HeaderGrp.Site_Name
        Report1.Site_Name.ChangeValue(Me.Site_Name)
        Me.Grade_Name = HeaderGrp.Grade_Name
        Report1.Grade_Name.ChangeValue(Me.Grade_Name)
        Me.Sale_Date = HeaderGrp.Sale_Date
        Report1.Sale_Date.ChangeValue(Me.Sale_Date)
        Me.Sum_money = HeaderGrp.Sum_money
        Report1.Sum_money.ChangeValue(Me.Sum_money)
        Me.Sum_Volume = HeaderGrp.Sum_Volume
        Report1.Sum_Volume.ChangeValue(Me.Sum_Volume)
        Me.Count_Sales = HeaderGrp.Count_Sales
        Report1.Count_Sales.ChangeValue(Me.Count_Sales)
        Me.Report_CurrentDate = HeaderGrp.Report_CurrentDate
        Report1.Report_CurrentDate.ChangeValue(Me.Report_CurrentDate)
    End Sub

    Public Sub SetTotalControls(isCalculate)
        Me.Report_TotalRecords = Report1.Report_TotalRecords.GetTotalValue(isCalculate)
        Me.Sum_Sum_money1 = Report1.Sum_Sum_money1.GetTotalValue(isCalculate)
        Me.Sum_Sum_Volume1 = Report1.Sum_Sum_Volume1.GetTotalValue(isCalculate)
        Me.Sum_Count_Sales1 = Report1.Sum_Count_Sales1.GetTotalValue(isCalculate)
        Me.Sum_Sum_money = Report1.Sum_Sum_money.GetTotalValue(isCalculate)
        Me.Sum_Sum_Volume = Report1.Sum_Sum_Volume.GetTotalValue(isCalculate)
        Me.Sum_Count_Sales = Report1.Sum_Count_Sales.GetTotalValue(isCalculate)
        Me.TotalSum_Sum_money = Report1.TotalSum_Sum_money.GetTotalValue(isCalculate)
        Me.TotalSum_Sum_Volume = Report1.TotalSum_Sum_Volume.GetTotalValue(isCalculate)
        Me.TotalSum_Count_Sales = Report1.TotalSum_Count_Sales.GetTotalValue(isCalculate)
    End Sub

    Public Sub ChangeTotalControls()
        Me.Report_TotalRecords = Report1.Report_TotalRecords.Value
        Me.Sum_Sum_money1 = Report1.Sum_Sum_money1.Value
        Me.Sum_Sum_Volume1 = Report1.Sum_Sum_Volume1.Value
        Me.Sum_Count_Sales1 = Report1.Sum_Count_Sales1.Value
        Me.Sum_Sum_money = Report1.Sum_Sum_money.Value
        Me.Sum_Sum_Volume = Report1.Sum_Sum_Volume.Value
        Me.Sum_Count_Sales = Report1.Sum_Count_Sales.Value
        Me.TotalSum_Sum_money = Report1.TotalSum_Sum_money.Value
        Me.TotalSum_Sum_Volume = Report1.TotalSum_Sum_Volume.Value
        Me.TotalSum_Count_Sales = Report1.TotalSum_Count_Sales.Value
    End Sub

    Public Property Get IsOpen
        IsOpen = mOpen
    End Property

    Public Property Get IsClose
        IsClose = mClose
    End Property

    Public Property Let IsOpen(Value)
        mOpen = Value
        mClose = Not Value
    End Property

    Public Property Let IsClose(Value)
        mClose = Value
        mOpen = Not Value
    End Property

End Class
'End Report1 clsReportGroup

'clsReport1GroupsCollection @2-F629730D
Class clsReport1GroupsCollection
    Public Groups
    Private mPageCurrentHeaderIndex
    Private mReportCurrentHeaderIndex
    Private mSite_NameCurrentHeaderIndex
    Private mGrade_NameCurrentHeaderIndex
    Private CurrentPageSize
    Public PageSize
    Public TotalPages
    Public TotalRows
    Public StartIndex
    Public EndIndex
    Public CurrentPage
    Private Sub Class_Initialize()
        TotalRows = 0: TotalPages = 0: StartIndex = -1: EndIndex = 0
        Set Groups = CreateObject("Scripting.Dictionary")
        mSite_NameCurrentHeaderIndex = 2
        mGrade_NameCurrentHeaderIndex = 3
        mReportCurrentHeaderIndex = 0
        mPageCurrentHeaderIndex = 1
        CurrentPageSize = 0
    End Sub

    Private Function InitGroup()
        Dim group
        Set group = New clsReportGroupReport1
        group.RowNumber = TotalRows
        group.PageNumber = TotalPages
        group.ReportTotalIndex = mReportCurrentHeaderIndex
        group.PageTotalIndex = mPageCurrentHeaderIndex
        group.Site_NameTotalIndex = mSite_NameCurrentHeaderIndex
        group.Grade_NameTotalIndex = mGrade_NameCurrentHeaderIndex
        Set InitGroup = group
    End Function

    Public Sub OpenPage()
        Dim Group
        Dim OpenFlag
        CurrentPageSize = CurrentPageSize + Report1.Page_Header.Height
        TotalPages = TotalPages + 1
        If TotalPages = CurrentPage And StartIndex = -1 Then StartIndex = Groups.Count
        Set Group = InitGroup()
            Group.SetTotalControls False
        CCSEventResult = CCRaiseEvent(Report1.CCSEvents, "Page_Header_OnCalculate", Me)
        Group.SetControls
        Group.IsOpen = True
        mPageCurrentHeaderIndex = Groups.Count
        Group.GroupType ="Page"
        Groups.Add Groups.Count,Group
    End Sub

    Public Sub OpenGroup(groupName)
        Dim Group
        Dim OpenFlag
        If groupName = "Report" Then
            If TotalPages =  0 And CurrentPage=1 Then StartIndex = 0
            CurrentPageSize = CurrentPageSize + Report1.Report_Header.Height
            Set Group = InitGroup()
            Group.SetTotalControls False
            CCSEventResult = CCRaiseEvent(Report1.CCSEvents, "Report_Header_OnCalculate", Me)
            Group.SetControls 
            mReportCurrentHeaderIndex = Groups.Count
            Group.IsOpen = True
            Group.GroupType ="Report"
            Groups.Add Groups.Count,Group
            OpenPage
        End If
        If groupName = "Site_Name" Then
            If PageSize > 0 And Report1.Site_Name_Header.Visible And CurrentPageSize + Report1.Page_Footer.Height + Report1.Site_Name_Header.Height > PageSize Then
                ClosePage
                OpenPage
            End If
            CurrentPageSize = CurrentPageSize + Report1.Site_Name_Header.Height
            If TotalPages = CurrentPage And StartIndex = -1 Then StartIndex = Groups.Count
            Set Group = InitGroup()
            Group.SetTotalControls False
            CCSEventResult = CCRaiseEvent(Report1.CCSEvents, "Site_Name_Header_OnCalculate", Me)
            Group.SetControls 
            Group.IsOpen = True
            OpenFlag = True
            mSite_NameCurrentHeaderIndex = Groups.Count
            Group.GroupType ="Site_Name"
            Groups.Add Groups.Count,Group
        End If
        If groupName = "Grade_Name" Or OpenFlag Then
            If PageSize > 0 And Report1.Grade_Name_Header.Visible And CurrentPageSize + Report1.Page_Footer.Height + Report1.Grade_Name_Header.Height > PageSize Then
                ClosePage
                OpenPage
            End If
            CurrentPageSize = CurrentPageSize + Report1.Grade_Name_Header.Height
            If TotalPages = CurrentPage And StartIndex = -1 Then StartIndex = Groups.Count
            Set Group = InitGroup()
            Group.SetTotalControls False
            CCSEventResult = CCRaiseEvent(Report1.CCSEvents, "Grade_Name_Header_OnCalculate", Me)
            Group.SetControls 
            Group.IsOpen = True
            mGrade_NameCurrentHeaderIndex = Groups.Count
            Group.GroupType ="Grade_Name"
            Groups.Add Groups.Count,Group
        End If
    End Sub

    Public Sub ClosePage
        Dim Group
        Set Group = InitGroup()
        CurrentPageSize = 0
        If Groups(Groups.Count -1).IsClose And Groups(Groups.Count -1).GroupType="Report" And StartIndex < 0 Then StartIndex = mPageCurrentHeaderIndex
        If StartIndex > -1 And EndIndex = 0 Then EndIndex = Groups.Count
        Group.SetTotalControls False
        Group.SyncWithHeader Groups(mPageCurrentHeaderIndex)
        CCSEventResult = CCRaiseEvent(Report1.CCSEvents, "Page_Footer_OnCalculate", Me)
        RestoreValues
        Group.IsClose = True
        Group.GroupType ="Page"
        Groups.Add Groups.Count,Group
    End Sub

    Public Sub CloseGroup(groupName)
        Dim Group
        If groupName = "Report" Then
            If PageSize > 0 And Report1.Report_Footer.Visible And CurrentPageSize + Report1.Page_Footer.Height + Report1.Report_Footer.Height > PageSize Then
                ClosePage:OpenPage
            End If
            CurrentPageSize = CurrentPageSize + Report1.Report_Footer.Height
            Set Group = InitGroup()
            Group.SetTotalControls False
            Group.SyncWithHeader Groups(mReportCurrentHeaderIndex)
            CCSEventResult = CCRaiseEvent(Report1.CCSEvents, "Report_Footer_OnCalculate", Me)
            RestoreValues
            Group.IsClose = True
            Group.GroupType ="Report"
            Groups.Add Groups.Count,Group
            ClosePage
            Exit Sub
        End If
        If PageSize > 0 And Report1.Grade_Name_Footer.Visible And CurrentPageSize + Report1.Page_Footer.Height + Report1.Grade_Name_Footer.Height > PageSize Then
            ClosePage:OpenPage
        End If
        CurrentPageSize = CurrentPageSize + Report1.Grade_Name_Footer.Height
        If TotalPages = CurrentPage And StartIndex = -1 Then StartIndex = Groups.Count
        Set Group = InitGroup()
        Group.SetTotalControls False
        Group.SyncWithHeader Groups(mGrade_NameCurrentHeaderIndex)
        CCSEventResult = CCRaiseEvent(Report1.CCSEvents, "Grade_Name_Footer_OnCalculate", Me)
        Report1.Sum_Sum_money1.Reset()
        Report1.Sum_Sum_Volume1.Reset()
        Report1.Sum_Count_Sales1.Reset()
        RestoreValues
        Group.IsClose = True
        Group.GroupType ="Grade_Name"
        Groups.Add Groups.Count,Group
        If groupName = "Grade_Name" Then Exit Sub
        If PageSize > 0 And Report1.Site_Name_Footer.Visible And CurrentPageSize + Report1.Page_Footer.Height + Report1.Site_Name_Footer.Height > PageSize Then
            ClosePage:OpenPage
        End If
        CurrentPageSize = CurrentPageSize + Report1.Site_Name_Footer.Height
        If TotalPages = CurrentPage And StartIndex = -1 Then StartIndex = Groups.Count
        Set Group = InitGroup()
        Group.SetTotalControls False
        Group.SyncWithHeader Groups(mSite_NameCurrentHeaderIndex)
        CCSEventResult = CCRaiseEvent(Report1.CCSEvents, "Site_Name_Footer_OnCalculate", Me)
        Report1.Sum_Sum_money.Reset()
        Report1.Sum_Sum_Volume.Reset()
        Report1.Sum_Count_Sales.Reset()
        RestoreValues
        Group.IsClose = True
        Group.GroupType ="Site_Name"
        Groups.Add Groups.Count,Group
    End Sub
    Public Sub RestoreValues
        Report1.Report_TotalRecords.Value = Report1.Report_TotalRecords.InitialValue
        Report1.Site_Name.Value = Report1.Site_Name.InitialValue
        Report1.Grade_Name.Value = Report1.Grade_Name.InitialValue
        Report1.Sale_Date.Value = Report1.Sale_Date.InitialValue
        Report1.Sum_money.Value = Report1.Sum_money.InitialValue
        Report1.Sum_Volume.Value = Report1.Sum_Volume.InitialValue
        Report1.Count_Sales.Value = Report1.Count_Sales.InitialValue
        Report1.Sum_Sum_money1.Value = Report1.Sum_Sum_money1.InitialValue
        Report1.Sum_Sum_Volume1.Value = Report1.Sum_Sum_Volume1.InitialValue
        Report1.Sum_Count_Sales1.Value = Report1.Sum_Count_Sales1.InitialValue
        Report1.Sum_Sum_money.Value = Report1.Sum_Sum_money.InitialValue
        Report1.Sum_Sum_Volume.Value = Report1.Sum_Sum_Volume.InitialValue
        Report1.Sum_Count_Sales.Value = Report1.Sum_Count_Sales.InitialValue
        Report1.TotalSum_Sum_money.Value = Report1.TotalSum_Sum_money.InitialValue
        Report1.TotalSum_Sum_Volume.Value = Report1.TotalSum_Sum_Volume.InitialValue
        Report1.TotalSum_Count_Sales.Value = Report1.TotalSum_Count_Sales.InitialValue
        Report1.Report_CurrentDate.Value = Report1.Report_CurrentDate.InitialValue
    End Sub

    Public Sub AddItem()
        If PageSize > 0 And Report1.Detail.Visible And CurrentPageSize + Report1.Page_Footer.Height + Report1.Detail.Height > PageSize Then
            ClosePage
            OpenPage
        End If
        CurrentPageSize = CurrentPageSize + Report1.Detail.Height
        If TotalPages = CurrentPage And StartIndex = -1 Then StartIndex = Groups.Count
        Dim Group
        TotalRows = TotalRows + 1
        Set Group = InitGroup()
        Group.SetTotalControls False
        CCSEventResult = CCRaiseEvent(Report1.CCSEvents, "Detail_OnCalculate", Me)
        Group.SetControls 
        Group.SetTotalControls True
        Groups.Add Groups.Count,Group
    End Sub
End Class
'End clsReport1GroupsCollection

Class clsReportReport1 'Report1 Class @2-D191C334

'Report1 Variables @2-29FAE996

    ' Private variables
    Private VarPageSize
    ' Public variables
    Public ComponentName, CCSEvents
    Public Visible, Errors
    Public ViewMode
    Public DataSource
    Private CurrentPageNumber
    Public Command
    Public TemplateBlock
    Public PageNumber, RowNumber, TotalRows, TotalPages
    Public IsDSEmpty
    Public UseClientPaging
    Public DetailBlock, Detail, Report_FooterBlock, Report_Footer, Report_HeaderBlock, Report_Header, Page_FooterBlock, Page_Footer, Page_HeaderBlock, Page_Header
    Public Site_Name_HeaderBlock, Site_Name_Header
    Public Site_Name_FooterBlock, Site_Name_Footer
    Public Grade_Name_HeaderBlock, Grade_Name_Header
    Public Grade_Name_FooterBlock, Grade_Name_Footer
    Public Recordset
    Public Attributes

    Private CCSEventResult
    Private AttributePrefix

    ' Report Controls
    Public StaticControls, RowControls, Report_FooterControls, Report_HeaderControls
    Public Page_FooterControls, Page_HeaderControls
    Public Site_Name_HeaderControls, Site_Name_FooterControls
    Public Grade_Name_HeaderControls, Grade_Name_FooterControls
    Dim Report_TotalRecords
    Dim Site_Name
    Dim Grade_Name
    Dim Sale_Date
    Dim Sum_money
    Dim Sum_Volume
    Dim Count_Sales
    Dim Sum_Sum_money1
    Dim Sum_Sum_Volume1
    Dim Sum_Count_Sales1
    Dim Sum_Sum_money
    Dim Sum_Sum_Volume
    Dim Sum_Count_Sales
    Dim NoRecords
    Dim TotalSum_Sum_money
    Dim TotalSum_Sum_Volume
    Dim TotalSum_Count_Sales
    Dim Report_CurrentDate
'End Report1 Variables

'Report1 Class_Initialize Event @2-2A491ECB
    Private Sub Class_Initialize()
        ComponentName = "Report1"
        Dim MaxSectionSize : MaxSectionSize = 0
        Dim MinPageSize : MinPageSize = 0
        Visible = True
        Set Detail = new clsSection
        Detail.Visible = True
        Detail.Height = 1
        MaxSectionSize = Max(MaxSectionSize, Detail.Height)
        Set Report_Footer = new clsSection
        Report_Footer.Visible = True
        Report_Footer.Height = 1
        MaxSectionSize = Max(MaxSectionSize, Report_Footer.Height)
        Set Report_Header = new clsSection
        Report_Header.Visible = True
        Report_Header.Height = 0
        MaxSectionSize = Max (MaxSectionSize, Report_Header.Height)
        Set Page_Footer = new clsSection
        Page_Footer.Visible = True
        Page_Footer.Height = 1
        MinPageSize = MinPageSize + Page_Footer.Height
        Set Page_Header = new clsSection
        Page_Header.Visible = True
        Page_Header.Height = 1
        MinPageSize = MinPageSize + Page_Header.Height
        Set Site_Name_Footer = new clsSection
        Site_Name_Footer.Visible = True
        Site_Name_Footer.Height = 1
        MaxSectionSize = Max(MaxSectionSize, Site_Name_Footer.Height)
        Set Site_Name_Header = new clsSection
        Site_Name_Header.Visible = True
        Site_Name_Header.Height = 1
        MaxSectionSize = Max(MaxSectionSize, Site_Name_Header.Height)
        Set Grade_Name_Footer = new clsSection
        Grade_Name_Footer.Visible = True
        Grade_Name_Footer.Height = 1
        MaxSectionSize = Max(MaxSectionSize, Grade_Name_Footer.Height)
        Set Grade_Name_Header = new clsSection
        Grade_Name_Header.Visible = True
        Grade_Name_Header.Height = 1
        MaxSectionSize = Max(MaxSectionSize, Grade_Name_Header.Height)
        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Attributes = New clsAttributes
        AttributePrefix = ComponentName & ":"
        Set Errors = New clsErrors
        Set DataSource = New clsReport1DataSource
        Set Command = New clsCommand
        Dim defaultPage
        MinPageSize = MinPageSize + MaxSectionSize
        ViewMode = CCGetParam("ViewMode", "Web")
        If ViewMode = "Print" Then
            defaultPage = 50
        Else
            defaultPage = 40
        End If
        PageSize = CCGetParam(ComponentName & "PageSize", defaultPage)
        If Not IsNumeric(PageSize) Or IsEmpty(PageSize) Then
            PageSize = defaultPage
        Else
            PageSize =  CInt(PageSize)
        End If
        If PageSize = 0 Then
            PageSize = 100
        ElseIf PageSize < 0 Then 
            PageSize = defaultPage
        End If
        If PageSize > 0 And PageSize < MinPageSize Then PageSize = MinPageSize
        CurrentPageNumber = CCGetParam(ComponentName & "Page", 1)
        If Not IsNumeric(CurrentPageNumber) And Len(CurrentPageNumber) > 0 Then
            CurrentPageNumber = 1
        ElseIf Len(CurrentPageNumber) > 0 Then
            If CurrentPageNumber > 0 Then
                CurrentPageNumber = CInt(CurrentPageNumber)
            Else
                CurrentPageNumber = 1
            End If
        Else
            CurrentPageNumber = 1
        End If

        Set Report_TotalRecords = CCCreateReportLabel( "Report_TotalRecords", Empty, ccsText, Empty, CCGetRequestParam("Report_TotalRecords", ccsGet), "Count",  False, True,"")
        Set Site_Name = CCCreateReportLabel( "Site_Name", Empty, ccsText, Empty, CCGetRequestParam("Site_Name", ccsGet), "",  False, False,"")
        Set Grade_Name = CCCreateReportLabel( "Grade_Name", Empty, ccsText, Empty, CCGetRequestParam("Grade_Name", ccsGet), "",  False, False,"")
        Set Sale_Date = CCCreateReportLabel( "Sale_Date", Empty, ccsDate, DefaultDateFormat, CCGetRequestParam("Sale_Date", ccsGet), "",  False, False,"")
        Set Sum_money = CCCreateReportLabel( "Sum_money", Empty, ccsFloat, Array(False, 2, True, False, False, "$", "", 1, True, ""), CCGetRequestParam("Sum_money", ccsGet), "",  False, False,"")
        Set Sum_Volume = CCCreateReportLabel( "Sum_Volume", Empty, ccsFloat, Array(False, 2, True, False, True, "", "", 1, True, ""), CCGetRequestParam("Sum_Volume", ccsGet), "",  False, False,"")
        Set Count_Sales = CCCreateReportLabel( "Count_Sales", Empty, ccsInteger, Empty, CCGetRequestParam("Count_Sales", ccsGet), "",  False, False,"")
        Set Sum_Sum_money1 = CCCreateReportLabel( "Sum_Sum_money1", Empty, ccsFloat, Array(False, 2, True, False, False, "$", "", 1, True, ""), CCGetRequestParam("Sum_Sum_money1", ccsGet), "Sum",  False, False,"")
        Set Sum_Sum_Volume1 = CCCreateReportLabel( "Sum_Sum_Volume1", Empty, ccsFloat, Array(False, 2, True, False, True, "", "", 1, True, ""), CCGetRequestParam("Sum_Sum_Volume1", ccsGet), "Sum",  False, False,"")
        Set Sum_Count_Sales1 = CCCreateReportLabel( "Sum_Count_Sales1", Empty, ccsInteger, Empty, CCGetRequestParam("Sum_Count_Sales1", ccsGet), "Sum",  False, False,"")
        Set Sum_Sum_money = CCCreateReportLabel( "Sum_Sum_money", Empty, ccsFloat, Array(False, 2, True, False, False, "$", "", 1, True, ""), CCGetRequestParam("Sum_Sum_money", ccsGet), "Sum",  False, False,"")
        Set Sum_Sum_Volume = CCCreateReportLabel( "Sum_Sum_Volume", Empty, ccsFloat, Array(False, 2, True, False, True, "", "", 1, True, ""), CCGetRequestParam("Sum_Sum_Volume", ccsGet), "Sum",  False, False,"")
        Set Sum_Count_Sales = CCCreateReportLabel( "Sum_Count_Sales", Empty, ccsInteger, Empty, CCGetRequestParam("Sum_Count_Sales", ccsGet), "Sum",  False, False,"")
        Set NoRecords = CCCreatePanel("NoRecords")
        Set TotalSum_Sum_money = CCCreateReportLabel( "TotalSum_Sum_money", Empty, ccsFloat, Array(False, 2, True, False, False, "$", "", 1, True, ""), CCGetRequestParam("TotalSum_Sum_money", ccsGet), "Sum",  False, False,"")
        Set TotalSum_Sum_Volume = CCCreateReportLabel( "TotalSum_Sum_Volume", Empty, ccsFloat, Array(False, 2, True, False, True, "", "", 1, True, ""), CCGetRequestParam("TotalSum_Sum_Volume", ccsGet), "Sum",  False, False,"")
        Set TotalSum_Count_Sales = CCCreateReportLabel( "TotalSum_Count_Sales", Empty, ccsInteger, Empty, CCGetRequestParam("TotalSum_Count_Sales", ccsGet), "Sum",  False, False,"")
        Set Report_CurrentDate = CCCreateReportLabel( "Report_CurrentDate", Empty, ccsDate, Array("ShortDate"), CCGetRequestParam("Report_CurrentDate", ccsGet), "",  False, False,"")
        IsDSEmpty = True
        UseClientPaging = False
    End Sub
'End Report1 Class_Initialize Event

'Report1 Initialize Method @2-05754AED
    Sub Initialize(objConnection)
        If NOT Visible Then Exit Sub

        Set DataSource.Connection = objConnection
    End Sub
'End Report1 Initialize Method

'Report1 Class_Terminate Event @2-8595EA66
    Private Sub Class_Terminate()
        Set DataSource = Nothing
        Set Command = Nothing
        Set Attributes = Nothing
        Set Errors = Nothing
    End Sub
'End Report1 Class_Terminate Event

'Report1 Show Method @2-B8088CDF
    Sub Show(Tpl)
        If NOT Visible Then Exit Sub

        Dim RecordCounter

        With DataSource
            .Parameters("urlsite") = CCGetRequestParam("site", ccsGET)
            .Parameters("urlprod") = CCGetRequestParam("prod", ccsGET)
            .Parameters("urldate") = CCGetRequestParam("date", ccsGET)
            .Parameters("urldate1") = CCGetRequestParam("date1", ccsGET)
        End With

        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeSelect", Me)
        Set Recordset = DataSource.Open(Command)
        IsDSEmpty = Recordset.EOF

        Set TemplateBlock = Tpl.Block("Report " & ComponentName)
        Set Report_HeaderBlock = TemplateBlock.Block("Section Report_Header")
        Set Report_FooterBlock = TemplateBlock.Block("Section Report_Footer")
        Set Page_HeaderBlock = TemplateBlock.Block("Section Page_Header")
        Set Page_FooterBlock = TemplateBlock.Block("Section Page_Footer")
        Set Site_Name_HeaderBlock = TemplateBlock.Block("Section Site_Name_Header")
        Set Site_Name_FooterBlock = TemplateBlock.Block("Section Site_Name_Footer")
        Set Grade_Name_HeaderBlock = TemplateBlock.Block("Section Grade_Name_Header")
        Set Grade_Name_FooterBlock = TemplateBlock.Block("Section Grade_Name_Footer")
        Set DetailBlock = TemplateBlock.Block("Section Detail")
        Set RowControls = CCCreateCollection(DetailBlock, Null, ccsParseAccumulate, _
            Array(Sale_Date, Sum_money, Sum_Volume, Count_Sales))
        Set Report_FooterControls = CCCreateCollection(Report_FooterBlock, DetailBlock, ccsParseAccumulate, _
            Array(NoRecords, TotalSum_Sum_money, TotalSum_Sum_Volume, TotalSum_Count_Sales))
        Set Report_HeaderControls = CCCreateCollection(Report_HeaderBlock, DetailBlock, ccsParseAccumulate, _
            Array(Report_TotalRecords))
        Set Page_FooterControls = CCCreateCollection(Page_FooterBlock, DetailBlock, ccsParseAccumulate, _
            Array(Report_CurrentDate))
        Set Site_Name_HeaderControls = CCCreateCollection(Site_Name_HeaderBlock, DetailBlock, ccsParseAccumulate, _
            Array(Site_Name))
        Set Site_Name_FooterControls = CCCreateCollection(Site_Name_FooterBlock, DetailBlock, ccsParseAccumulate, _
            Array(Sum_Sum_money, Sum_Sum_Volume, Sum_Count_Sales))
        Set Grade_Name_HeaderControls = CCCreateCollection(Grade_Name_HeaderBlock, DetailBlock, ccsParseAccumulate, _
            Array(Grade_Name))
        Set Grade_Name_FooterControls = CCCreateCollection(Grade_Name_FooterBlock, DetailBlock, ccsParseAccumulate, _
            Array(Sum_Sum_money1, Sum_Sum_Volume1, Sum_Count_Sales1))
        Dim Site_NameKey
        Dim Grade_NameKey
        Dim Groups
        Set Groups = New clsReport1GroupsCollection
        Groups.CurrentPage = CurrentPageNumber
        If PageSize > 0 Then Groups.PageSize = PageSize
        Errors.AddErrors DataSource.Errors
        If Errors.Count > 0 Then
            TemplateBlock.HTML = CCFormatError("Report Report1", Errors)
        Else
            Do While Not Recordset.EOF
                Site_Name.Value = Recordset.Fields("Site_Name")
                Grade_Name.Value = Recordset.Fields("Grade_Name")
                Sale_Date.Value = Recordset.Fields("Sale_Date")
                Sum_money.Value = Recordset.Fields("Sum_money")
                Sum_Volume.Value = Recordset.Fields("Sum_Volume")
                Count_Sales.Value = Recordset.Fields("Count_Sales")
                Sum_Sum_money1.Value = Recordset.Fields("Sum_Sum_money1")
                Sum_Sum_Volume1.Value = Recordset.Fields("Sum_Sum_Volume1")
                Sum_Count_Sales1.Value = Recordset.Fields("Sum_Count_Sales1")
                Sum_Sum_money.Value = Recordset.Fields("Sum_Sum_money")
                Sum_Sum_Volume.Value = Recordset.Fields("Sum_Sum_Volume")
                Sum_Count_Sales.Value = Recordset.Fields("Sum_Count_Sales")
                TotalSum_Sum_money.Value = Recordset.Fields("TotalSum_Sum_money")
                TotalSum_Sum_Volume.Value = Recordset.Fields("TotalSum_Sum_Volume")
                TotalSum_Count_Sales.Value = Recordset.Fields("TotalSum_Count_Sales")
                Report_CurrentDate.Value = Recordset.Fields("Report_CurrentDate")
                Report_TotalRecords.Value = 1
                If Groups.Groups.Count = 0 Then Groups.OpenGroup "Report"
                If Groups.Groups.Count = 2 Or Site_NameKey <> Recordset.Fields("Site_Name") Then
                    Groups.OpenGroup "Site_Name"
                ElseIf Grade_NameKey <> Recordset.Fields("Grade_Name") Then
                    Groups.OpenGroup "Grade_Name"
                End If
                Groups.AddItem 
                Site_NameKey = Recordset.Fields("Site_Name")
                Grade_NameKey = Recordset.Fields("Grade_Name")
                Recordset.MoveNext
                If Site_NameKey <> Recordset.Fields("Site_Name") Or Recordset.EOF Then
                    Groups.CloseGroup "Site_Name"
                ElseIf Grade_NameKey <> Recordset.Fields("Grade_Name") Then
                    Groups.CloseGroup "Grade_Name"
                End If
            Loop
            If Groups.Groups.Count = 0 Then Groups.OpenGroup "Report"
            Groups.CloseGroup "Report"

            CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShow", Me)
            If NOT Visible Then Exit Sub

            RowControls.PreserveControlsVisible
            TotalPages = Groups.TotalPages
            TotalRows = Groups.TotalRows
            Dim i,k, StartItem, EndItem, LastValueInd
            Dim items
            items = Groups.Groups.Items
            If PageSize <> 0 And ViewMode = "Web" Then
                StartItem = Groups.StartIndex 
                EndItem = Groups.EndIndex
                If EndItem > UBound(items) Then EndItem = UBound(items)
            Else
                StartItem = 0
                EndItem = UBound(items)
            End If
            LastValueInd = 0
            For i=0 To UBound(items)
                RowNumber = items(i).RowNumber
                PageNumber = items(i).PageNumber
                Select Case items(i).GroupType
                    Case ""
                        Sale_Date.Value = items(i).Sale_Date
                        Sum_money.Value = items(i).Sum_money
                        Sum_Volume.Value = items(i).Sum_Volume
                        Count_Sales.Value = items(i).Count_Sales
                        If i >= StartItem And i<= EndItem Then CCSEventResult = CCRaiseEvent(CCSEvents, "Detail_BeforeShow", Me)
                        If Detail.Visible And i >= StartItem And i<= EndItem Then
                            Attributes.Show DetailBlock, AttributePrefix
                            RowControls.Show
                        End If
                        LastValueInd = i
                    Case "Report"
                        Report_TotalRecords.Value = items(i).Report_TotalRecords
                        TotalSum_Sum_money.Value = items(i).TotalSum_Sum_money
                        TotalSum_Sum_Volume.Value = items(i).TotalSum_Sum_Volume
                        TotalSum_Count_Sales.Value = items(i).TotalSum_Count_Sales
                        If items(i).IsOpen Then
                            If i >= StartItem And i<= EndItem Then CCSEventResult = CCRaiseEvent(CCSEvents, "Report_Header_BeforeShow", Me)
                            If Report_Header.Visible  And i >= StartItem And i<= EndItem Then 
                                Attributes.Show Report_HeaderBlock, AttributePrefix
                                Report_HeaderControls.Show
                            End If
                        End If
                        If items(i).IsClose Then
                            If i >= StartItem And i<= EndItem Then CCSEventResult = CCRaiseEvent(CCSEvents, "Report_Footer_BeforeShow", Me)
                            If Report_Footer.Visible And i >= StartItem And i<= EndItem Then 
                                Attributes.Show Report_FooterBlock, AttributePrefix
                                Report_FooterControls.Show
                            End If
                        End If
                    Case "Page"
                        Report_CurrentDate.Value = Date
                        If items(i).IsOpen Then
                            If i >= StartItem And i<= EndItem Then CCSEventResult = CCRaiseEvent(CCSEvents, "Page_Header_BeforeShow", Me)
                            If Page_Header.Visible  And i >= StartItem And i<= EndItem Then 
                                Attributes.Show Page_HeaderBlock, AttributePrefix
                                Page_HeaderBlock.ParseTo ccsParseAccumulate, DetailBlock
                            End If
                        End If
                        If (items(i).IsClose And Not UseClientPaging) Or (items(i).IsOpen And UseClientPaging) Then
                            If i >= StartItem And i<= EndItem Then CCSEventResult = CCRaiseEvent(CCSEvents, "Page_Footer_BeforeShow", Me)
                            If Page_Footer.Visible And i >= StartItem And i<= EndItem Then 
                                Attributes.Show Page_FooterBlock, AttributePrefix
                                Page_FooterControls.Show
                            End If
                        End If
                        NoRecords.Visible = Recordset.EOF And Recordset.BOF And items(i).IsOpen
                    Case "Site_Name"
                        Site_Name.Value = items(i).Site_Name
                        Sum_Sum_money.Value = items(i).Sum_Sum_money
                        Sum_Sum_Volume.Value = items(i).Sum_Sum_Volume
                        Sum_Count_Sales.Value = items(i).Sum_Count_Sales
                        If items(i).IsOpen Then
                            If i >= StartItem And i<= EndItem Then CCSEventResult = CCRaiseEvent(CCSEvents, "Site_Name_Header_BeforeShow", Me)
                            If Site_Name_Header.Visible  And i >= StartItem And i<= EndItem Then 
                                Attributes.Show Site_Name_HeaderBlock, AttributePrefix
                                Site_Name_HeaderControls.Show
                            End If
                        End If
                        If items(i).IsClose Then
                            If i >= StartItem And i<= EndItem Then CCSEventResult = CCRaiseEvent(CCSEvents, "Site_Name_Footer_BeforeShow", Me)
                            If Site_Name_Footer.Visible And i >= StartItem And i<= EndItem Then 
                                Attributes.Show Site_Name_FooterBlock, AttributePrefix
                                Site_Name_FooterControls.Show
                            End If
                        End If
                    Case "Grade_Name"
                        Grade_Name.Value = items(i).Grade_Name
                        Sum_Sum_money1.Value = items(i).Sum_Sum_money1
                        Sum_Sum_Volume1.Value = items(i).Sum_Sum_Volume1
                        Sum_Count_Sales1.Value = items(i).Sum_Count_Sales1
                        If items(i).IsOpen Then
                            If i >= StartItem And i<= EndItem Then CCSEventResult = CCRaiseEvent(CCSEvents, "Grade_Name_Header_BeforeShow", Me)
                            If Grade_Name_Header.Visible  And i >= StartItem And i<= EndItem Then 
                                Attributes.Show Grade_Name_HeaderBlock, AttributePrefix
                                Grade_Name_HeaderControls.Show
                            End If
                        End If
                        If items(i).IsClose Then
                            If i >= StartItem And i<= EndItem Then CCSEventResult = CCRaiseEvent(CCSEvents, "Grade_Name_Footer_BeforeShow", Me)
                            If Grade_Name_Footer.Visible And i >= StartItem And i<= EndItem Then 
                                Attributes.Show Grade_Name_FooterBlock, AttributePrefix
                                Grade_Name_FooterControls.Show
                            End If
                        End If
                End Select
            Next
            TemplateBlock.Parse ccsParseOverwrite
        End If

    End Sub
'End Report1 Show Method

'Report1 PageSize Property Let @2-54E46DD6
    Public Property Let PageSize(NewValue)
        VarPageSize = NewValue
        DataSource.PageSize = NewValue
    End Property
'End Report1 PageSize Property Let

'Report1 PageSize Property Get @2-9AA1D1E9
    Public Property Get PageSize()
        PageSize = VarPageSize
    End Property
'End Report1 PageSize Property Get

End Class 'End Report1 Class @2-A61BA892

Class clsReport1DataSource 'Report1DataSource Class @2-1489A126

'DataSource Variables @2-C11C9A76
    Public Errors, Connection, Parameters, CCSEvents

    Public Recordset
    Public SQL, CountSQL, Order, Where, Orders, StaticOrder
    Public PageSize
    Public PageCount
    Public AbsolutePage
    Public Fields
    Dim WhereParameters
    Public AllParamsSet
    Public CmdExecution
    Public InsertOmitIfEmpty
    Public UpdateOmitIfEmpty

    Private CurrentOperation
    Private CCSEventResult

    ' Datasource fields
    Public Site_Name
    Public Grade_Name
    Public Sale_Date
    Public Sum_money
    Public Sum_Volume
    Public Count_Sales
    Public Sum_Sum_money1
    Public Sum_Sum_Volume1
    Public Sum_Count_Sales1
    Public Sum_Sum_money
    Public Sum_Sum_Volume
    Public Sum_Count_Sales
    Public TotalSum_Sum_money
    Public TotalSum_Sum_Volume
    Public TotalSum_Count_Sales
'End DataSource Variables

'DataSource Class_Initialize Event @2-4F42FE19
    Private Sub Class_Initialize()

        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Fields = New clsFields
        Set Recordset = New clsDataSource
        Set Recordset.DataSource = Me
        Set Errors = New clsErrors
        Set Connection = Nothing
        AllParamsSet = True
        Set Site_Name = CCCreateField("Site_Name", "Site_Name", ccsText, Empty, Recordset)
        Set Grade_Name = CCCreateField("Grade_Name", "Grade_Name", ccsText, Empty, Recordset)
        Set Sale_Date = CCCreateField("Sale_Date", "Sale_Date", ccsDate, Array("yyyy", "-", "mm", "-", "dd", " ", "HH", ":", "nn", ":", "ss"), Recordset)
        Set Sum_money = CCCreateField("Sum_money", "Sum_money", ccsFloat, Empty, Recordset)
        Set Sum_Volume = CCCreateField("Sum_Volume", "Sum_Volume", ccsFloat, Empty, Recordset)
        Set Count_Sales = CCCreateField("Count_Sales", "Count_Sales", ccsInteger, Empty, Recordset)
        Set Sum_Sum_money1 = CCCreateField("Sum_Sum_money1", "Sum_money", ccsFloat, Empty, Recordset)
        Set Sum_Sum_Volume1 = CCCreateField("Sum_Sum_Volume1", "Sum_Volume", ccsFloat, Empty, Recordset)
        Set Sum_Count_Sales1 = CCCreateField("Sum_Count_Sales1", "Count_Sales", ccsInteger, Empty, Recordset)
        Set Sum_Sum_money = CCCreateField("Sum_Sum_money", "Sum_money", ccsFloat, Empty, Recordset)
        Set Sum_Sum_Volume = CCCreateField("Sum_Sum_Volume", "Sum_Volume", ccsFloat, Empty, Recordset)
        Set Sum_Count_Sales = CCCreateField("Sum_Count_Sales", "Count_Sales", ccsInteger, Empty, Recordset)
        Set TotalSum_Sum_money = CCCreateField("TotalSum_Sum_money", "Sum_money", ccsFloat, Empty, Recordset)
        Set TotalSum_Sum_Volume = CCCreateField("TotalSum_Sum_Volume", "Sum_Volume", ccsFloat, Empty, Recordset)
        Set TotalSum_Count_Sales = CCCreateField("TotalSum_Count_Sales", "Count_Sales", ccsInteger, Empty, Recordset)
        Fields.AddFields Array(Site_Name,  Grade_Name,  Sale_Date,  Sum_money,  Sum_Volume,  Count_Sales,  Sum_Sum_money1, _
             Sum_Sum_Volume1,  Sum_Count_Sales1,  Sum_Sum_money,  Sum_Sum_Volume,  Sum_Count_Sales,  TotalSum_Sum_money,  TotalSum_Sum_Volume,  TotalSum_Count_Sales)
        Set Parameters = Server.CreateObject("Scripting.Dictionary")
        Set WhereParameters = Nothing

        SQL = "SELECT DAY_DATE AS Sale_Date, SiteName AS Site_Name, grade_name AS Grade_Name, sum(money) AS Sum_money, sum(volume) AS Sum_Volume, " & vbLf & _
        "Count(sale_id) AS Count_Sales  " & vbLf & _
        "FROM Sites INNER JOIN (pump_sales INNER JOIN Grades ON " & vbLf & _
        "pump_sales.grade_id = Grades.grade_id) ON " & vbLf & _
        "Sites.ss_id = pump_sales.ss_id " & vbLf & _
        "WHERE SiteName LIKE '%{site}%' " & vbLf & _
        "AND grade_name LIKE '%{prod}%' " & vbLf & _
        "AND DAY_DATE >= '{date}' " & vbLf & _
        "AND DAY_DATE <= '{date1}' " & vbLf & _
        "GROUP BY SiteName, DAY_DATE, grade_name  {SQL_OrderBy}"
        CountSQL = "SELECT COUNT(*) FROM (SELECT DAY_DATE AS Sale_Date, SiteName AS Site_Name, grade_name AS Grade_Name, sum(money) AS Sum_money, sum(volume) AS Sum_Volume, " & vbLf & _
        "Count(sale_id) AS Count_Sales  " & vbLf & _
        "FROM Sites INNER JOIN (pump_sales INNER JOIN Grades ON " & vbLf & _
        "pump_sales.grade_id = Grades.grade_id) ON " & vbLf & _
        "Sites.ss_id = pump_sales.ss_id " & vbLf & _
        "WHERE SiteName LIKE '%{site}%' " & vbLf & _
        "AND grade_name LIKE '%{prod}%' " & vbLf & _
        "AND DAY_DATE >= '{date}' " & vbLf & _
        "AND DAY_DATE <= '{date1}' " & vbLf & _
        "GROUP BY SiteName, DAY_DATE, grade_name) cnt"
        Where = ""
        Order = "DAY_DATE"
        StaticOrder = "Site_Name asc,Grade_Name asc"
    End Sub
'End DataSource Class_Initialize Event

'BuildTableWhere Method @2-A48312A2
    Public Sub BuildTableWhere()
        If Not WhereParameters Is Nothing Then _
            Exit Sub
        Set WhereParameters = new clsSQLParameters
        With WhereParameters
            Set .Connection = Connection
            Set .ParameterSources = Parameters
            Set .DataSource = Me
            .AddParameter "site", "urlsite", ccsText, Empty, Empty, Empty, False
            .AddParameter "prod", "urlprod", ccsText, Empty, Empty, Empty, False
            .AddParameter "date", "urldate", ccsText, Empty, Empty, Empty, False
            .AddParameter "date1", "urldate1", ccsText, Empty, Empty, Empty, False
        End With
    End Sub
'End BuildTableWhere Method

'Open Method @2-486FAD2E
    Function Open(Cmd)
        Errors.Clear
        If Connection Is Nothing Then
            Set Open = New clsEmptyDataSource
            Exit Function
        End If
        Set Cmd.Connection = Connection
        Cmd.CommandOperation = cmdOpen
        Cmd.PageSize = PageSize
        Cmd.ActivePage = AbsolutePage
        Cmd.CommandType = dsSQL
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeBuildSelect", Me)
        Cmd.SQL = SQL
        Cmd.CountSQL =IIF(CountSQL <> "",  CountSQL, Empty)
        BuildTableWhere
        Set Cmd.WhereParameters = WhereParameters
        Cmd.Where = Where
        Cmd.OrderBy = Order
        If(Len(StaticOrder)>0) Then
            If Len(Order)>0 Then Cmd.OrderBy = ", "+Cmd.OrderBy
            Cmd.OrderBy = StaticOrder + Cmd.OrderBy
        End If
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeExecuteSelect", Me)
        If Errors.Count = 0 And CCSEventResult Then _
            Set Recordset = Cmd.Exec(Errors)
        CCSEventResult = CCRaiseEvent(CCSEvents, "AfterExecuteSelect", Me)
        Set Recordset.FieldsCollection = Fields
        Set Open = Recordset
    End Function
'End Open Method

'DataSource Class_Terminate Event @2-41B4B08D
    Private Sub Class_Terminate()
        If Recordset.State = adStateOpen Then _
            Recordset.Close
        Set Recordset = Nothing
        Set Parameters = Nothing
        Set Errors = Nothing
    End Sub
'End DataSource Class_Terminate Event

End Class 'End Report1DataSource Class @2-A61BA892


%>
