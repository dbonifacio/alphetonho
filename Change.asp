<%@ CodePage=1252 %>
<%
'Include Common Files @1-AA7D7934
%>
<!-- #INCLUDE VIRTUAL="/Alpheton/Common.asp"-->
<!-- #INCLUDE VIRTUAL="/Alpheton/Cache.asp" -->
<!-- #INCLUDE VIRTUAL="/Alpheton/Template.asp" -->
<!-- #INCLUDE VIRTUAL="/Alpheton/Sorter.asp" -->
<!-- #INCLUDE VIRTUAL="/Alpheton/Navigator.asp" -->
<!-- #INCLUDE VIRTUAL="/Alpheton/Services.asp" -->
<%
'End Include Common Files

'Initialize Page @1-5AC4F050
' Variables
Dim PathToRoot, ScriptPath, TemplateFilePath
Dim FileName
Dim Redirect
Dim IsService
Dim Tpl, HTMLTemplate
Dim TemplateFileName
Dim ComponentName
Dim PathToCurrentPage
Dim Attributes

' Events
Dim CCSEvents
Dim CCSEventResult

' Connections
Dim DBFusionHO

' Page controls
Dim changePass
Dim Header
Dim ChildControls

Session.CodePage = CCSLocales.Locale.CodePage
Response.Charset = CCSLocales.Locale.Charset
Response.ContentType = CCSContentType
IsService = False
Redirect = ""
TemplateFileName = "Change.html"
Set CCSEvents = CreateObject("Scripting.Dictionary")
PathToCurrentPage = "./"
FileName = "Change.asp"
PathToRoot = "./"
ScriptPath = Left(Request.ServerVariables("PATH_TRANSLATED"), Len(Request.ServerVariables("PATH_TRANSLATED")) - Len(FileName))
TemplateFilePath = ScriptPath
'End Initialize Page

'Initialize Objects @1-6ADAD7ED
BindEvents "Page"
CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeInitialize", Nothing)

Set DBFusionHO = New clsDBFusionHO
DBFusionHO.Open
Set Attributes = New clsAttributes
Attributes("pathToRoot") = PathToRoot

' Controls
Set changePass = new clsRecordchangePass
Set Header = New clsHeader
Set Header.Attributes = Attributes
Header.Initialize "Header", ""
changePass.Initialize DBFusionHO

' Events
%>
<!-- #INCLUDE VIRTUAL="/Alpheton/Change_events.asp" -->
<%
BindEvents Empty

CCSEventResult = CCRaiseEvent(CCSEvents, "AfterInitialize", Nothing)
'End Initialize Objects

'Execute Components @1-9D62A66B
changePass.Operation
Header.Operations
'End Execute Components

'Go to destination page @1-6D35F4FD
If NOT ( Redirect = "" ) Then
    UnloadPage
    Response.Redirect Redirect
End If
'End Go to destination page

'Initialize HTML Template @1-2E9DB4BC
CCSEventResult = CCRaiseEvent(CCSEvents, "OnInitializeView", Nothing)
Set HTMLTemplate = new clsTemplate
Set HTMLTemplate.Cache = TemplatesRepository
HTMLTemplate.LoadTemplate TemplateFilePath & TemplateFileName
HTMLTemplate.SetVar "@CCS_PathToRoot", PathToRoot
Set Tpl = HTMLTemplate.Block("main")
CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShow", Nothing)
'End Initialize HTML Template

'Show Page @1-1A88FF3C
Attributes.Show HTMLTemplate, "page:"
Set ChildControls = CCCreateCollection(Tpl, Null, ccsParseOverwrite, _
    Array(changePass, Header))
ChildControls.Show
Dim MainHTML
HTMLTemplate.Parse "main", False
If IsEmpty(MainHTML) Then MainHTML = HTMLTemplate.GetHTML("main")
CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeOutput", Nothing)
If CCSEventResult Then Response.Write MainHTML
'End Show Page

'Unload Page @1-CB210C62
UnloadPage
Set Tpl = Nothing
Set HTMLTemplate = Nothing
'End Unload Page

'UnloadPage Sub @1-50DA7F43
Sub UnloadPage()
    CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeUnload", Nothing)
    If DBFusionHO.State = adStateOpen Then _
        DBFusionHO.Close
    Set DBFusionHO = Nothing
    Set CCSEvents = Nothing
    Set Attributes = Nothing
    Set changePass = Nothing
    Header.UnloadPage
    Set Header = Nothing
End Sub
'End UnloadPage Sub

Class clsRecordchangePass 'changePass Class @2-66CB4C24

'changePass Variables @2-FF55E3F1

    ' Public variables
    Public ComponentName
    Public HTMLFormAction
    Public PressedButton
    Public Errors
    Public FormSubmitted
    Public EditMode
    Public Visible
    Public Recordset
    Public TemplateBlock
    Public Attributes

    Public CCSEvents
    Private CCSEventResult

    Public InsertAllowed
    Public UpdateAllowed
    Public DeleteAllowed
    Public ReadAllowed
    Public DataSource
    Public Command
    Public ValidatingControls
    Public Controls

    ' Class variables
    Dim Button_Update
    Dim Button_Cancel
    Dim usuario
    Dim opass
    Dim npass
    Dim cpass
    Dim pass
'End changePass Variables

'changePass Class_Initialize Event @2-66BB96D0
    Private Sub Class_Initialize()

        Visible = True
        Set Errors = New clsErrors
        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Attributes = New clsAttributes
        Set DataSource = New clschangePassDataSource
        Set Command = New clsCommand
        InsertAllowed = False
        UpdateAllowed = True
        DeleteAllowed = False
        ReadAllowed = True
        Dim Method
        Dim OperationMode
        OperationMode = Split(CCGetFromGet("ccsForm", Empty), ":")
        If UBound(OperationMode) > -1 Then 
            FormSubmitted = (OperationMode(0) = "changePass")
        End If
        If UBound(OperationMode) > 0 Then 
            EditMode = (OperationMode(1) = "Edit")
        End If
        ComponentName = "changePass"
        Method = IIf(FormSubmitted, ccsPost, ccsGet)
        Set Button_Update = CCCreateButton("Button_Update", Method)
        Set Button_Cancel = CCCreateButton("Button_Cancel", Method)
        Set usuario = CCCreateControl(ccsLabel, "usuario", Empty, ccsText, Empty, CCGetRequestParam("usuario", Method))
        Set opass = CCCreateControl(ccsTextBox, "opass", Empty, ccsText, Empty, CCGetRequestParam("opass", Method))
        opass.Required = True
        Set npass = CCCreateControl(ccsTextBox, "npass", Empty, ccsText, Empty, CCGetRequestParam("npass", Method))
        npass.Required = True
        Set cpass = CCCreateControl(ccsTextBox, "cpass", Empty, ccsText, Empty, CCGetRequestParam("cpass", Method))
        cpass.Required = True
        Set pass = CCCreateControl(ccsHidden, "pass", Empty, ccsInteger, Empty, CCGetRequestParam("pass", Method))
        Set ValidatingControls = new clsControls
        ValidatingControls.addControls Array(opass, npass, cpass, pass)
    End Sub
'End changePass Class_Initialize Event

'changePass Initialize Method @2-36D27969
    Sub Initialize(objConnection)

        If NOT Visible Then Exit Sub


        Set DataSource.Connection = objConnection
        With DataSource
        End With
    End Sub
'End changePass Initialize Method

'changePass Class_Terminate Event @2-0C5D276C
    Private Sub Class_Terminate()
        Set Errors = Nothing
        Set Attributes = Nothing
    End Sub
'End changePass Class_Terminate Event

'changePass Validate Method @2-B9D513CF
    Function Validate()
        Dim Validation
        ValidatingControls.Validate
        CCSEventResult = CCRaiseEvent(CCSEvents, "OnValidate", Me)
        Validate = ValidatingControls.isValid() And (Errors.Count = 0)
    End Function
'End changePass Validate Method

'changePass Operation Method @2-C69818F7
    Sub Operation()
        If NOT ( Visible AND FormSubmitted ) Then Exit Sub

        If FormSubmitted Then
            PressedButton = IIf(EditMode, "Button_Update", "Button_Cancel")
            If Button_Update.Pressed Then
                PressedButton = "Button_Update"
            ElseIf Button_Cancel.Pressed Then
                PressedButton = "Button_Cancel"
            End If
        End If
        Redirect = FileName & "?" & CCGetQueryString("QueryString", Array("ccsForm", "Button_Update.x", "Button_Update.y", "Button_Update", "Button_Cancel.x", "Button_Cancel.y", "Button_Cancel"))
        If PressedButton = "Button_Cancel" Then
            If NOT Button_Cancel.OnClick Then
                Redirect = ""
            End If
        ElseIf Validate() Then
            If PressedButton = "Button_Update" Then
                If NOT Button_Update.OnClick() OR NOT UpdateRow() Then
                    Redirect = ""
                End If
            End If
        Else
            Redirect = ""
        End If
    End Sub
'End changePass Operation Method

'changePass UpdateRow Method @2-2EAF7C2C
    Function UpdateRow()
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeUpdate", Me)
        If NOT UpdateAllowed Then UpdateRow = False : Exit Function
        With Command.Parameters
            .ParameterSources("ctrlnpass") = npass.Value
            .ParameterSources("ctrlopass") = opass.Value
            .ParameterSources("ctrlusuario") = usuario.Value
            .ParameterSources("ctrlpass") = pass.Value
        End With
        DataSource.Update(Command)


        CCSEventResult = CCRaiseEvent(CCSEvents, "AfterUpdate", Me)
        If DataSource.Errors.Count > 0 Then
            Errors.AddErrors(DataSource.Errors)
            DataSource.Errors.Clear
        End If
        UpdateRow = (Errors.Count = 0)
    End Function
'End changePass UpdateRow Method

'changePass Show Method @2-35B49896
    Sub Show(Tpl)

        If NOT Visible Then Exit Sub

        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeSelect", Me)
        Set Recordset = DataSource.Open(Command)
        EditMode = Recordset.EditMode(ReadAllowed)
        HTMLFormAction = FileName & "?" & CCAddParam(Request.ServerVariables("QUERY_STRING"), "ccsForm", "changePass" & IIf(EditMode, ":Edit", ""))
        Set TemplateBlock = Tpl.Block("Record " & ComponentName)
        If TemplateBlock is Nothing Then Exit Sub
        TemplateBlock.Variable("HTMLFormName") = ComponentName
        TemplateBlock.Variable("HTMLFormEnctype") ="application/x-www-form-urlencoded"
        If DataSource.Errors.Count > 0 Then
            Errors.AddErrors(DataSource.Errors)
            DataSource.Errors.Clear
            With TemplateBlock.Block("Error")
                .Variable("Error") = Errors.ToString
                .Parse False
            End With
        End If
        Set Controls = CCCreateCollection(TemplateBlock, Null, ccsParseOverwrite, _
            Array(usuario, pass, opass, npass, cpass, Button_Update, Button_Cancel))
        If EditMode AND ReadAllowed Then
            If Errors.Count = 0 Then
                If Recordset.Errors.Count > 0 Then
                    With TemplateBlock.Block("Error")
                        .Variable("Error") = Recordset.Errors.ToString
                        .Parse False
                    End With
                ElseIf Recordset.CanPopulate() Then
                    If Not FormSubmitted Then
                        
                        
                        
                        
                    End If
                Else
                    EditMode = False
                End If
            End If
            If EditMode Then
                
            End If
        End If
        If Not FormSubmitted Then
        End If
        If FormSubmitted Then
            Errors.AddErrors opass.Errors
            Errors.AddErrors npass.Errors
            Errors.AddErrors cpass.Errors
            Errors.AddErrors pass.Errors
            Errors.AddErrors DataSource.Errors
            With TemplateBlock.Block("Error")
                .Variable("Error") = Errors.ToString()
                .Parse False
            End With
        End If
        TemplateBlock.Variable("Action") = IIF(CCSUseAmps, Replace(HTMLFormAction, "&", CCSAmps), HTMLFormAction)
        Button_Update.Visible = EditMode AND UpdateAllowed

        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShow", Me)
        If Visible Then 
            Attributes.Show TemplateBlock, "changePass" & ":"
            Controls.Show
        End If
    End Sub
'End changePass Show Method

End Class 'End changePass Class @2-A61BA892

Class clschangePassDataSource 'changePassDataSource Class @2-7F33BB03

'DataSource Variables @2-51A7ED8F
    Public Errors, Connection, Parameters, CCSEvents

    Public Recordset
    Public SQL, CountSQL, Order, Where, Orders, StaticOrder
    Public PageSize
    Public PageCount
    Public AbsolutePage
    Public Fields
    Dim WhereParameters
    Public AllParamsSet
    Public CmdExecution
    Public InsertOmitIfEmpty
    Public UpdateOmitIfEmpty

    Private CurrentOperation
    Private CCSEventResult

    ' Datasource fields
'End DataSource Variables

'DataSource Class_Initialize Event @2-3B7A5B77
    Private Sub Class_Initialize()

        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Fields = New clsFields
        Set Recordset = New clsDataSource
        Set Recordset.DataSource = Me
        Set Errors = New clsErrors
        Set Connection = Nothing
        AllParamsSet = True
        Fields.AddFields Array()

        SQL = "SELECT *  " & vbLf & _
        "FROM Users {SQL_Where} {SQL_OrderBy}"
        Where = ""
        Order = ""
        StaticOrder = ""
    End Sub
'End DataSource Class_Initialize Event

'BuildTableWhere Method @2-98E5A92F
    Public Sub BuildTableWhere()
    End Sub
'End BuildTableWhere Method

'Open Method @2-5DD0F01A
    Function Open(Cmd)
        Errors.Clear
        If Connection Is Nothing Then
            Set Open = New clsEmptyDataSource
            Exit Function
        End If
        Set Cmd.Connection = Connection
        Cmd.CommandOperation = cmdOpen
        Cmd.PageSize = PageSize
        Cmd.ActivePage = AbsolutePage
        Cmd.CommandType = dsTable
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeBuildSelect", Me)
        Cmd.SQL = SQL
        Cmd.Where = Where
        Cmd.OrderBy = Order
        If(Len(StaticOrder)>0) Then
            If Len(Order)>0 Then Cmd.OrderBy = ", "+Cmd.OrderBy
            Cmd.OrderBy = StaticOrder + Cmd.OrderBy
        End If
        Cmd.Options("TOP") = True
        If Not AllParamsSet Then
            Set Open = New clsEmptyDataSource
            Exit Function
        End If
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeExecuteSelect", Me)
        If Errors.Count = 0 And CCSEventResult Then _
            Set Recordset = Cmd.Exec(Errors)
        CCSEventResult = CCRaiseEvent(CCSEvents, "AfterExecuteSelect", Me)
        Set Recordset.FieldsCollection = Fields
        Set Open = Recordset
    End Function
'End Open Method

'DataSource Class_Terminate Event @2-41B4B08D
    Private Sub Class_Terminate()
        If Recordset.State = adStateOpen Then _
            Recordset.Close
        Set Recordset = Nothing
        Set Parameters = Nothing
        Set Errors = Nothing
    End Sub
'End DataSource Class_Terminate Event

'Update Method @2-FEEEE8FE
    Sub Update(Cmd)
        CmdExecution = True
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeBuildUpdate", Me)
        Set Cmd.Connection = Connection
        Cmd.CommandOperation = cmdExec
        Cmd.CommandType = dsSQL
        Cmd.SQL = "UPDATE Users SET Password='{npass}' WHERE  User_id = {pass} And Password = '{opass}'"
        With Cmd.Parameters
            Set .Connection = Connection
            Set .DataSource = Me
            .AddParameter "npass", "ctrlnpass", ccsText, Empty, Empty, Empty, False
            .AddParameter "opass", "ctrlopass", ccsText, Empty, Empty, Empty, False
            .AddParameter "usuario", "ctrlusuario", ccsText, Empty, Empty, Empty, False
            .AddParameter "pass", "ctrlpass", ccsInteger, Empty, Empty, 1212, False
        End With
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeExecuteUpdate", Me)
        If Errors.Count = 0  And CmdExecution Then
            Cmd.Exec(Errors)
            CCSEventResult = CCRaiseEvent(CCSEvents, "AfterExecuteUpdate", Me)
        End If
    End Sub
'End Update Method

End Class 'End changePassDataSource Class @2-A61BA892

'Include Page Implementation @11-F4BF1AC9
%>
<!-- #INCLUDE VIRTUAL="/Alpheton/Header.asp" -->
<%
'End Include Page Implementation


%>
