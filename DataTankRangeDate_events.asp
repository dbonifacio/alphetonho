<%
'BindEvents Method @1-7C8C0B75
Sub BindEvents(Level)
    If Level="Page" Then
        Set CCSEvents("BeforeShow") = GetRef("Page_BeforeShow")
    Else
    End If
End Sub
'End BindEvents Method

Function Page_BeforeShow(Sender) 'Page_BeforeShow @1-A1547E8B

'Custom Code @2-73254650
' -------------------------
dim FusionHO
Dim SQL
Dim rsGroups
Dim rGroups
Dim strGroups
Dim strXML

dim strCategories, strtank1, strtank2, strtank3, strtank4
dim i
dim var
dim var1
dim var2
dim b
dim fecha
dim fecha1
dim var5

dim estacion
dim productos
dim tipo
dim SQL2
dim SQL3
dim cant_tanks
dim aux
dim k

b = false
strGroups = ""
strXML = ""
Set FusionHO = Server.CreateObject("ADODB.Connection") 
FusionHO.open "PROVIDER=SQLNCLI10;DATA SOURCE=MICROSTR-0A74B0;UID=sa;PWD=serbiznet;DATABASE=FusionHO " 
var = ""
var = CCGetRequestParam("site", ccsGet)
var2 = CCGetRequestParam("prod", ccsGet)

var1 = CCGetRequestParam("date", ccsGet)
fecha = mid(var1,7,4)&"-"& mid(var1,1,2)&"-"& mid(var1,4,2)
fecha1 = mid(var1,17,4)&"-"& mid(var1,11,2)&"-"& mid(var1,14,2)



SQL = "	select t.s_tank as tank, t.Tank_date as dias,  round(t.Volume,0,1) as vol from tank_table as t where t.tank_DATE >= '"&fecha&"' and t.tank_date <= '"&fecha1&"' and t.Site_Name = '"&var&"' and t.tank_time = (select MAX(TANk_time) from tank_table where t.tank_date = tank_date)	order by  t.tank_date"
SQL2 = "select count(distinct(t.tank_id)) as cantidad from tank_actual_info as t inner join Sites as s on t.ss_id = s.ss_id where s.SiteName = '"&var&"'"
SQL3 = "select distinct(t.tank_id) as tanques from tank_actual_info as t inner join Sites as s on t.ss_id = s.ss_id where s.SiteName = '"&var&"'"

Set rGroups = FusionHO.Execute(SQL2)
While not rGroups.EOF
	cant_tanks = rGroups("cantidad")
rGroups.MoveNext 
wend


dim algo																																																												
dim vec()
Redim vec(cant_tanks)
dim tanks()
Redim tanks(cant_tanks)
dim color(16) 
color(1) = "AF00F8"
color(2) = "F6000F"
color(3) = "AFBBF8"
color(4) = "FBFF00"
color(5) = "C75045"
color(6) = "00CC00"
color(7) = "FF6600"
color(8) = "0066B3"
color(9) = "8FB200"
color(10) = "80FE80"
color(11) = "B24700"
color(12) = "0033CC"
color(13) = "007D47"
color(14) = "9CD4A2"
color(15) = "26C7E3"
color(16) = "FFB300"

k=1
Set rGroups = FusionHO.Execute(SQL3)
While not rGroups.EOF
	tanks(k) = rGroups("tanques")
	k = k + 1
rGroups.MoveNext 
wend

algo = datediff("d", fecha, fecha1)
dim arrData()
dim fila
dim col
fila = algo + 1
col = cant_tanks + 1
ReDim arrData(fila,col)

dim variable
dim datefor	
dim j

For i=0 to UBound(arrData)-1  
variable = dateadd("d", i,fecha)
arrData(i,1) = mid(variable,1,2)&"/"& mid(variable,4,2)
for j = 1 to cant_tanks
arrData(i,j+1) = 0
next
next

'/////////// leo de la base, y guardo los datos en un array con las horas como indices

 Set rsGroups = FusionHO.Execute(SQL)
While not rsGroups.EOF

		for i = 0 to UBound(arrData)-1
		variable = rsGroups("dias")
		if (arrData(i,1) = (mid(variable,1,2)&"/"& mid(variable,4,2))) then
			for j= 1 to cant_tanks
 			if rsGroups("tank") = tanks(j) then
				arrData(i,j+1) = rsGroups("vol")
			end if
			next
		end if

	next
  rsGroups.MoveNext


Wend

'Initialize <graph> element
strXML = "<graph caption='        Stock By Tank in " & var& "' subcaption='"&fecha&" to "&fecha1&"                ' hovercapbg='FFECAA' chartBottomMargin='0' hovercapborder='F47E00' formatNumberScale='0' decimalPrecision='2' showvalues='0' numdivlines='5' numVdivlines='0' yaxisminvalue='0' yaxismaxvalue='7000'  rotateNames='1' xAxisName='Hours' yAxisName='Volume'>"

'Initialize <categories> element - necessary to generate a multi-series chart
strCategories = "<categories>"


	 
'Initiate <dataset> elements

for j = 1 to cant_tanks
vec(j) = "<dataset seriesName=' Tanque "
vec(j) = vec(j) & tanks(j)
vec(j) = vec(j) & "' color='"
vec(j) = vec(j) & color(j)
vec(j) = vec(j) & "' >"
next



'//////// armo el xml llenando los campos vacios hacia atras
 For i=0 to UBound(arrData)-1
		'Append <category name='...' /> to strCategories
		strCategories = strCategories & "<category name='" & arrData(i,1) & "' />"
		'Add <set value='...' /> to both the datasets

			for j = 1 to cant_tanks
			vec(j)= vec(j) &  "<set value='" & arrData(i,j+1) & "' />"
			next

   Next
 

  


   'Close <categories> element
   strCategories = strCategories & "</categories>"
   
      'Close <dataset> elements
	  for j = 1 to cant_tanks
	  vec(j) = vec(j) & "</dataset>"
	  next


   'Assemble the entire XML now
'   ///////////// definir esto cuando capturo los datos del form ////////////
var5 = Cint(var2)
if var2 = "" then
	aux = ""
	for j = 1 to cant_tanks
    	aux = aux & vec(j)
	next
	strXML = strXML & strCategories & aux & "</graph>"'
else

	for j = 1 to cant_tanks	
		if (tanks(j) = var5) then
			strXML = strXML & strCategories & vec(j) & "</graph>"'
		end if
next
end if

Response.ContentType = "text/xml"
'Just write out the XML data
'NOTE THAT THIS PAGE DOESN'T CONTAIN ANY HTML TAG, WHATSOEVER
 Response.Write(strXML)
'end if


' -------------------------
'End Custom Code

End Function 'Close Page_BeforeShow @1-54C34B28


%>
