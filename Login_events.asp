<%
'BindEvents Method @1-EB84FFFA
Sub BindEvents(Level)
    If Level="Page" Then
        Set CCSEvents("OnInitializeView") = GetRef("Page_OnInitializeView")
    Else
        Set Login.Button_DoLogin.CCSEvents("OnClick") = GetRef("Login_Button_DoLogin_OnClick")
    End If
End Sub
'End BindEvents Method

Function Login_Button_DoLogin_OnClick(Sender) 'Login_Button_DoLogin_OnClick @3-57DDB0BB

'Login @4-3D04C4CB
    With Login
        If NOT CCLoginUser(.login.Value, .password.Value) Then
            .Errors.addError(CCSLocales.GetText("CCS_LoginError", Empty))
            Login_Button_DoLogin_OnClick = False
            .password.Value = ""
        Else
            If Not IsEmpty(CCGetParam("ret_link", Empty)) Then _
                Redirect = CCGetParam("ret_link", Empty)
            Login_Button_DoLogin_OnClick = True
        End If
    End With
'End Login

'DEL  ' -------------------------
'DEL  
'DEL    If Login_DoLogin_OnClick = True Then
'DEL  
'DEL       
'DEL       Session("User_name") = CCDLookUp("User_name","Users","User_id="& FusionHO.ToSQL(CCGetUserID(),ccsInteger), FusionHO)
'DEL       
'DEL      
'DEL    End if
'DEL  
'DEL  
'DEL  
'DEL  ' ------------------------- 


End Function 'Close Login_Button_DoLogin_OnClick @3-54C34B28

Function Page_OnInitializeView(Sender) 'Page_OnInitializeView @1-1F930D9C

'Logout @9-E37F7265
    If NOT IsEmpty(CCGetParam("Logout", Empty)) Then
    CCLogoutUser
    End If
'End Logout

End Function 'Close Page_OnInitializeView @1-54C34B28


%>
