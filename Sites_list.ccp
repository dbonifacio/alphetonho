<Page id="1" templateExtension="html" relativePath="." fullRelativePath="." secured="True" urlType="Relative" isIncluded="False" SSLAccess="False" isService="False" cachingEnabled="False" cachingDuration="1 minutes" wizardTheme="FusionHO1" wizardThemeVersion="3.0" needGeneration="0" pasteActions="pasteActions">
	<Components>
		<Record id="2" sourceType="Table" urlType="Relative" secured="False" allowInsert="False" allowUpdate="False" allowDelete="False" validateData="True" preserveParameters="None" returnValueType="Number" returnValueTypeForDelete="Number" returnValueTypeForInsert="Number" returnValueTypeForUpdate="Number" name="SitesSearch" returnPage="Sites_list.ccp" wizardCaption="Search Sites " wizardOrientation="Vertical" wizardFormMethod="post" PathID="SitesSearch" pasteActions="pasteActions">
			<Components>
				<TextBox id="4" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="s_keyword" wizardCaption="Keyword" wizardSize="30" wizardMaxLength="30" wizardIsPassword="False" PathID="SitesSearchs_keyword">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
				<Button id="72" urlType="Relative" enableValidation="True" isDefault="False" name="Button_DoSearch1" operation="Search" wizardCaption="{res:CCS_Search}" PathID="SitesSearchButton_DoSearch1">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Button>
			</Components>
			<Events/>
			<TableParameters/>
			<SPParameters/>
			<SQLParameters/>
			<JoinTables/>
			<JoinLinks/>
			<Fields/>
			<ISPParameters/>
			<ISQLParameters/>
			<IFormElements/>
			<USPParameters/>
			<USQLParameters/>
			<UConditions/>
			<UFormElements/>
			<DSPParameters/>
			<DSQLParameters/>
			<DConditions/>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</Record>
		<Grid id="6" secured="False" sourceType="Table" returnValueType="Number" defaultPageSize="20" name="Sites" connection="FusionHO" pageSizeLimit="100" wizardCaption="List of Sites " wizardGridType="Tabular" wizardAllowSorting="True" wizardSortingType="SimpleDir" wizardUsePageScroller="True" wizardAllowInsert="True" wizardAltRecord="False" wizardRecordSeparator="False" wizardAltRecordType="Controls" dataSource="Sites, Moso , Tm , Region , Country , City " activeCollection="TableParameters">
			<Components>
				<Link id="18" visible="Yes" fieldSourceType="DBColumn" dataType="Text" html="False" hrefType="Page" urlType="Relative" preserveParameters="GET" name="Sites_Insert" hrefSource="Sites_list.ccp" removeParameters="ss_id" wizardThemeItem="NavigatorLink" wizardDefaultValue="Add New" PathID="SitesSites_Insert" wizardUseTemplateBlock="False">
					<Components/>
					<Events/>
					<LinkParameters>
						<LinkParameter id="110" sourceType="Expression" name="var" source="1"/>
					</LinkParameters>
					<Attributes/>
					<Features/>
				</Link>
				<Sorter id="24" visible="True" name="Sorter_ss_id" column="ss_id" wizardCaption="Id" wizardSortingType="SimpleDir" wizardControl="site_id" wizardAddNbsp="False" PathID="SitesSorter_ss_id" connection="FusionHO">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="25" visible="True" name="Sorter_SiteName" column="SiteName" wizardCaption="Name" wizardSortingType="SimpleDir" wizardControl="SiteName" wizardAddNbsp="False" PathID="SitesSorter_SiteName">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="26" visible="True" name="Sorter_Address" column="Address" wizardCaption="Address" wizardSortingType="SimpleDir" wizardControl="Address" wizardAddNbsp="False" PathID="SitesSorter_Address">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="27" visible="True" name="Sorter_Moso_name" column="Moso_name" wizardCaption="Moso Name" wizardSortingType="SimpleDir" wizardControl="Moso_name" wizardAddNbsp="False" PathID="SitesSorter_Moso_name">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="28" visible="True" name="Sorter_Latitud" column="Latitud" wizardCaption="Latitud" wizardSortingType="SimpleDir" wizardControl="Latitud" wizardAddNbsp="False" PathID="SitesSorter_Latitud">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="29" visible="True" name="Sorter_Longitud" column="Longitud" wizardCaption="Longitud" wizardSortingType="SimpleDir" wizardControl="Longitud" wizardAddNbsp="False" PathID="SitesSorter_Longitud">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="30" visible="True" name="Sorter_Tm_name" column="Tm_name" wizardCaption="Tm Name" wizardSortingType="SimpleDir" wizardControl="Tm_name" wizardAddNbsp="False" PathID="SitesSorter_Tm_name">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="31" visible="True" name="Sorter_Zip" column="Zip" wizardCaption="Zip" wizardSortingType="SimpleDir" wizardControl="Zip" wizardAddNbsp="False" PathID="SitesSorter_Zip">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="32" visible="True" name="Sorter_Phone" column="Phone" wizardCaption="Phone" wizardSortingType="SimpleDir" wizardControl="Phone" wizardAddNbsp="False" PathID="SitesSorter_Phone">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="33" visible="True" name="Sorter_Mail" column="Mail" wizardCaption="Mail" wizardSortingType="SimpleDir" wizardControl="Mail" wizardAddNbsp="False" PathID="SitesSorter_Mail">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="34" visible="True" name="Sorter_Valid" column="Sites.Valid" wizardCaption="Valid" wizardSortingType="SimpleDir" wizardControl="Valid" wizardAddNbsp="False" PathID="SitesSorter_Valid">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="35" visible="True" name="Sorter_Region_name" column="Region_name" wizardCaption="Region Name" wizardSortingType="SimpleDir" wizardControl="Region_name" wizardAddNbsp="False" PathID="SitesSorter_Region_name">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="36" visible="True" name="Sorter_Country_name" column="Country_name" wizardCaption="Country Name" wizardSortingType="SimpleDir" wizardControl="Country_name" wizardAddNbsp="False" PathID="SitesSorter_Country_name">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Sorter id="37" visible="True" name="Sorter_City_name" column="City_name" wizardCaption="City Name" wizardSortingType="SimpleDir" wizardControl="City_name" wizardAddNbsp="False" PathID="SitesSorter_City_name">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Sorter>
				<Link id="39" visible="Yes" fieldSourceType="DBColumn" dataType="Integer" html="False" hrefType="Page" urlType="Relative" preserveParameters="GET" name="ss_id" fieldSource="ss_id" wizardCaption="Id" wizardSize="10" wizardMaxLength="10" wizardIsPassword="False" wizardAddNbsp="True" wizardAlign="right" hrefSource="Sites_list.ccp" PathID="Sitesss_id" wizardUseTemplateBlock="False">
					<Components/>
					<Events/>
					<LinkParameters>
						<LinkParameter id="40" sourceType="DataField" format="yyyy-mm-dd" name="ss_id" source="ss_id"/>
					</LinkParameters>
					<Attributes/>
					<Features/>
				</Link>
				<Label id="42" fieldSourceType="DBColumn" dataType="Text" html="False" name="SiteName" fieldSource="SiteName" wizardCaption="Name" wizardSize="30" wizardMaxLength="30" wizardIsPassword="False" wizardAddNbsp="True" PathID="SitesSiteName">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="44" fieldSourceType="DBColumn" dataType="Text" html="False" name="Address" fieldSource="Expr1" wizardCaption="Address" wizardSize="50" wizardMaxLength="50" wizardIsPassword="False" wizardAddNbsp="True" PathID="SitesAddress">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="46" fieldSourceType="DBColumn" dataType="Text" html="False" name="Moso_name" fieldSource="Moso_name" wizardCaption="Moso Name" wizardSize="50" wizardMaxLength="50" wizardIsPassword="False" wizardAddNbsp="True" PathID="SitesMoso_name">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="48" fieldSourceType="DBColumn" dataType="Float" html="False" name="Latitud" fieldSource="Latitud" wizardCaption="Latitud" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardAddNbsp="True" wizardAlign="right" PathID="SitesLatitud">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="50" fieldSourceType="DBColumn" dataType="Float" html="False" name="Longitud" fieldSource="Longitud" wizardCaption="Longitud" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" wizardAddNbsp="True" wizardAlign="right" PathID="SitesLongitud">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="52" fieldSourceType="DBColumn" dataType="Text" html="False" name="Tm_name" fieldSource="Tm_name" wizardCaption="Tm Name" wizardSize="50" wizardMaxLength="50" wizardIsPassword="False" wizardAddNbsp="True" PathID="SitesTm_name">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="54" fieldSourceType="DBColumn" dataType="Text" html="False" name="Zip" fieldSource="Zip" wizardCaption="Zip" wizardSize="50" wizardMaxLength="50" wizardIsPassword="False" wizardAddNbsp="True" PathID="SitesZip">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="56" fieldSourceType="DBColumn" dataType="Text" html="False" name="Phone" fieldSource="Phone" wizardCaption="Phone" wizardSize="50" wizardMaxLength="50" wizardIsPassword="False" wizardAddNbsp="True" PathID="SitesPhone">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="58" fieldSourceType="DBColumn" dataType="Text" html="False" name="Mail" fieldSource="Mail" wizardCaption="Mail" wizardSize="50" wizardMaxLength="50" wizardIsPassword="False" wizardAddNbsp="True" PathID="SitesMail">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="60" fieldSourceType="DBColumn" dataType="Boolean" html="False" name="Valid" fieldSource="Sites_Valid" wizardCaption="Valid" wizardSize="1" wizardMaxLength="1" wizardIsPassword="False" wizardAddNbsp="True" PathID="SitesValid">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="62" fieldSourceType="DBColumn" dataType="Text" html="False" name="Region_name" fieldSource="Region_name" wizardCaption="Region Name" wizardSize="50" wizardMaxLength="50" wizardIsPassword="False" wizardAddNbsp="True" PathID="SitesRegion_name">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="64" fieldSourceType="DBColumn" dataType="Text" html="False" name="Country_name" fieldSource="Country_name" wizardCaption="Country Name" wizardSize="50" wizardMaxLength="50" wizardIsPassword="False" wizardAddNbsp="True" PathID="SitesCountry_name">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Label id="66" fieldSourceType="DBColumn" dataType="Text" html="False" name="City_name" fieldSource="City_name" wizardCaption="City Name" wizardSize="50" wizardMaxLength="50" wizardIsPassword="False" wizardAddNbsp="True" PathID="SitesCity_name">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Label>
				<Navigator id="67" size="10" type="Simple" pageSizes="1;5;10;25;50" name="Navigator" wizardFirst="True" wizardPrev="True" wizardFirstText="|&lt;" wizardPrevText="&lt;&lt;" wizardNextText="&gt;&gt;" wizardLastText="&gt;|" wizardNext="True" wizardLast="True" wizardPageNumbers="Simple" wizardSize="10" wizardTotalPages="True" wizardHideDisabled="True" wizardOfText="of" wizardImagesScheme="Fresh">
					<Components/>
					<Events>
						<Event name="BeforeShow" type="Server">
							<Actions>
								<Action actionName="Hide-Show Component" actionCategory="General" id="68" action="Hide" conditionType="Parameter" dataType="Integer" condition="LessThan" name1="TotalPages" sourceType1="SpecialValue" name2="2" sourceType2="Expression"/>
							</Actions>
						</Event>
					</Events>
					<Attributes/>
					<Features/>
				</Navigator>
			</Components>
			<Events/>
			<TableParameters>
				<TableParameter id="19" conditionType="Parameter" useIsNull="False" field="Sites.SiteName" dataType="Text" logicOperator="Or" searchConditionType="Contains" parameterType="URL" orderNumber="1" leftBrackets="0" parameterSource="s_keyword"/>
			</TableParameters>
			<JoinTables>
				<JoinTable id="7" tableName="Sites" posWidth="115" posHeight="180" posLeft="10" posRight="-1" posTop="10"/>
				<JoinTable id="8" tableName="Moso" posWidth="95" posHeight="88" posLeft="146" posRight="-1" posTop="10"/>
				<JoinTable id="10" tableName="Tm" posWidth="95" posHeight="120" posLeft="262" posRight="-1" posTop="10"/>
				<JoinTable id="12" tableName="Region" posWidth="95" posHeight="104" posLeft="146" posRight="-1" posTop="108"/>
				<JoinTable id="14" tableName="Country" posWidth="95" posHeight="88" posLeft="262" posRight="-1" posTop="140"/>
				<JoinTable id="16" tableName="City" posWidth="95" posHeight="104" posLeft="21" posRight="-1" posTop="200"/>
			</JoinTables>
			<JoinLinks>
				<JoinTable2 id="9" tableLeft="Sites" fieldLeft="Sites.Moso_id" tableRight="Moso" fieldRight="Moso.Moso_id" conditionType="Equal" joinType="left"/>
				<JoinTable2 id="11" tableLeft="Sites" fieldLeft="Sites.Tm_id" tableRight="Tm" fieldRight="Tm.Tm_id" conditionType="Equal" joinType="left"/>
				<JoinTable2 id="13" tableLeft="Sites" fieldLeft="Sites.Region_id" tableRight="Region" fieldRight="Region.Region_id" conditionType="Equal" joinType="left"/>
				<JoinTable2 id="15" tableLeft="Sites" fieldLeft="Sites.Country_id" tableRight="Country" fieldRight="Country.Country_id" conditionType="Equal" joinType="left"/>
				<JoinTable2 id="17" tableLeft="Sites" fieldLeft="Sites.City_id" tableRight="City" fieldRight="City.City_id" conditionType="Equal" joinType="left"/>
			</JoinLinks>
			<Fields>
				<Field id="38" tableName="Sites" fieldName="ss_id" isExpression="False"/>
				<Field id="41" tableName="Sites" fieldName="SiteName" isExpression="False"/>
				<Field id="43" tableName="Sites" fieldName="Address" isExpression="False" alias="Expr1"/>
				<Field id="45" tableName="Moso" fieldName="Moso_name" isExpression="False"/>
				<Field id="47" tableName="Sites" fieldName="Latitud" isExpression="False"/>
				<Field id="49" tableName="Sites" fieldName="Longitud" isExpression="False"/>
				<Field id="51" tableName="Tm" fieldName="Tm_name" isExpression="False"/>
				<Field id="53" tableName="Sites" fieldName="Zip" isExpression="False"/>
				<Field id="55" tableName="Sites" fieldName="Phone" isExpression="False"/>
				<Field id="57" tableName="Sites" fieldName="Sites.Mail"/>
				<Field id="59" tableName="Sites" fieldName="Sites.Valid" alias="Sites_Valid"/>
				<Field id="61" tableName="Region" fieldName="Region.Region_name"/>
				<Field id="63" tableName="Country" fieldName="Country.Country_name"/>
				<Field id="65" tableName="City" fieldName="City.City_name"/>
			</Fields>
			<SPParameters/>
			<SQLParameters/>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</Grid>
		<IncludePage id="70" name="Header" PathID="Header" page="Header.ccp">
			<Components/>
			<Events/>
			<Features/>
		</IncludePage>
		<IncludePage id="71" name="Footer" PathID="Footer" page="Footer.ccp">
			<Components/>
			<Events/>
			<Features/>
		</IncludePage>
		<Record id="73" sourceType="Table" urlType="Relative" secured="False" allowInsert="True" allowUpdate="True" allowDelete="True" validateData="True" preserveParameters="None" returnValueType="Number" returnValueTypeForDelete="Number" returnValueTypeForInsert="Number" returnValueTypeForUpdate="Number" connection="FusionHO" name="Sites1" dataSource="Sites" errorSummator="Error" wizardCaption="Add/Edit Sites " wizardFormMethod="post" returnPage="Sites_list.ccp" PathID="Sites1" pasteActions="pasteActions" pasteAsReplace="pasteAsReplace" activeCollection="TableParameters" activeTableType="customDelete" removeParameters="ss_id">
			<Components>
				<TextBox id="74" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="SiteName" fieldSource="SiteName" required="False" caption="Name" wizardCaption="Name" wizardSize="30" wizardMaxLength="30" wizardIsPassword="False" PathID="Sites1SiteName">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
				<TextBox id="75" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="Address" fieldSource="Address" required="False" caption="Address" wizardCaption="Address" wizardSize="50" wizardMaxLength="50" wizardIsPassword="False" PathID="Sites1Address">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
				<TextBox id="76" visible="Yes" fieldSourceType="DBColumn" dataType="Float" name="Latitud" fieldSource="Latitud" required="False" caption="Latitud" wizardCaption="Latitud" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" PathID="Sites1Latitud">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
				<TextBox id="77" visible="Yes" fieldSourceType="DBColumn" dataType="Float" name="Longitud" fieldSource="Longitud" required="False" caption="Longitud" wizardCaption="Longitud" wizardSize="20" wizardMaxLength="20" wizardIsPassword="False" PathID="Sites1Longitud">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
				<ListBox id="78" visible="Yes" fieldSourceType="DBColumn" sourceType="Table" dataType="Integer" returnValueType="Number" name="Tm_id" fieldSource="Tm_id" required="False" caption="Tm Id" wizardCaption="Tm Id" wizardSize="10" wizardMaxLength="10" wizardIsPassword="False" wizardEmptyCaption="Select Value" connection="FusionHO" dataSource="Tm" boundColumn="Tm_id" textColumn="Tm_name" PathID="Sites1Tm_id" activeCollection="TableParameters">
					<Components/>
					<Events/>
					<TableParameters>
						<TableParameter id="79" conditionType="Parameter" useIsNull="False" field="Valid" dataType="Boolean" searchConditionType="Equal" parameterType="Expression" logicOperator="And" format="True;False" parameterSource="True"/>
					</TableParameters>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables>
						<JoinTable id="80" tableName="Tm" posLeft="10" posTop="10" posWidth="95" posHeight="56"/>
					</JoinTables>
					<JoinLinks/>
					<Fields/>
					<Attributes/>
					<Features/>
				</ListBox>
				<TextBox id="81" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="Zip" fieldSource="Zip" required="False" caption="Zip" wizardCaption="Zip" wizardSize="50" wizardMaxLength="50" wizardIsPassword="False" PathID="Sites1Zip">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
				<TextBox id="82" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="Phone" fieldSource="Phone" required="False" caption="Phone" wizardCaption="Phone" wizardSize="50" wizardMaxLength="50" wizardIsPassword="False" PathID="Sites1Phone">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
				<TextBox id="83" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="Mail" fieldSource="Mail" required="False" caption="Mail" wizardCaption="Mail" wizardSize="50" wizardMaxLength="50" wizardIsPassword="False" PathID="Sites1Mail">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
				<CheckBox id="84" visible="Yes" fieldSourceType="DBColumn" dataType="Boolean" name="Valid" fieldSource="Valid" required="False" caption="Valid" wizardCaption="Valid" wizardSize="1" wizardMaxLength="1" wizardIsPassword="False" PathID="Sites1Valid">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</CheckBox>
				<ListBox id="85" visible="Yes" fieldSourceType="DBColumn" sourceType="Table" dataType="Integer" returnValueType="Number" name="Country_id" fieldSource="Country_id" required="False" caption="Country Id" wizardCaption="Country Id" wizardSize="10" wizardMaxLength="10" wizardIsPassword="False" wizardEmptyCaption="Select Value" connection="FusionHO" dataSource="Country" boundColumn="Country_id" textColumn="Country_name" PathID="Sites1Country_id">
					<Components/>
					<Events/>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables/>
					<JoinLinks/>
					<Fields/>
					<Attributes/>
					<Features/>
				</ListBox>
				<ListBox id="86" visible="Yes" fieldSourceType="DBColumn" sourceType="Table" dataType="Integer" returnValueType="Number" name="City_id" fieldSource="City_id" required="False" caption="City Id" wizardCaption="City Id" wizardSize="10" wizardMaxLength="10" wizardIsPassword="False" wizardEmptyCaption="Select Value" connection="FusionHO" dataSource="City" boundColumn="City_id" textColumn="City_name" PathID="Sites1City_id" features="(assigned)">
					<Components/>
					<Events/>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables/>
					<JoinLinks/>
					<Fields/>
					<Attributes/>
					<Features>
						<PTDependentListBox id="87" enabled="True" name="PTDependentListBox3" servicePage="services/Sites_maint_Sites_City_id_PTDependentListBox1.ccp" masterListbox="Region_id" category="Prototype">
							<Components/>
							<Events/>
							<Features/>
						</PTDependentListBox>
					</Features>
				</ListBox>
				<TextBox id="88" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="ss_id" PathID="Sites1ss_id" fieldSource="ss_id">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
				<ListBox id="93" visible="Yes" fieldSourceType="DBColumn" sourceType="Table" dataType="Integer" returnValueType="Number" name="Service_id" fieldSource="Service_id" required="False" caption="Service Id" wizardCaption="Country Id" wizardSize="10" wizardMaxLength="10" wizardIsPassword="False" wizardEmptyCaption="Select Value" connection="FusionHO" dataSource="Service" boundColumn="Service_id" textColumn="Service_name" PathID="Sites1Service_id">
					<Components/>
					<Events/>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables/>
					<JoinLinks/>
					<Fields/>
					<Attributes/>
					<Features/>
				</ListBox>
				<ListBox id="94" visible="Yes" fieldSourceType="DBColumn" sourceType="Table" dataType="Integer" returnValueType="Number" name="Brand_id" fieldSource="Brand_id" required="False" caption="{res:Brand}" wizardCaption="Country Id" wizardSize="10" wizardMaxLength="10" wizardIsPassword="False" wizardEmptyCaption="Select Value" connection="FusionHO" dataSource="Brand" boundColumn="Brand_id" textColumn="Brand_name" PathID="Sites1Brand_id">
					<Components/>
					<Events/>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables/>
					<JoinLinks/>
					<Fields/>
					<Attributes/>
					<Features/>
				</ListBox>
				<ListBox id="95" visible="Yes" fieldSourceType="DBColumn" sourceType="Table" dataType="Integer" returnValueType="Number" name="Local_type_id" fieldSource="Local_type_id" required="False" caption="Local_type Id" wizardCaption="Country Id" wizardSize="10" wizardMaxLength="10" wizardIsPassword="False" wizardEmptyCaption="Select Value" connection="FusionHO" dataSource="Local_type" boundColumn="Local_type_id" textColumn="Local_type_name" PathID="Sites1Local_type_id">
					<Components/>
					<Events/>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables/>
					<JoinLinks/>
					<Fields/>
					<Attributes/>
					<Features/>
				</ListBox>
				<ListBox id="99" visible="Yes" fieldSourceType="DBColumn" sourceType="Table" dataType="Integer" returnValueType="Number" name="Region_id" fieldSource="Region_id" required="False" caption="Region Id" wizardCaption="Country Id" wizardSize="10" wizardMaxLength="10" wizardIsPassword="False" wizardEmptyCaption="Select Value" connection="FusionHO" dataSource="Region" boundColumn="Region_id" textColumn="Region_name" PathID="Sites1Region_id" features="(assigned)">
					<Components/>
					<Events/>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables/>
					<JoinLinks/>
					<Fields/>
					<Attributes/>
					<Features>
						<PTDependentListBox id="100" enabled="True" name="PTDependentListBox2" servicePage="services/Sites_maint_Sites_Region_id_PTDependentListBox1.ccp" masterListbox="Country_id" category="Prototype">
							<Components/>
							<Events/>
							<Features/>
						</PTDependentListBox>
					</Features>
				</ListBox>
				<ListBox id="102" visible="Yes" fieldSourceType="DBColumn" sourceType="Table" dataType="Float" returnValueType="Number" name="Moso" wizardEmptyCaption="Select Value" PathID="Sites1Moso" connection="FusionHO" dataSource="Moso" boundColumn="Moso_id" textColumn="Moso_name" fieldSource="Moso_id">
					<Components/>
					<Events>
					</Events>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables/>
					<JoinLinks/>
					<Fields/>
					<Attributes/>
					<Features/>
				</ListBox>
				<Button id="103" urlType="Relative" enableValidation="True" isDefault="False" name="Button_Insert1" operation="Insert" wizardCaption="{res:CCS_Insert}" wizardThemeItem="FooterIMG" wizardButtonImage="ButtonInsertOn" PathID="Sites1Button_Insert1">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Button>
				<Button id="104" urlType="Relative" enableValidation="True" isDefault="False" name="Button_Update" operation="Update" wizardCaption="{res:CCS_Update}" wizardThemeItem="FooterIMG" wizardButtonImage="ButtonUpdateOn" PathID="Sites1Button_Update">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Button>
				<Button id="105" urlType="Relative" enableValidation="False" isDefault="False" name="Button_Delete" operation="Delete" wizardCaption="{res:CCS_Delete}" wizardThemeItem="FooterIMG" wizardButtonImage="ButtonDeleteOn" PathID="Sites1Button_Delete">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Button>
				<Button id="106" urlType="Relative" enableValidation="False" isDefault="False" name="Button_Cancel" operation="Cancel" wizardCaption="{res:CCS_Cancel}" wizardThemeItem="FooterIMG" wizardButtonImage="ButtonCancelOn" PathID="Sites1Button_Cancel">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Button>
			</Components>
			<Events>
				<Event name="BeforeShow" type="Server">
					<Actions>
						<Action actionName="Custom Code" actionCategory="General" id="111"/>
					</Actions>
				</Event>
			</Events>
			<TableParameters>
				<TableParameter id="108" conditionType="Parameter" useIsNull="False" field="ss_id" dataType="Integer" logicOperator="And" searchConditionType="Equal" parameterType="URL" orderNumber="1" parameterSource="ss_id"/>
			</TableParameters>
			<SPParameters/>
			<SQLParameters/>
			<JoinTables>
				<JoinTable id="112" tableName="Sites" posLeft="10" posTop="10" posWidth="115" posHeight="180"/>
			</JoinTables>
			<JoinLinks/>
			<Fields/>
			<ISPParameters/>
			<ISQLParameters/>
			<IFormElements/>
			<USPParameters/>
			<USQLParameters/>
			<UConditions/>
			<UFormElements/>
			<DSPParameters/>
			<DSQLParameters/>
			<DConditions>
				<TableParameter id="109" conditionType="Parameter" useIsNull="False" field="site_id" dataType="Integer" parameterType="URL" parameterSource="site_id" searchConditionType="Equal" logicOperator="And" orderNumber="1"/>
			</DConditions>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</Record>
	</Components>
	<CodeFiles>
		<CodeFile id="Events" language="ASPTemplates" name="Sites_list_events.asp" forShow="False" comment="'" codePage="windows-1252"/>
		<CodeFile id="Code" language="ASPTemplates" name="Sites_list.asp" forShow="True" url="Sites_list.asp" comment="'" codePage="windows-1252"/>
	</CodeFiles>
	<SecurityGroups>
		<Group id="69" groupID="1"/>
	</SecurityGroups>
	<CachingParameters/>
	<Attributes/>
	<Features/>
	<Events/>
</Page>
