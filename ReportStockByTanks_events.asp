<%
'BindEvents Method @1-F19EB633
Sub BindEvents(Level)
    If Level="Page" Then
    Else
        Set Report1.Navigator.CCSEvents("BeforeShow") = GetRef("Report1_Page_Footer_Navigator_BeforeShow")
        Set Report_Print.CCSEvents("BeforeShow") = GetRef("Report_Print_BeforeShow")
    End If
End Sub
'End BindEvents Method

Function Report1_Page_Footer_Navigator_BeforeShow(Sender) 'Report1_Page_Footer_Navigator_BeforeShow @24-0AA8A152

'Hide-Show Component @25-15CE4C54
    Dim TotalPages_25_1 : TotalPages_25_1 = CCSConverter.VBSConvert(ccsInteger, Report1.DataSource.Recordset.PageCount)
    Dim Param2_25_2 : Param2_25_2 = CCSConverter.VBSConvert(ccsInteger, 2)
    If  (Not IsEmpty(TotalPages_25_1) And Not IsEmpty(Param2_25_2) And TotalPages_25_1 < Param2_25_2) Then _
        Report1.Navigator.Visible = False
'End Hide-Show Component

End Function 'Close Report1_Page_Footer_Navigator_BeforeShow @24-54C34B28

Function Report_Print_BeforeShow(Sender) 'Report_Print_BeforeShow @76-D2FE8D6A

'Hide-Show Component @78-DE511F98
    Dim ViewMode_78_1 : ViewMode_78_1 = CCSConverter.VBSConvert(ccsText, CCGetFromGet("ViewMode", Empty))
    Dim Param2_78_2 : Param2_78_2 = CCSConverter.VBSConvert(ccsText, "Print")
    If (IsEmpty(ViewMode_78_1) And IsEmpty(Param2_78_2)) Or  (Not IsEmpty(ViewMode_78_1) And Not IsEmpty(Param2_78_2) And ViewMode_78_1 = Param2_78_2) Then _
        Report_Print.Visible = False
'End Hide-Show Component

End Function 'Close Report_Print_BeforeShow @76-54C34B28


%>
