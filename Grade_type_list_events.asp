<%
'BindEvents Method @1-901157EB
Sub BindEvents(Level)
    If Level="Page" Then
    Else
        Set Grade_type.Navigator.CCSEvents("BeforeShow") = GetRef("Grade_type_Navigator_BeforeShow")
        Set Grade_type1.CCSEvents("BeforeShow") = GetRef("Grade_type1_BeforeShow")
    End If
End Sub
'End BindEvents Method

Function Grade_type_Navigator_BeforeShow(Sender) 'Grade_type_Navigator_BeforeShow @14-4B117C32

'Hide-Show Component @15-7E11A3D8
    Dim TotalPages_15_1 : TotalPages_15_1 = CCSConverter.VBSConvert(ccsInteger, Grade_type.DataSource.Recordset.PageCount)
    Dim Param2_15_2 : Param2_15_2 = CCSConverter.VBSConvert(ccsInteger, 2)
    If  (Not IsEmpty(TotalPages_15_1) And Not IsEmpty(Param2_15_2) And TotalPages_15_1 < Param2_15_2) Then _
        Grade_type.Navigator.Visible = False
'End Hide-Show Component

End Function 'Close Grade_type_Navigator_BeforeShow @14-54C34B28

Function Grade_type1_BeforeShow(Sender) 'Grade_type1_BeforeShow @24-95FE30DD

'Custom Code @32-73254650
' -------------------------
  	If Grade_type1.recordset.EOF Then
		Grade_type1.Visible = False
 	End if

 	If(CCGetFromGet("var","") = "1") Then
 		Grade_type1.Visible = true
	end if

' -------------------------
'End Custom Code

End Function 'Close Grade_type1_BeforeShow @24-54C34B28


%>
