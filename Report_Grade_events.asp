<%
'BindEvents Method @1-F73EB53C
Sub BindEvents(Level)
    If Level="Page" Then
        Set CCSEvents("OnInitializeView") = GetRef("Page_OnInitializeView")
    Else
        Set Grades.CCSEvents("BeforeSelect") = GetRef("Grades_BeforeSelect")
        Set Grades.CCSEvents("BeforeShowRow") = GetRef("Grades_BeforeShowRow")
    End If
End Sub
'End BindEvents Method

Function Grades_BeforeSelect(Sender) 'Grades_BeforeSelect @2-CCE76F20

'Custom Code @10-73254650
' -------------------------
If(CCGetFromGet("export","") = "1") Then
	'	Show up to 10,000 records
	  '  Report_Grade.PageSize = 10000

	End if
' -------------------------
'End Custom Code

End Function 'Close Grades_BeforeSelect @2-54C34B28

'Global variable
 Dim Counter

Function Grades_BeforeShowRow(Sender) 'Grades_BeforeShowRow @2-38241C17

'Custom Code @12-73254650
' -------------------------

  If (Counter = 0) Then
    grades.Label1.Value = "Row"
    Counter = 1
  Else
    grades.Label1.Value = "AltRow"
    Counter = 0
  End if


' -------------------------
'End Custom Code

End Function 'Close Grades_BeforeShowRow @2-54C34B28

Function Page_OnInitializeView(Sender) 'Page_OnInitializeView @1-1F930D9C

'Custom Code @9-73254650
' -------------------------
Dim ExportFileName
	
	ExportFileName = "Report.xls"
 	If(CCGetFromGet("export","") = "1") Then
	'Hide the ToExcelLink Link 
		ToExcelLink.Visible = False
   'Set Content type to Excel
       Response.ContentType="application/vnd.ms-excel"
	   Response.AddHeader "Content-Disposition", "attachment; filename=" & ExportFileName  & ";"
	
	End if

' -------------------------
'End Custom Code

End Function 'Close Page_OnInitializeView @1-54C34B28


%>
