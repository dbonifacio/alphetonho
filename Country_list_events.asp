<%
'BindEvents Method @1-5B52BFAC
Sub BindEvents(Level)
    If Level="Page" Then
    Else
        Set Country.Navigator.CCSEvents("BeforeShow") = GetRef("Country_Navigator_BeforeShow")
        Set Country1.CCSEvents("BeforeShow") = GetRef("Country1_BeforeShow")
    End If
End Sub
'End BindEvents Method

Function Country_Navigator_BeforeShow(Sender) 'Country_Navigator_BeforeShow @17-9F907B57

'Hide-Show Component @18-78BF5B5D
    Dim TotalPages_18_1 : TotalPages_18_1 = CCSConverter.VBSConvert(ccsInteger, Country.DataSource.Recordset.PageCount)
    Dim Param2_18_2 : Param2_18_2 = CCSConverter.VBSConvert(ccsInteger, 2)
    If  (Not IsEmpty(TotalPages_18_1) And Not IsEmpty(Param2_18_2) And TotalPages_18_1 < Param2_18_2) Then _
        Country.Navigator.Visible = False
'End Hide-Show Component

End Function 'Close Country_Navigator_BeforeShow @17-54C34B28

Function Country1_BeforeShow(Sender) 'Country1_BeforeShow @23-1C84A9E5

'Custom Code @31-73254650
' -------------------------
  	If Country1.Recordset.EOF Then
		Country1.Visible = False
 	End if

 	If(CCGetFromGet("var","") = "1") Then
 		Country1.Visible = true
	end if
' -------------------------
'End Custom Code

End Function 'Close Country1_BeforeShow @23-54C34B28


%>
