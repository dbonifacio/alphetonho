<Page id="1" templateExtension="html" relativePath="." fullRelativePath="." secured="False" urlType="Relative" isIncluded="False" SSLAccess="False" isService="False" cachingEnabled="False" cachingDuration="1 minutes" wizardTheme="FusionHO1" wizardThemeVersion="3.0" pasteActions="pasteActions" needGeneration="0">
	<Components>
		<Record id="2" sourceType="Table" urlType="Relative" secured="False" allowInsert="False" allowUpdate="False" allowDelete="False" validateData="True" preserveParameters="None" returnValueType="Number" returnValueTypeForDelete="Number" returnValueTypeForInsert="Number" returnValueTypeForUpdate="Number" name="tank_actual_info" wizardCaption="{res:CCS_SearchFormPrefix} {res:tank_actual_info} {res:CCS_SearchFormSuffix}" wizardOrientation="Vertical" wizardFormMethod="post" returnPage="excel_report_grade.ccp" PathID="tank_actual_info" pasteActions="pasteActions">
			<Components>
				<Link id="3" visible="Yes" fieldSourceType="DBColumn" dataType="Text" html="False" hrefType="Page" urlType="Relative" preserveParameters="GET" name="ClearParameters" hrefSource="ChartByProd.ccp" removeParameters="site;prod;s_DAY_DATE" wizardThemeItem="SorterLink" wizardDefaultValue="{res:CCS_Clear}" PathID="tank_actual_infoClearParameters" wizardUseTemplateBlock="False">
					<Components/>
					<Events/>
					<LinkParameters/>
					<Attributes/>
					<Features/>
				</Link>
				<Button id="4" urlType="Relative" enableValidation="True" isDefault="False" name="Button_DoSearch" operation="Search" wizardCaption="{res:CCS_Search}" PathID="tank_actual_infoButton_DoSearch">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</Button>
				<ListBox id="5" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="prod" wizardCaption="{res:tank_id}" wizardSize="10" wizardMaxLength="10" wizardIsPassword="False" PathID="tank_actual_infoprod" sourceType="Table" connection="FusionHO" boundColumn="grade_name" textColumn="grade_name" dataSource="Grades" caption="prod">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables>
						<JoinTable id="19" tableName="Grades" schemaName="dbo" posLeft="10" posTop="10" posWidth="95" posHeight="104"/>
					</JoinTables>
					<JoinLinks/>
					<Fields>
						<Field id="20" tableName="Grades" fieldName="grade_name"/>
					</Fields>
				</ListBox>
				<ListBox id="6" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="site" wizardCaption="{res:ss_id}" wizardSize="10" wizardMaxLength="10" wizardIsPassword="False" PathID="tank_actual_infosite" sourceType="Table" connection="FusionHO" dataSource="Sites" boundColumn="SiteName" textColumn="SiteName">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
					<TableParameters/>
					<SPParameters/>
					<SQLParameters/>
					<JoinTables>
						<JoinTable id="7" tableName="Sites" schemaName="dbo" posLeft="10" posTop="10" posWidth="115" posHeight="180"/>
					</JoinTables>
					<JoinLinks/>
					<Fields/>
				</ListBox>
				<TextBox id="8" visible="Yes" fieldSourceType="DBColumn" dataType="Date" name="s_DAY_DATE" wizardCaption="{res:DAY_DATE}" wizardSize="8" wizardMaxLength="100" wizardIsPassword="False" PathID="tank_actual_infos_DAY_DATE" format="mm/dd/yyyy">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</TextBox>
				<DatePicker id="9" name="DatePicker_s_DAY_DATE" control="s_DAY_DATE" wizardSatellite="True" wizardControl="s_DAY_DATE" wizardDatePickerType="Image" wizardPicture="Styles/FusionHO1/Images/DatePicker.gif" style="Styles/FusionHO1/Style.css" PathID="tank_actual_infoDatePicker_s_DAY_DATE">
					<Components/>
					<Events/>
					<Attributes/>
					<Features/>
				</DatePicker>
			</Components>
			<Events/>
			<TableParameters/>
			<SPParameters/>
			<SQLParameters/>
			<JoinTables/>
			<JoinLinks/>
			<Fields/>
			<ISPParameters/>
			<ISQLParameters/>
			<IFormElements/>
			<USPParameters/>
			<USQLParameters/>
			<UConditions/>
			<UFormElements/>
			<DSPParameters/>
			<DSQLParameters/>
			<DConditions/>
			<SecurityGroups/>
			<Attributes/>
			<Features/>
		</Record>
		<IncludePage id="18" name="Header" PathID="Header" page="Header.ccp">
			<Components/>
			<Events/>
			<Features/>
		</IncludePage>
	</Components>
	<CodeFiles>
		<CodeFile id="Code" language="ASPTemplates" name="ChartByProd.asp" forShow="True" url="ChartByProd.asp" comment="'" codePage="windows-1252"/>
	</CodeFiles>
	<SecurityGroups/>
	<CachingParameters/>
	<Attributes/>
	<Features/>
	<Events/>
</Page>
