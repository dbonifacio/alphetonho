<%
'BindEvents Method @1-BD70460C
Sub BindEvents(Level)
    If Level="Page" Then
        Set CCSEvents("BeforeInitialize") = GetRef("Page_BeforeInitialize")
    Else
        Set Sites_price_change_status.CCSEvents("BeforeShow") = GetRef("Sites_price_change_status_BeforeShow")
        Set Brand_City_Country_Local.s_SiteName.CCSEvents("BeforeShow") = GetRef("Brand_City_Country_Local_s_SiteName_BeforeShow")
        Set price_change_status.CCSEvents("BeforeShow") = GetRef("price_change_status_BeforeShow")
        Set price_send.CCSEvents("BeforeShow") = GetRef("price_send_BeforeShow")
        Set price_send.CCSEvents("BeforeShowRow") = GetRef("price_send_BeforeShowRow")
        Set Grid3.CCSEvents("BeforeShow") = GetRef("Grid3_BeforeShow")
    End If
End Sub
'End BindEvents Method

Function Sites_price_change_status_BeforeShow(Sender) 'Sites_price_change_status_BeforeShow @47-D5061DEF

'Custom Code @239-73254650
' -------------------------
		Sites_price_change_status.Visible = false
		If(CCGetFromGet("site","") = "1") Then
 			Sites_price_change_status.Visible = true
		end if
' -------------------------
'End Custom Code

End Function 'Close Sites_price_change_status_BeforeShow @47-54C34B28

Function Brand_City_Country_Local_s_SiteName_BeforeShow(Sender) 'Brand_City_Country_Local_s_SiteName_BeforeShow @117-40AE16A7

'PTAutocomplete1 BeforeShow @238-448DEF7E
    Sender.Attributes("id") = "Brand_City_Country_Locals_SiteName"
'End PTAutocomplete1 BeforeShow

End Function 'Close Brand_City_Country_Local_s_SiteName_BeforeShow @117-54C34B28
dim RowNumber 
Function price_change_status_BeforeShow(Sender) 'price_change_status_BeforeShow @171-B2A12D48

'Custom Code @181-73254650
' -------------------------
dim FusionHO
Dim SQL
Dim rsGroups
Dim strGroups
dim id
dim strXML

	  	If price_change_status.Recordset.EOF Then
			price_change_status.Visible = False
 		End if

		If(CCGetFromGet("var","") = "1") Then
 			price_change_status.Visible = true
		end if

		rsGroups = ""
		strXML = ""
		id = price_change_status.price_change_id.Value 
		Set FusionHO = Server.CreateObject("ADODB.Connection") 
		FusionHO.open "PROVIDER=SQLNCLI10;DATA SOURCE=MICROSTR-0A74B0;UID=sa;PWD=serbiznet;DATABASE=FusionHO " 
		SQL = "select status_code from price_change_status where price_change_id = '"& id &"'"
 		Set rsGroups = FusionHO.Execute(SQL)
		While not rsGroups.EOF
			strXML= strXML & rsGroups("status_code") 
			rsGroups.MoveNext
		Wend
		if strXML = "1" then
			
			price_change_status.Button_Update.visible = false
			price_change_status.Button_delete.visible = false
		end if
' -------------------------
'End Custom Code

End Function 'Close price_change_status_BeforeShow @171-54C34B28

Function price_send_BeforeShow(Sender) 'price_send_BeforeShow @185-0FF5D4A2

'Custom Code @208-73254650
' -------------------------
dim FusionHO
Dim SQL
Dim rsGroups
Dim strGroups
dim id
dim strXML
  	If price_change_status.Recordset.EOF Then
		price_send.Visible = False
	else
		rsGroups = ""
		strXML = ""
		id = price_change_status.price_change_id.Value 
		Set FusionHO = Server.CreateObject("ADODB.Connection") 
		FusionHO.open "PROVIDER=SQLNCLI10;DATA SOURCE=MICROSTR-0A74B0;UID=sa;PWD=serbiznet;DATABASE=FusionHO " 
		SQL = "select status_code from price_change_status where price_change_id = '"& id &"'"
 		Set rsGroups = FusionHO.Execute(SQL)
		While not rsGroups.EOF
			strXML= strXML & rsGroups("status_code") 
			rsGroups.MoveNext
		Wend
		if strXML = "1" then
			price_send.Visible = False
		else
			price_send.Visible = true
		end if
 	End if

' -------------------------
'End Custom Code

End Function 'Close price_send_BeforeShow @185-54C34B28

Function price_send_BeforeShowRow(Sender) 'price_send_BeforeShowRow @185-BEB574AB

'Custom Code @209-73254650
' -------------------------
  RowNumber = RowNumber + 1
  price_send.RowIDAttribute.Value = RowNumber
' -------------------------
'End Custom Code

End Function 'Close price_send_BeforeShowRow @185-54C34B28

Function Grid3_BeforeShow(Sender) 'Grid3_BeforeShow @218-46DCD233

'Custom Code @233-73254650
' -------------------------
dim FusionHO
Dim SQL
Dim rsGroups
Dim strGroups
dim id
dim strXML
  	If price_change_status.Recordset.EOF Then
		Grid3.Visible = False
	else
		rsGroups = ""
		strXML = ""
		id = price_change_status.price_change_id.Value 
		Set FusionHO = Server.CreateObject("ADODB.Connection") 
		FusionHO.open "PROVIDER=SQLNCLI10;DATA SOURCE=MICROSTR-0A74B0;UID=sa;PWD=serbiznet;DATABASE=FusionHO " 
		SQL = "select status_code from price_change_status where price_change_id = '"& id &"'"
 		Set rsGroups = FusionHO.Execute(SQL)
		While not rsGroups.EOF
			strXML= strXML & rsGroups("status_code") 
			rsGroups.MoveNext
		Wend
		if strXML = "1" then
			Grid3.Visible = True
			price_change_status.Button_Update.visible = false
			price_change_status.Button_delete.visible = false

		else
			Grid3.Visible = False
		end if
 	End if
' -------------------------
'End Custom Code

End Function 'Close Grid3_BeforeShow @218-54C34B28

Function Page_BeforeInitialize(Sender) 'Page_BeforeInitialize @1-A73C9435

'PTAutocomplete1 ServiceCall @238-06C43FF6
    If CCGetFromGet("callbackControl", Empty) = "Brand_City_Country_Locals_SiteNamePTAutocomplete1" Then _
        ServiceBrand_City_Country_Locals_SiteNamePTAutocomplete1
'End PTAutocomplete1 ServiceCall

End Function 'Close Page_BeforeInitialize @1-54C34B28

Function ServiceBrand_City_Country_Locals_SiteNamePTAutocomplete1() 'Function ServiceBrand_City_Country_Locals_SiteNamePTAutocomplete1 @238-814BC607

'PTAutocomplete1 Initialization @238-C9CF6B18
    Dim Service,  DBServiceFusionHO, ServiceDS, Fields, AdditionalWhere
    Set DBServiceFusionHO = New clsDBFusionHO
'End PTAutocomplete1 Initialization

'PTAutocomplete1 DataSource @238-B49A38D8
    Set ServiceDS = CCCreateDataSource(dsTable,DBServiceFusionHO, Array("SELECT *  " & _
"FROM Sites {SQL_Where} {SQL_OrderBy}", "", ""))
    With ServiceDS.WhereParameters
        Set .ParameterSources = Server.CreateObject("Scripting.Dictionary")
        .ParameterSources("posts_SiteName") = CCGetRequestParam("s_SiteName", ccsPOST)
        .AddParameter 1, "posts_SiteName", ccsText, Empty, Empty, -1, False
        .Criterion(1) = .Operation(opBeginsWith, False, "SiteName", .getParamByID(1))
        .AssembledWhere = .Criterion(1)
    End With
    ServiceDS.Where = ServiceDS.WhereParameters.AssembledWhere
    Set Service = CCCreateService("PTAutocomplete", "Brand_City_Country_Locals_SiteNamePTAutocomplete1", ServiceDS,Nothing, Nothing)
'End PTAutocomplete1 DataSource

'PTAutocomplete1 DataFields @238-5612690B
    Service.AddDatabaseField("SiteName")
'End PTAutocomplete1 DataFields

'PTAutocomplete1 Execution @238-0FED32A8
    Service.OutputFormatter = New clsListFormatter
    Response.Write  Service.Execute
    Set Service  = Nothing
    Response.End
'End PTAutocomplete1 Execution

End Function 'End Function ServiceBrand_City_Country_Locals_SiteNamePTAutocomplete1 @238-54C34B28


%>
