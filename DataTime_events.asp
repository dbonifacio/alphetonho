<%
'BindEvents Method @1-7C8C0B75
Sub BindEvents(Level)
    If Level="Page" Then
        Set CCSEvents("BeforeShow") = GetRef("Page_BeforeShow")
    Else
    End If
End Sub
'End BindEvents Method

Function Page_BeforeShow(Sender) 'Page_BeforeShow @1-A1547E8B

'Custom Code @2-73254650
' -------------------------
' Write your own code here.
' -------------------------
'End Custom Code
dim FusionHO
Dim SQL
Dim rsGroups
Dim strGroups
Dim strXML

dim strCategories, strtank1, strtank2, strtank3, strtank4
dim i
dim var
dim var1
dim var2
dim var5
dim b
dim fecha
dim fecha1

b = false
strGroups = ""
strXML = ""
Set FusionHO = Server.CreateObject("ADODB.Connection") 
FusionHO.open "PROVIDER=SQLNCLI10;DATA SOURCE=MICROSTR-0A74B0;UID=sa;PWD=serbiznet;DATABASE=FusionHO " 
var = ""
var = CCGetRequestParam("site", ccsGet)
var2 = CCGetRequestParam("prod", ccsGet)

var1 = CCGetRequestParam("date", ccsGet)
fecha = mid(var1,7,4)&"-"& mid(var1,1,2)&"-"& mid(var1,4,2)
fecha1 = mid(var1,17,4)&"-"& mid(var1,11,2)&"-"& mid(var1,14,2)

SQL = "SELECT SiteName AS Site_Name, DAY_TIME AS hour, round(Sum(money),0,1) AS Money, round(sum(volume),0,1) AS Volume, round(Count(sale_id),0,1) AS Sales FROM FusionHO.dbo.Sites INNER JOIN FusionHO.dbo.pump_sales ON FusionHO.dbo.Sites.ss_id = FusionHO.dbo.pump_sales.ss_id WHERE pump_sales.DAY_DATE >= '"&fecha&"' AND pump_sales.DAY_DATE <= '"&fecha1&"' and SiteName = '"&var&"' GROUP BY SiteName, DAY_TIME ORDER BY DAY_TIME"

dim arrData()
dim fila
dim col
fila = 24
col = 5
ReDim arrData(fila,col)


	
For i=0 to UBound(arrData)-1  
arrData(i,1) = i
arrData(i,2) = 0
arrData(i,3) = 0
arrData(i,4) = 0
arrData(i,5) = 0
next


'/////////// leo de la base, y guardo los datos en un array con las horas como indices
 Set rsGroups = FusionHO.Execute(SQL)
While not rsGroups.EOF
		
		arrData(rsGroups("hour"),2) = rsGroups("Money")
		arrData(rsGroups("hour"),3) = rsGroups("Volume")
		arrData(rsGroups("hour"),4) = rsGroups("Sales")

  rsGroups.MoveNext

'Initialize <graph> element
strXML = "<graph caption='      Report By Sales in " & var & "' subcaption='"&fecha&" to "&fecha1&"            ' hovercapbg='FFECAA' chartBottomMargin='0' hovercapborder='F47E00' formatNumberScale='0' decimalPrecision='2' showvalues='0' numdivlines='5' numVdivlines='0' yaxisminvalue='0' rotateNames='0' xAxisName='Hours' yAxisName='Values'>"

'Initialize <categories> element - necessary to generate a multi-series chart
strCategories = "<categories>"


	 
'Initiate <dataset> elements
strtank1 = "<dataset seriesName=' Money ($)' color='AF00F8' >"
strtank2 = "<dataset seriesName=' Volume (Lts)' color='F6000F' >"
strtank3 = "<dataset seriesName=' Sales (Count)' color='AFBBF8' >"

 

'//////// armo el xml llenando los campos vacios hacia atras
 For i=0 to UBound(arrData)-1
		'Append <category name='...' /> to strCategories
		strCategories = strCategories & "<category name='" & arrData(i,1) & "' />"
		'Add <set value='...' /> to both the datasets

			strtank1 = strtank1 & "<set value='" & arrData(i,2) & "' />"

			strtank2 = strtank2 & "<set value='" & arrData(i,3) & "' />"	

			strtank3 = strtank3 & "<set value='" & arrData(i,4) & "' />"
		
   Next
 

  

Wend
   'Close <categories> element
   strCategories = strCategories & "</categories>"
   
      'Close <dataset> elements
   strtank1 = strtank1 & "</dataset>"
   strtank2 = strtank2 & "</dataset>"
   strtank3 = strtank3 & "</dataset>"


   'Assemble the entire XML now
'   ///////////// definir esto cuando capturo los datos del form ////////////

Select Case (var2)
   Case "Money":
       'Sentencias 
       strXML = strXML & strCategories & strtank1 & "</graph>"'
   Case "Volume":
       'Sentencias 
       strXML = strXML & strCategories & strtank2 & "</graph>"'
   Case "Sales":
       'Sentencias 
       strXML = strXML & strCategories & strtank3 & "</graph>"'
   Case else:
       'Sentencias 
       strXML = strXML & strCategories & strtank1 & strtank2 & strtank3 & "</graph>"'
End Select

Response.ContentType = "text/xml"
'Just write out the XML data
'NOTE THAT THIS PAGE DOESN'T CONTAIN ANY HTML TAG, WHATSOEVER
 Response.Write(strXML)
'end if


End Function 'Close Page_BeforeShow @1-54C34B28


%>
