<%
'BindEvents Method @1-6F69CA40
Sub BindEvents(Level)
    If Level="Page" Then
    Else
        Set pump_sales_Sites.Navigator.CCSEvents("BeforeShow") = GetRef("pump_sales_Sites_Page_Footer_Navigator_BeforeShow")
        Set Sites_pump_sales.CCSEvents("BeforeShow") = GetRef("Sites_pump_sales_BeforeShow")
        Set Report_Print.CCSEvents("BeforeShow") = GetRef("Report_Print_BeforeShow")
    End If
End Sub
'End BindEvents Method

Function pump_sales_Sites_Page_Footer_Navigator_BeforeShow(Sender) 'pump_sales_Sites_Page_Footer_Navigator_BeforeShow @97-FC439AC3

'Hide-Show Component @98-75B3DAA5
    Dim TotalPages_98_1 : TotalPages_98_1 = CCSConverter.VBSConvert(ccsInteger, pump_sales_Sites.DataSource.Recordset.PageCount)
    Dim Param2_98_2 : Param2_98_2 = CCSConverter.VBSConvert(ccsInteger, 2)
    If  (Not IsEmpty(TotalPages_98_1) And Not IsEmpty(Param2_98_2) And TotalPages_98_1 < Param2_98_2) Then _
        pump_sales_Sites.Navigator.Visible = False
'End Hide-Show Component

End Function 'Close pump_sales_Sites_Page_Footer_Navigator_BeforeShow @97-54C34B28

Function Sites_pump_sales_BeforeShow(Sender) 'Sites_pump_sales_BeforeShow @72-46FAB467

'Hide-Show Component @80-63B10D5D
    Dim ViewMode_80_1 : ViewMode_80_1 = CCSConverter.VBSConvert(ccsText, CCGetFromGet("ViewMode", Empty))
    Dim Param2_80_2 : Param2_80_2 = CCSConverter.VBSConvert(ccsText, "Print")
    If (IsEmpty(ViewMode_80_1) And IsEmpty(Param2_80_2)) Or  (Not IsEmpty(ViewMode_80_1) And Not IsEmpty(Param2_80_2) And ViewMode_80_1 = Param2_80_2) Then _
        Sites_pump_sales.Visible = False
'End Hide-Show Component

End Function 'Close Sites_pump_sales_BeforeShow @72-54C34B28

Function Report_Print_BeforeShow(Sender) 'Report_Print_BeforeShow @77-D2FE8D6A

'Hide-Show Component @79-EE0F25B0
    Dim ViewMode_79_1 : ViewMode_79_1 = CCSConverter.VBSConvert(ccsText, CCGetFromGet("ViewMode", Empty))
    Dim Param2_79_2 : Param2_79_2 = CCSConverter.VBSConvert(ccsText, "Print")
    If (IsEmpty(ViewMode_79_1) And IsEmpty(Param2_79_2)) Or  (Not IsEmpty(ViewMode_79_1) And Not IsEmpty(Param2_79_2) And ViewMode_79_1 = Param2_79_2) Then _
        Report_Print.Visible = False
'End Hide-Show Component

End Function 'Close Report_Print_BeforeShow @77-54C34B28


%>
