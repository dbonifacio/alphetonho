<%
'BindEvents Method @1-F19EB633
Sub BindEvents(Level)
    If Level="Page" Then
    Else
        Set Report1.Navigator.CCSEvents("BeforeShow") = GetRef("Report1_Page_Footer_Navigator_BeforeShow")
        Set Report_Print.CCSEvents("BeforeShow") = GetRef("Report_Print_BeforeShow")
    End If
End Sub
'End BindEvents Method

Function Report1_Page_Footer_Navigator_BeforeShow(Sender) 'Report1_Page_Footer_Navigator_BeforeShow @23-0AA8A152

'Hide-Show Component @24-C58379EB
    Dim TotalPages_24_1 : TotalPages_24_1 = CCSConverter.VBSConvert(ccsInteger, Report1.DataSource.Recordset.PageCount)
    Dim Param2_24_2 : Param2_24_2 = CCSConverter.VBSConvert(ccsInteger, 2)
    If  (Not IsEmpty(TotalPages_24_1) And Not IsEmpty(Param2_24_2) And TotalPages_24_1 < Param2_24_2) Then _
        Report1.Navigator.Visible = False
'End Hide-Show Component

End Function 'Close Report1_Page_Footer_Navigator_BeforeShow @23-54C34B28

Function Report_Print_BeforeShow(Sender) 'Report_Print_BeforeShow @3-D2FE8D6A

'Hide-Show Component @5-568D7866
    Dim ViewMode_5_1 : ViewMode_5_1 = CCSConverter.VBSConvert(ccsText, CCGetFromGet("ViewMode", Empty))
    Dim Param2_5_2 : Param2_5_2 = CCSConverter.VBSConvert(ccsText, "Print")
    If (IsEmpty(ViewMode_5_1) And IsEmpty(Param2_5_2)) Or  (Not IsEmpty(ViewMode_5_1) And Not IsEmpty(Param2_5_2) And ViewMode_5_1 = Param2_5_2) Then _
        Report_Print.Visible = False
'End Hide-Show Component

End Function 'Close Report_Print_BeforeShow @3-54C34B28


%>
