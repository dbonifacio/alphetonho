<%@ CodePage=1252 %>
<%
'Include Common Files @1-AA7D7934
%>
<!-- #INCLUDE VIRTUAL="/Alpheton/Common.asp"-->
<!-- #INCLUDE VIRTUAL="/Alpheton/Cache.asp" -->
<!-- #INCLUDE VIRTUAL="/Alpheton/Template.asp" -->
<!-- #INCLUDE VIRTUAL="/Alpheton/Sorter.asp" -->
<!-- #INCLUDE VIRTUAL="/Alpheton/Navigator.asp" -->
<!-- #INCLUDE VIRTUAL="/Alpheton/Services.asp" -->
<%
'End Include Common Files

'Initialize Page @1-1464A3C3
' Variables
Dim PathToRoot, ScriptPath, TemplateFilePath
Dim FileName
Dim Redirect
Dim IsService
Dim Tpl, HTMLTemplate
Dim TemplateFileName
Dim ComponentName
Dim PathToCurrentPage
Dim Attributes

' Events
Dim CCSEvents
Dim CCSEventResult

' Connections
Dim DBFusionHO

' Page controls
Dim Sites
Dim Footer
Dim Header
Dim ChildControls

Session.CodePage = CCSLocales.Locale.CodePage
Response.Charset = CCSLocales.Locale.Charset
Response.ContentType = CCSContentType
IsService = False
Redirect = ""
TemplateFileName = "Sites_maint.html"
Set CCSEvents = CreateObject("Scripting.Dictionary")
PathToCurrentPage = "./"
FileName = "Sites_maint.asp"
PathToRoot = "./"
ScriptPath = Left(Request.ServerVariables("PATH_TRANSLATED"), Len(Request.ServerVariables("PATH_TRANSLATED")) - Len(FileName))
TemplateFilePath = ScriptPath
'End Initialize Page

'Authenticate User @1-78185724
CCSecurityRedirect "1", Empty
'End Authenticate User

'Initialize Objects @1-0A73DC89
BindEvents "Page"
CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeInitialize", Nothing)

Set DBFusionHO = New clsDBFusionHO
DBFusionHO.Open
Set Attributes = New clsAttributes
Attributes("pathToRoot") = PathToRoot

' Controls
Set Sites = new clsRecordSites
Set Footer = New clsFooter
Set Footer.Attributes = Attributes
Footer.Initialize "Footer", ""
Set Header = New clsHeader
Set Header.Attributes = Attributes
Header.Initialize "Header", ""
Sites.Initialize DBFusionHO

' Events
%>
<!-- #INCLUDE VIRTUAL="/Alpheton/Sites_maint_events.asp" -->
<%
BindEvents Empty

CCSEventResult = CCRaiseEvent(CCSEvents, "AfterInitialize", Nothing)
'End Initialize Objects

'Execute Components @1-2ABF33F2
Sites.Operation
Footer.Operations
Header.Operations
'End Execute Components

'Go to destination page @1-6D35F4FD
If NOT ( Redirect = "" ) Then
    UnloadPage
    Response.Redirect Redirect
End If
'End Go to destination page

'Initialize HTML Template @1-2E9DB4BC
CCSEventResult = CCRaiseEvent(CCSEvents, "OnInitializeView", Nothing)
Set HTMLTemplate = new clsTemplate
Set HTMLTemplate.Cache = TemplatesRepository
HTMLTemplate.LoadTemplate TemplateFilePath & TemplateFileName
HTMLTemplate.SetVar "@CCS_PathToRoot", PathToRoot
Set Tpl = HTMLTemplate.Block("main")
CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShow", Nothing)
'End Initialize HTML Template

'Show Page @1-D343F1ED
Attributes.Show HTMLTemplate, "page:"
Set ChildControls = CCCreateCollection(Tpl, Null, ccsParseOverwrite, _
    Array(Sites, Footer, Header))
ChildControls.Show
Dim MainHTML
HTMLTemplate.Parse "main", False
If IsEmpty(MainHTML) Then MainHTML = HTMLTemplate.GetHTML("main")
CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeOutput", Nothing)
If CCSEventResult Then Response.Write MainHTML
'End Show Page

'Unload Page @1-CB210C62
UnloadPage
Set Tpl = Nothing
Set HTMLTemplate = Nothing
'End Unload Page

'UnloadPage Sub @1-17281094
Sub UnloadPage()
    CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeUnload", Nothing)
    If DBFusionHO.State = adStateOpen Then _
        DBFusionHO.Close
    Set DBFusionHO = Nothing
    Set CCSEvents = Nothing
    Set Attributes = Nothing
    Set Sites = Nothing
    Footer.UnloadPage
    Set Footer = Nothing
    Header.UnloadPage
    Set Header = Nothing
End Sub
'End UnloadPage Sub

Class clsRecordSites 'Sites Class @2-C5C1F907

'Sites Variables @2-81D6750E

    ' Public variables
    Public ComponentName
    Public HTMLFormAction
    Public PressedButton
    Public Errors
    Public FormSubmitted
    Public EditMode
    Public Visible
    Public Recordset
    Public TemplateBlock
    Public Attributes

    Public CCSEvents
    Private CCSEventResult

    Public InsertAllowed
    Public UpdateAllowed
    Public DeleteAllowed
    Public ReadAllowed
    Public DataSource
    Public Command
    Public ValidatingControls
    Public Controls

    ' Class variables
    Dim SiteName
    Dim Address
    Dim Latitud
    Dim Longitud
    Dim Tm_id
    Dim Zip
    Dim Phone
    Dim Mail
    Dim Valid
    Dim Country_id
    Dim City_id
    Dim Site_id
    Dim Link1
    Dim Link2
    Dim Link3
    Dim Link4
    Dim Service_id
    Dim Brand_id
    Dim Local_type_id
    Dim Link6
    Dim Link7
    Dim Link8
    Dim Region_id
    Dim Link9
    Dim Moso
    Dim Button_Insert1
    Dim Button_Update
    Dim Button_Delete
    Dim Button_Cancel
'End Sites Variables

'Sites Class_Initialize Event @2-D7CC9358
    Private Sub Class_Initialize()

        Visible = True
        Set Errors = New clsErrors
        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Attributes = New clsAttributes
        Set DataSource = New clsSitesDataSource
        Set Command = New clsCommand
        InsertAllowed = True
        UpdateAllowed = True
        DeleteAllowed = True
        ReadAllowed = True
        Dim Method
        Dim OperationMode
        OperationMode = Split(CCGetFromGet("ccsForm", Empty), ":")
        If UBound(OperationMode) > -1 Then 
            FormSubmitted = (OperationMode(0) = "Sites")
        End If
        If UBound(OperationMode) > 0 Then 
            EditMode = (OperationMode(1) = "Edit")
        End If
        ComponentName = "Sites"
        Method = IIf(FormSubmitted, ccsPost, ccsGet)
        Set SiteName = CCCreateControl(ccsTextBox, "SiteName", "Name", ccsText, Empty, CCGetRequestParam("SiteName", Method))
        Set Address = CCCreateControl(ccsTextBox, "Address", "Address", ccsText, Empty, CCGetRequestParam("Address", Method))
        Set Latitud = CCCreateControl(ccsTextBox, "Latitud", "Latitud", ccsFloat, Empty, CCGetRequestParam("Latitud", Method))
        Set Longitud = CCCreateControl(ccsTextBox, "Longitud", "Longitud", ccsFloat, Empty, CCGetRequestParam("Longitud", Method))
        Set Tm_id = CCCreateList(ccsListBox, "Tm_id", "Tm Id", ccsInteger, CCGetRequestParam("Tm_id", Method), Empty)
        Tm_id.BoundColumn = "Tm_id"
        Tm_id.TextColumn = "Tm_name"
        Set Tm_id.DataSource = CCCreateDataSource(dsTable,DBFusionHO, Array("SELECT *  " & _
"FROM Tm {SQL_Where} {SQL_OrderBy}", "", ""))
        With Tm_id.DataSource.WhereParameters
            Set .ParameterSources = Server.CreateObject("Scripting.Dictionary")
            .ParameterSources("expr33") = True
            .AddParameter 1, "expr33", ccsBoolean, Array("True", "False", Empty), Array(1, 0, Empty), Empty, False
            .Criterion(1) = .Operation(opEqual, False, "[Valid]", .getParamByID(1))
            .AssembledWhere = .Criterion(1)
        End With
        Tm_id.DataSource.Where = Tm_id.DataSource.WhereParameters.AssembledWhere
        Set Zip = CCCreateControl(ccsTextBox, "Zip", "Zip", ccsText, Empty, CCGetRequestParam("Zip", Method))
        Set Phone = CCCreateControl(ccsTextBox, "Phone", "Phone", ccsText, Empty, CCGetRequestParam("Phone", Method))
        Set Mail = CCCreateControl(ccsTextBox, "Mail", "Mail", ccsText, Empty, CCGetRequestParam("Mail", Method))
        Set Valid = CCCreateControl(ccsCheckBox, "Valid", Empty, ccsBoolean, DefaultBooleanFormat, CCGetRequestParam("Valid", Method))
        Set Country_id = CCCreateList(ccsListBox, "Country_id", "Country Id", ccsInteger, CCGetRequestParam("Country_id", Method), Empty)
        Country_id.BoundColumn = "Country_id"
        Country_id.TextColumn = "Country_name"
        Set Country_id.DataSource = CCCreateDataSource(dsTable,DBFusionHO, Array("SELECT *  " & _
"FROM Country {SQL_Where} {SQL_OrderBy}", "", ""))
        Set City_id = CCCreateList(ccsListBox, "City_id", "City Id", ccsInteger, CCGetRequestParam("City_id", Method), Empty)
        City_id.BoundColumn = "City_id"
        City_id.TextColumn = "City_name"
        Set City_id.DataSource = CCCreateDataSource(dsTable,DBFusionHO, Array("SELECT *  " & _
"FROM City {SQL_Where} {SQL_OrderBy}", "", ""))
        Set Site_id = CCCreateControl(ccsTextBox, "Site_id", Empty, ccsText, Empty, CCGetRequestParam("Site_id", Method))
        Set Link1 = CCCreateControl(ccsLink, "Link1", Empty, ccsText, Empty, CCGetRequestParam("Link1", Method))
        Set Link2 = CCCreateControl(ccsLink, "Link2", Empty, ccsText, Empty, CCGetRequestParam("Link2", Method))
        Set Link3 = CCCreateControl(ccsLink, "Link3", Empty, ccsText, Empty, CCGetRequestParam("Link3", Method))
        Set Link4 = CCCreateControl(ccsLink, "Link4", Empty, ccsText, Empty, CCGetRequestParam("Link4", Method))
        Set Service_id = CCCreateList(ccsListBox, "Service_id", "Service Id", ccsInteger, CCGetRequestParam("Service_id", Method), Empty)
        Service_id.BoundColumn = "Service_id"
        Service_id.TextColumn = "Service_name"
        Set Service_id.DataSource = CCCreateDataSource(dsTable,DBFusionHO, Array("SELECT *  " & _
"FROM Service {SQL_Where} {SQL_OrderBy}", "", ""))
        Set Brand_id = CCCreateList(ccsListBox, "Brand_id", CCSLocales.GetText("Brand", ""), ccsInteger, CCGetRequestParam("Brand_id", Method), Empty)
        Brand_id.BoundColumn = "Brand_id"
        Brand_id.TextColumn = "Brand_name"
        Set Brand_id.DataSource = CCCreateDataSource(dsTable,DBFusionHO, Array("SELECT *  " & _
"FROM Brand {SQL_Where} {SQL_OrderBy}", "", ""))
        Set Local_type_id = CCCreateList(ccsListBox, "Local_type_id", "Local_type Id", ccsInteger, CCGetRequestParam("Local_type_id", Method), Empty)
        Local_type_id.BoundColumn = "Local_type_id"
        Local_type_id.TextColumn = "Local_type_name"
        Set Local_type_id.DataSource = CCCreateDataSource(dsTable,DBFusionHO, Array("SELECT *  " & _
"FROM Local_type {SQL_Where} {SQL_OrderBy}", "", ""))
        Set Link6 = CCCreateControl(ccsLink, "Link6", Empty, ccsText, Empty, CCGetRequestParam("Link6", Method))
        Set Link7 = CCCreateControl(ccsLink, "Link7", Empty, ccsText, Empty, CCGetRequestParam("Link7", Method))
        Set Link8 = CCCreateControl(ccsLink, "Link8", Empty, ccsText, Empty, CCGetRequestParam("Link8", Method))
        Set Region_id = CCCreateList(ccsListBox, "Region_id", "Region Id", ccsInteger, CCGetRequestParam("Region_id", Method), Empty)
        Region_id.BoundColumn = "Region_id"
        Region_id.TextColumn = "Region_name"
        Set Region_id.DataSource = CCCreateDataSource(dsTable,DBFusionHO, Array("SELECT *  " & _
"FROM Region {SQL_Where} {SQL_OrderBy}", "", ""))
        Set Link9 = CCCreateControl(ccsLink, "Link9", Empty, ccsText, Empty, CCGetRequestParam("Link9", Method))
        Set Moso = CCCreateList(ccsListBox, "Moso", Empty, ccsText, CCGetRequestParam("Moso", Method), Empty)
        Moso.BoundColumn = "Moso_id"
        Moso.TextColumn = "Moso_name"
        Set Moso.DataSource = CCCreateDataSource(dsTable,DBFusionHO, Array("SELECT *  " & _
"FROM Moso {SQL_Where} {SQL_OrderBy}", "", ""))
        Set Button_Insert1 = CCCreateButton("Button_Insert1", Method)
        Set Button_Update = CCCreateButton("Button_Update", Method)
        Set Button_Delete = CCCreateButton("Button_Delete", Method)
        Set Button_Cancel = CCCreateButton("Button_Cancel", Method)
        Set ValidatingControls = new clsControls
        ValidatingControls.addControls Array(SiteName, Address, Latitud, Longitud, Tm_id, Zip, Phone, _
             Mail, Valid, Country_id, City_id, Site_id, Service_id, Brand_id, Local_type_id, Region_id, Moso)
    End Sub
'End Sites Class_Initialize Event

'Sites Initialize Method @2-3A8882A3
    Sub Initialize(objConnection)

        If NOT Visible Then Exit Sub


        Set DataSource.Connection = objConnection
        With DataSource
            .Parameters("urlsite_id") = CCGetRequestParam("site_id", ccsGET)
        End With
    End Sub
'End Sites Initialize Method

'Sites Class_Terminate Event @2-0C5D276C
    Private Sub Class_Terminate()
        Set Errors = Nothing
        Set Attributes = Nothing
    End Sub
'End Sites Class_Terminate Event

'Sites Validate Method @2-B9D513CF
    Function Validate()
        Dim Validation
        ValidatingControls.Validate
        CCSEventResult = CCRaiseEvent(CCSEvents, "OnValidate", Me)
        Validate = ValidatingControls.isValid() And (Errors.Count = 0)
    End Function
'End Sites Validate Method

'Sites Operation Method @2-05A6D508
    Sub Operation()
        If NOT ( Visible AND FormSubmitted ) Then Exit Sub

        If FormSubmitted Then
            PressedButton = IIf(EditMode, "Button_Update", "Button_Insert1")
            If Button_Insert1.Pressed Then
                PressedButton = "Button_Insert1"
            ElseIf Button_Update.Pressed Then
                PressedButton = "Button_Update"
            ElseIf Button_Delete.Pressed Then
                PressedButton = "Button_Delete"
            ElseIf Button_Cancel.Pressed Then
                PressedButton = "Button_Cancel"
            End If
        End If
        Redirect = "Sites_list.asp"
        If PressedButton = "Button_Delete" Then
            If NOT Button_Delete.OnClick OR NOT DeleteRow() Then
                Redirect = ""
            End If
        ElseIf PressedButton = "Button_Cancel" Then
            If NOT Button_Cancel.OnClick Then
                Redirect = ""
            End If
        ElseIf Validate() Then
            If PressedButton = "Button_Insert1" Then
                If NOT Button_Insert1.OnClick() OR NOT InsertRow() Then
                    Redirect = ""
                End If
            ElseIf PressedButton = "Button_Update" Then
                If NOT Button_Update.OnClick() OR NOT UpdateRow() Then
                    Redirect = ""
                End If
            End If
        Else
            Redirect = ""
        End If
    End Sub
'End Sites Operation Method

'Sites InsertRow Method @2-E2C3FB7A
    Function InsertRow()
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeInsert", Me)
        If NOT InsertAllowed Then InsertRow = False : Exit Function
        DataSource.SiteName.Value = SiteName.Value
        DataSource.Address.Value = Address.Value
        DataSource.Latitud.Value = Latitud.Value
        DataSource.Longitud.Value = Longitud.Value
        DataSource.Tm_id.Value = Tm_id.Value
        DataSource.Zip.Value = Zip.Value
        DataSource.Phone.Value = Phone.Value
        DataSource.Mail.Value = Mail.Value
        DataSource.Valid.Value = Valid.Value
        DataSource.Country_id.Value = Country_id.Value
        DataSource.City_id.Value = City_id.Value
        DataSource.Site_id.Value = Site_id.Value
        DataSource.Service_id.Value = Service_id.Value
        DataSource.Brand_id.Value = Brand_id.Value
        DataSource.Local_type_id.Value = Local_type_id.Value
        DataSource.Region_id.Value = Region_id.Value
        DataSource.Insert(Command)


        CCSEventResult = CCRaiseEvent(CCSEvents, "AfterInsert", Me)
        If DataSource.Errors.Count > 0 Then
            Errors.AddErrors(DataSource.Errors)
            DataSource.Errors.Clear
        End If
        InsertRow = (Errors.Count = 0)
    End Function
'End Sites InsertRow Method

'Sites UpdateRow Method @2-F11B60A4
    Function UpdateRow()
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeUpdate", Me)
        If NOT UpdateAllowed Then UpdateRow = False : Exit Function
        DataSource.SiteName.Value = SiteName.Value
        DataSource.Address.Value = Address.Value
        DataSource.Latitud.Value = Latitud.Value
        DataSource.Longitud.Value = Longitud.Value
        DataSource.Tm_id.Value = Tm_id.Value
        DataSource.Zip.Value = Zip.Value
        DataSource.Phone.Value = Phone.Value
        DataSource.Mail.Value = Mail.Value
        DataSource.Valid.Value = Valid.Value
        DataSource.Country_id.Value = Country_id.Value
        DataSource.City_id.Value = City_id.Value
        DataSource.Site_id.Value = Site_id.Value
        DataSource.Service_id.Value = Service_id.Value
        DataSource.Brand_id.Value = Brand_id.Value
        DataSource.Local_type_id.Value = Local_type_id.Value
        DataSource.Region_id.Value = Region_id.Value
        DataSource.Update(Command)


        CCSEventResult = CCRaiseEvent(CCSEvents, "AfterUpdate", Me)
        If DataSource.Errors.Count > 0 Then
            Errors.AddErrors(DataSource.Errors)
            DataSource.Errors.Clear
        End If
        UpdateRow = (Errors.Count = 0)
    End Function
'End Sites UpdateRow Method

'Sites DeleteRow Method @2-461DEBD9
    Function DeleteRow()
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeDelete", Me)
        If NOT DeleteAllowed Then DeleteRow = False : Exit Function
        With Command.WhereParameters
            .ParameterSources("urlsite_id") = CCGetRequestParam("site_id", ccsGET)
        End With
        DataSource.Delete(Command)


        CCSEventResult = CCRaiseEvent(CCSEvents, "AfterDelete", Me)
        If DataSource.Errors.Count > 0 Then
            Errors.AddErrors(DataSource.Errors)
            DataSource.Errors.Clear
        End If
        DeleteRow = (Errors.Count = 0)
    End Function
'End Sites DeleteRow Method

'Sites Show Method @2-AC86C644
    Sub Show(Tpl)

        If NOT Visible Then Exit Sub

        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeSelect", Me)
        Set Recordset = DataSource.Open(Command)
        EditMode = Recordset.EditMode(ReadAllowed)
        HTMLFormAction = FileName & "?" & CCAddParam(Request.ServerVariables("QUERY_STRING"), "ccsForm", "Sites" & IIf(EditMode, ":Edit", ""))
        Set TemplateBlock = Tpl.Block("Record " & ComponentName)
        If TemplateBlock is Nothing Then Exit Sub
        TemplateBlock.Variable("HTMLFormName") = ComponentName
        TemplateBlock.Variable("HTMLFormEnctype") ="application/x-www-form-urlencoded"
        If DataSource.Errors.Count > 0 Then
            Errors.AddErrors(DataSource.Errors)
            DataSource.Errors.Clear
            With TemplateBlock.Block("Error")
                .Variable("Error") = Errors.ToString
                .Parse False
            End With
        End If
        Set Controls = CCCreateCollection(TemplateBlock, Null, ccsParseOverwrite, _
            Array(Site_id,  SiteName,  Address,  Moso,  Link9,  Latitud,  Longitud, _
                 Tm_id,  Link4,  Zip,  Phone,  Mail,  Valid,  Local_type_id,  Link8, _
                 Brand_id,  Link7,  Service_id,  Link6,  Country_id,  Link2,  Region_id,  Link1, _
                 City_id,  Link3,  Button_Insert1,  Button_Update,  Button_Delete,  Button_Cancel))
        If EditMode AND ReadAllowed Then
            If Errors.Count = 0 Then
                If Recordset.Errors.Count > 0 Then
                    With TemplateBlock.Block("Error")
                        .Variable("Error") = Recordset.Errors.ToString
                        .Parse False
                    End With
                ElseIf Recordset.CanPopulate() Then
                    If Not FormSubmitted Then
                        SiteName.Value = Recordset.Fields("SiteName")
                        Address.Value = Recordset.Fields("Address")
                        Latitud.Value = Recordset.Fields("Latitud")
                        Longitud.Value = Recordset.Fields("Longitud")
                        Tm_id.Value = Recordset.Fields("Tm_id")
                        Zip.Value = Recordset.Fields("Zip")
                        Phone.Value = Recordset.Fields("Phone")
                        Mail.Value = Recordset.Fields("Mail")
                        Valid.Value = Recordset.Fields("Valid")
                        Country_id.Value = Recordset.Fields("Country_id")
                        City_id.Value = Recordset.Fields("City_id")
                        Site_id.Value = Recordset.Fields("Site_id")
                        Service_id.Value = Recordset.Fields("Service_id")
                        Brand_id.Value = Recordset.Fields("Brand_id")
                        Local_type_id.Value = Recordset.Fields("Local_type_id")
                        Region_id.Value = Recordset.Fields("Region_id")
                        
                    End If
                Else
                    EditMode = False
                End If
            End If
            If EditMode Then
                
                
                
                
                
                
                
                
            End If
        End If
        Link1.Parameters = CCGetQueryString("QueryString", Array("ccsForm"))
        Link1.Page = "AddRegion.asp"
        Link2.Parameters = CCGetQueryString("QueryString", Array("ccsForm"))
        Link2.Page = "AddCountry.asp"
        Link3.Parameters = CCGetQueryString("QueryString", Array("ccsForm"))
        Link3.Page = "AddCity.asp"
        Link4.Parameters = CCGetQueryString("QueryString", Array("ccsForm"))
        Link4.Page = "AddTm.asp"
        Link6.Parameters = CCGetQueryString("QueryString", Array("ccsForm"))
        Link6.Page = "AddService.asp"
        Link7.Parameters = CCGetQueryString("QueryString", Array("ccsForm"))
        Link7.Page = ServerURL & "AddBrand.asp"
        Link8.Parameters = CCGetQueryString("QueryString", Array("ccsForm"))
        Link8.Page = "AddLocalType.asp"
        Link9.Parameters = CCGetQueryString("QueryString", Array("ccsForm"))
        Link9.Page = "AddMoso.asp"
        If Not FormSubmitted Then
        End If
        If FormSubmitted Then
            Errors.AddErrors SiteName.Errors
            Errors.AddErrors Address.Errors
            Errors.AddErrors Latitud.Errors
            Errors.AddErrors Longitud.Errors
            Errors.AddErrors Tm_id.Errors
            Errors.AddErrors Zip.Errors
            Errors.AddErrors Phone.Errors
            Errors.AddErrors Mail.Errors
            Errors.AddErrors Valid.Errors
            Errors.AddErrors Country_id.Errors
            Errors.AddErrors City_id.Errors
            Errors.AddErrors Site_id.Errors
            Errors.AddErrors Service_id.Errors
            Errors.AddErrors Brand_id.Errors
            Errors.AddErrors Local_type_id.Errors
            Errors.AddErrors Region_id.Errors
            Errors.AddErrors Moso.Errors
            Errors.AddErrors DataSource.Errors
            With TemplateBlock.Block("Error")
                .Variable("Error") = Errors.ToString()
                .Parse False
            End With
        End If
        TemplateBlock.Variable("Action") = IIF(CCSUseAmps, Replace(HTMLFormAction, "&", CCSAmps), HTMLFormAction)
        Button_Insert1.Visible = NOT EditMode AND InsertAllowed
        Button_Update.Visible = EditMode AND UpdateAllowed
        Button_Delete.Visible = EditMode AND DeleteAllowed

        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeShow", Me)
        If Visible Then 
            Attributes.Show TemplateBlock, "Sites" & ":"
            Controls.Show
        End If
    End Sub
'End Sites Show Method

End Class 'End Sites Class @2-A61BA892

Class clsSitesDataSource 'SitesDataSource Class @2-DCC7012B

'DataSource Variables @2-0C407576
    Public Errors, Connection, Parameters, CCSEvents

    Public Recordset
    Public SQL, CountSQL, Order, Where, Orders, StaticOrder
    Public PageSize
    Public PageCount
    Public AbsolutePage
    Public Fields
    Dim WhereParameters
    Public AllParamsSet
    Public CmdExecution
    Public InsertOmitIfEmpty
    Public UpdateOmitIfEmpty

    Private CurrentOperation
    Private CCSEventResult

    ' Datasource fields
    Public SiteName
    Public Address
    Public Latitud
    Public Longitud
    Public Tm_id
    Public Zip
    Public Phone
    Public Mail
    Public Valid
    Public Country_id
    Public City_id
    Public Site_id
    Public Service_id
    Public Brand_id
    Public Local_type_id
    Public Region_id
'End DataSource Variables

'DataSource Class_Initialize Event @2-18F477A3
    Private Sub Class_Initialize()

        Set CCSEvents = CreateObject("Scripting.Dictionary")
        Set Fields = New clsFields
        Set Recordset = New clsDataSource
        Set Recordset.DataSource = Me
        Set Errors = New clsErrors
        Set Connection = Nothing
        AllParamsSet = True
        Set SiteName = CCCreateField("SiteName", "SiteName", ccsText, Empty, Recordset)
        Set Address = CCCreateField("Address", "Address", ccsText, Empty, Recordset)
        Set Latitud = CCCreateField("Latitud", "Latitud", ccsFloat, Empty, Recordset)
        Set Longitud = CCCreateField("Longitud", "Longitud", ccsFloat, Empty, Recordset)
        Set Tm_id = CCCreateField("Tm_id", "Tm_id", ccsInteger, Empty, Recordset)
        Set Zip = CCCreateField("Zip", "Zip", ccsText, Empty, Recordset)
        Set Phone = CCCreateField("Phone", "Phone", ccsText, Empty, Recordset)
        Set Mail = CCCreateField("Mail", "Mail", ccsText, Empty, Recordset)
        Set Valid = CCCreateField("Valid", "Valid", ccsBoolean, Array(1, 0, Empty), Recordset)
        Set Country_id = CCCreateField("Country_id", "Country_id", ccsInteger, Empty, Recordset)
        Set City_id = CCCreateField("City_id", "City_id", ccsInteger, Empty, Recordset)
        Set Site_id = CCCreateField("Site_id", "site_id", ccsText, Empty, Recordset)
        Set Service_id = CCCreateField("Service_id", "Service_id", ccsInteger, Empty, Recordset)
        Set Brand_id = CCCreateField("Brand_id", "Brand_id", ccsInteger, Empty, Recordset)
        Set Local_type_id = CCCreateField("Local_type_id", "Local_type_id", ccsInteger, Empty, Recordset)
        Set Region_id = CCCreateField("Region_id", "Region_id", ccsInteger, Empty, Recordset)
        Fields.AddFields Array(SiteName,  Address,  Latitud,  Longitud,  Tm_id,  Zip,  Phone, _
             Mail,  Valid,  Country_id,  City_id,  Site_id,  Service_id,  Brand_id,  Local_type_id,  Region_id)
        Set InsertOmitIfEmpty = CreateObject("Scripting.Dictionary")
        InsertOmitIfEmpty.Add "SiteName", True
        InsertOmitIfEmpty.Add "Address", True
        InsertOmitIfEmpty.Add "Latitud", True
        InsertOmitIfEmpty.Add "Longitud", True
        InsertOmitIfEmpty.Add "Tm_id", True
        InsertOmitIfEmpty.Add "Zip", True
        InsertOmitIfEmpty.Add "Phone", True
        InsertOmitIfEmpty.Add "Mail", True
        InsertOmitIfEmpty.Add "Valid", False
        InsertOmitIfEmpty.Add "Country_id", True
        InsertOmitIfEmpty.Add "City_id", True
        InsertOmitIfEmpty.Add "Site_id", True
        InsertOmitIfEmpty.Add "Service_id", True
        InsertOmitIfEmpty.Add "Brand_id", True
        InsertOmitIfEmpty.Add "Local_type_id", True
        InsertOmitIfEmpty.Add "Region_id", True
        Set UpdateOmitIfEmpty = CreateObject("Scripting.Dictionary")
        UpdateOmitIfEmpty.Add "SiteName", True
        UpdateOmitIfEmpty.Add "Address", True
        UpdateOmitIfEmpty.Add "Latitud", True
        UpdateOmitIfEmpty.Add "Longitud", True
        UpdateOmitIfEmpty.Add "Tm_id", True
        UpdateOmitIfEmpty.Add "Zip", True
        UpdateOmitIfEmpty.Add "Phone", True
        UpdateOmitIfEmpty.Add "Mail", True
        UpdateOmitIfEmpty.Add "Valid", False
        UpdateOmitIfEmpty.Add "Country_id", True
        UpdateOmitIfEmpty.Add "City_id", True
        UpdateOmitIfEmpty.Add "Site_id", True
        UpdateOmitIfEmpty.Add "Service_id", True
        UpdateOmitIfEmpty.Add "Brand_id", True
        UpdateOmitIfEmpty.Add "Local_type_id", True
        UpdateOmitIfEmpty.Add "Region_id", True
        Set Parameters = Server.CreateObject("Scripting.Dictionary")
        Set WhereParameters = Nothing

        SQL = "SELECT *  " & vbLf & _
        "FROM Sites {SQL_Where} {SQL_OrderBy}"
        Where = ""
        Order = ""
        StaticOrder = ""
    End Sub
'End DataSource Class_Initialize Event

'BuildTableWhere Method @2-411B5E19
    Public Sub BuildTableWhere()
        Dim WhereParams

        If Not WhereParameters Is Nothing Then _
            Exit Sub
        Set WhereParameters = new clsSQLParameters
        With WhereParameters
            Set .Connection = Connection
            Set .ParameterSources = Parameters
            Set .DataSource = Me
            .AddParameter 1, "urlsite_id", ccsInteger, Empty, Empty, Empty, False
            AllParamsSet = .AllParamsSet
            .Criterion(1) = .Operation(opEqual, False, "site_id", .getParamByID(1))
            .AssembledWhere = .Criterion(1)
            WhereParams = .AssembledWhere
            If Len(Where) > 0 Then 
                If Len(WhereParams) > 0 Then _
                    Where = Where & " AND " & WhereParams
            Else
                If Len(WhereParams) > 0 Then _
                    Where = WhereParams
            End If
        End With
    End Sub
'End BuildTableWhere Method

'Open Method @2-48A2DA7D
    Function Open(Cmd)
        Errors.Clear
        If Connection Is Nothing Then
            Set Open = New clsEmptyDataSource
            Exit Function
        End If
        Set Cmd.Connection = Connection
        Cmd.CommandOperation = cmdOpen
        Cmd.PageSize = PageSize
        Cmd.ActivePage = AbsolutePage
        Cmd.CommandType = dsTable
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeBuildSelect", Me)
        Cmd.SQL = SQL
        BuildTableWhere
        Cmd.Where = Where
        Cmd.OrderBy = Order
        If(Len(StaticOrder)>0) Then
            If Len(Order)>0 Then Cmd.OrderBy = ", "+Cmd.OrderBy
            Cmd.OrderBy = StaticOrder + Cmd.OrderBy
        End If
        Cmd.Options("TOP") = True
        If Not AllParamsSet Then
            Set Open = New clsEmptyDataSource
            Exit Function
        End If
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeExecuteSelect", Me)
        If Errors.Count = 0 And CCSEventResult Then _
            Set Recordset = Cmd.Exec(Errors)
        CCSEventResult = CCRaiseEvent(CCSEvents, "AfterExecuteSelect", Me)
        Set Recordset.FieldsCollection = Fields
        Set Open = Recordset
    End Function
'End Open Method

'DataSource Class_Terminate Event @2-41B4B08D
    Private Sub Class_Terminate()
        If Recordset.State = adStateOpen Then _
            Recordset.Close
        Set Recordset = Nothing
        Set Parameters = Nothing
        Set Errors = Nothing
    End Sub
'End DataSource Class_Terminate Event

'Delete Method @2-D1FC7B70
    Sub Delete(Cmd)
        CmdExecution = True
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeBuildDelete", Me)
        Set Cmd.Connection = Connection
        Cmd.CommandOperation = cmdExec
        Cmd.CommandType = dsTable
        Cmd.CommandParameters = Empty
        With Cmd.WhereParameters
            Set .Connection = Connection
            Set .DataSource = Me
            .AddParameter 1, "urlsite_id", ccsInteger, Empty, Empty, Empty, False
            .Criterion(1) = .Operation(opEqual, False, "site_id", .getParamByID(1))
            .AssembledWhere = .Criterion(1)
            Cmd.Where = .AssembledWhere
            If NOT .AllParamsSet() Then
                Errors.AddError(CCSLocales.GetText("CCS_CustomOperationError_MissingParameters", Empty))
            End If
        End With
        Cmd.SQL = "DELETE FROM [Sites]" & IIf(Len(Cmd.Where) > 0, " WHERE " & Cmd.Where, "")
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeExecuteDelete", Me)
        If Errors.Count = 0  And CmdExecution Then
            Cmd.Exec(Errors)
            CCSEventResult = CCRaiseEvent(CCSEvents, "AfterExecuteDelete", Me)
        End If
    End Sub
'End Delete Method

'Update Method @2-24E72563
    Sub Update(Cmd)
        CmdExecution = True
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeBuildUpdate", Me)
        Set Cmd.Connection = Connection
        Cmd.CommandOperation = cmdExec
        Cmd.CommandType = dsTable
        Cmd.CommandParameters = Empty
        BuildTableWhere
        If NOT AllParamsSet Then
            Errors.AddError(CCSLocales.GetText("CCS_CustomOperationError_MissingParameters", Empty))
        End If
        Dim IsDef_SiteName : IsDef_SiteName = CCIsDefined("SiteName", "Form")
        Dim IsDef_Address : IsDef_Address = CCIsDefined("Address", "Form")
        Dim IsDef_Latitud : IsDef_Latitud = CCIsDefined("Latitud", "Form")
        Dim IsDef_Longitud : IsDef_Longitud = CCIsDefined("Longitud", "Form")
        Dim IsDef_Tm_id : IsDef_Tm_id = CCIsDefined("Tm_id", "Form")
        Dim IsDef_Zip : IsDef_Zip = CCIsDefined("Zip", "Form")
        Dim IsDef_Phone : IsDef_Phone = CCIsDefined("Phone", "Form")
        Dim IsDef_Mail : IsDef_Mail = CCIsDefined("Mail", "Form")
        Dim IsDef_Valid : IsDef_Valid = CCIsDefined("Valid", "Form")
        Dim IsDef_Country_id : IsDef_Country_id = CCIsDefined("Country_id", "Form")
        Dim IsDef_City_id : IsDef_City_id = CCIsDefined("City_id", "Form")
        Dim IsDef_Site_id : IsDef_Site_id = CCIsDefined("Site_id", "Form")
        Dim IsDef_Service_id : IsDef_Service_id = CCIsDefined("Service_id", "Form")
        Dim IsDef_Brand_id : IsDef_Brand_id = CCIsDefined("Brand_id", "Form")
        Dim IsDef_Local_type_id : IsDef_Local_type_id = CCIsDefined("Local_type_id", "Form")
        Dim IsDef_Region_id : IsDef_Region_id = CCIsDefined("Region_id", "Form")
        If Not UpdateOmitIfEmpty("SiteName") Or IsDef_SiteName Then Cmd.AddSQLStrings "[SiteName]=" & Connection.ToSQL(SiteName, SiteName.DataType), Empty
        If Not UpdateOmitIfEmpty("Address") Or IsDef_Address Then Cmd.AddSQLStrings "[Address]=" & Connection.ToSQL(Address, Address.DataType), Empty
        If Not UpdateOmitIfEmpty("Latitud") Or IsDef_Latitud Then Cmd.AddSQLStrings "[Latitud]=" & Connection.ToSQL(Latitud, Latitud.DataType), Empty
        If Not UpdateOmitIfEmpty("Longitud") Or IsDef_Longitud Then Cmd.AddSQLStrings "[Longitud]=" & Connection.ToSQL(Longitud, Longitud.DataType), Empty
        If Not UpdateOmitIfEmpty("Tm_id") Or IsDef_Tm_id Then Cmd.AddSQLStrings "[Tm_id]=" & Connection.ToSQL(Tm_id, Tm_id.DataType), Empty
        If Not UpdateOmitIfEmpty("Zip") Or IsDef_Zip Then Cmd.AddSQLStrings "[Zip]=" & Connection.ToSQL(Zip, Zip.DataType), Empty
        If Not UpdateOmitIfEmpty("Phone") Or IsDef_Phone Then Cmd.AddSQLStrings "[Phone]=" & Connection.ToSQL(Phone, Phone.DataType), Empty
        If Not UpdateOmitIfEmpty("Mail") Or IsDef_Mail Then Cmd.AddSQLStrings "[Mail]=" & Connection.ToSQL(Mail, Mail.DataType), Empty
        If Not UpdateOmitIfEmpty("Valid") Or IsDef_Valid Then Cmd.AddSQLStrings "[Valid]=" & Connection.ToSQL(Valid, Valid.DataType), Empty
        If Not UpdateOmitIfEmpty("Country_id") Or IsDef_Country_id Then Cmd.AddSQLStrings "[Country_id]=" & Connection.ToSQL(Country_id, Country_id.DataType), Empty
        If Not UpdateOmitIfEmpty("City_id") Or IsDef_City_id Then Cmd.AddSQLStrings "[City_id]=" & Connection.ToSQL(City_id, City_id.DataType), Empty
        If Not UpdateOmitIfEmpty("Site_id") Or IsDef_Site_id Then Cmd.AddSQLStrings "site_id=" & Connection.ToSQL(Site_id, Site_id.DataType), Empty
        If Not UpdateOmitIfEmpty("Service_id") Or IsDef_Service_id Then Cmd.AddSQLStrings "[Service_id]=" & Connection.ToSQL(Service_id, Service_id.DataType), Empty
        If Not UpdateOmitIfEmpty("Brand_id") Or IsDef_Brand_id Then Cmd.AddSQLStrings "[Brand_id]=" & Connection.ToSQL(Brand_id, Brand_id.DataType), Empty
        If Not UpdateOmitIfEmpty("Local_type_id") Or IsDef_Local_type_id Then Cmd.AddSQLStrings "[Local_type_id]=" & Connection.ToSQL(Local_type_id, Local_type_id.DataType), Empty
        If Not UpdateOmitIfEmpty("Region_id") Or IsDef_Region_id Then Cmd.AddSQLStrings "[Region_id]=" & Connection.ToSQL(Region_id, Region_id.DataType), Empty
        CmdExecution = Cmd.PrepareSQL("Update", "[Sites]", Where)
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeExecuteUpdate", Me)
        If Errors.Count = 0  And CmdExecution Then
            Cmd.Exec(Errors)
            CCSEventResult = CCRaiseEvent(CCSEvents, "AfterExecuteUpdate", Me)
        End If
    End Sub
'End Update Method

'Insert Method @2-E1D1D9B1
    Sub Insert(Cmd)
        CmdExecution = True
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeBuildInsert", Me)
        Set Cmd.Connection = Connection
        Cmd.CommandOperation = cmdExec
        Cmd.CommandType = dsTable
        Cmd.CommandParameters = Empty
        Dim IsDef_SiteName : IsDef_SiteName = CCIsDefined("SiteName", "Form")
        Dim IsDef_Address : IsDef_Address = CCIsDefined("Address", "Form")
        Dim IsDef_Latitud : IsDef_Latitud = CCIsDefined("Latitud", "Form")
        Dim IsDef_Longitud : IsDef_Longitud = CCIsDefined("Longitud", "Form")
        Dim IsDef_Tm_id : IsDef_Tm_id = CCIsDefined("Tm_id", "Form")
        Dim IsDef_Zip : IsDef_Zip = CCIsDefined("Zip", "Form")
        Dim IsDef_Phone : IsDef_Phone = CCIsDefined("Phone", "Form")
        Dim IsDef_Mail : IsDef_Mail = CCIsDefined("Mail", "Form")
        Dim IsDef_Valid : IsDef_Valid = CCIsDefined("Valid", "Form")
        Dim IsDef_Country_id : IsDef_Country_id = CCIsDefined("Country_id", "Form")
        Dim IsDef_City_id : IsDef_City_id = CCIsDefined("City_id", "Form")
        Dim IsDef_Site_id : IsDef_Site_id = CCIsDefined("Site_id", "Form")
        Dim IsDef_Service_id : IsDef_Service_id = CCIsDefined("Service_id", "Form")
        Dim IsDef_Brand_id : IsDef_Brand_id = CCIsDefined("Brand_id", "Form")
        Dim IsDef_Local_type_id : IsDef_Local_type_id = CCIsDefined("Local_type_id", "Form")
        Dim IsDef_Region_id : IsDef_Region_id = CCIsDefined("Region_id", "Form")
        If Not InsertOmitIfEmpty("SiteName") Or IsDef_SiteName Then Cmd.AddSQLStrings "[SiteName]", Connection.ToSQL(SiteName, SiteName.DataType)
        If Not InsertOmitIfEmpty("Address") Or IsDef_Address Then Cmd.AddSQLStrings "[Address]", Connection.ToSQL(Address, Address.DataType)
        If Not InsertOmitIfEmpty("Latitud") Or IsDef_Latitud Then Cmd.AddSQLStrings "[Latitud]", Connection.ToSQL(Latitud, Latitud.DataType)
        If Not InsertOmitIfEmpty("Longitud") Or IsDef_Longitud Then Cmd.AddSQLStrings "[Longitud]", Connection.ToSQL(Longitud, Longitud.DataType)
        If Not InsertOmitIfEmpty("Tm_id") Or IsDef_Tm_id Then Cmd.AddSQLStrings "[Tm_id]", Connection.ToSQL(Tm_id, Tm_id.DataType)
        If Not InsertOmitIfEmpty("Zip") Or IsDef_Zip Then Cmd.AddSQLStrings "[Zip]", Connection.ToSQL(Zip, Zip.DataType)
        If Not InsertOmitIfEmpty("Phone") Or IsDef_Phone Then Cmd.AddSQLStrings "[Phone]", Connection.ToSQL(Phone, Phone.DataType)
        If Not InsertOmitIfEmpty("Mail") Or IsDef_Mail Then Cmd.AddSQLStrings "[Mail]", Connection.ToSQL(Mail, Mail.DataType)
        If Not InsertOmitIfEmpty("Valid") Or IsDef_Valid Then Cmd.AddSQLStrings "[Valid]", Connection.ToSQL(Valid, Valid.DataType)
        If Not InsertOmitIfEmpty("Country_id") Or IsDef_Country_id Then Cmd.AddSQLStrings "[Country_id]", Connection.ToSQL(Country_id, Country_id.DataType)
        If Not InsertOmitIfEmpty("City_id") Or IsDef_City_id Then Cmd.AddSQLStrings "[City_id]", Connection.ToSQL(City_id, City_id.DataType)
        If Not InsertOmitIfEmpty("Site_id") Or IsDef_Site_id Then Cmd.AddSQLStrings "site_id", Connection.ToSQL(Site_id, Site_id.DataType)
        If Not InsertOmitIfEmpty("Service_id") Or IsDef_Service_id Then Cmd.AddSQLStrings "[Service_id]", Connection.ToSQL(Service_id, Service_id.DataType)
        If Not InsertOmitIfEmpty("Brand_id") Or IsDef_Brand_id Then Cmd.AddSQLStrings "[Brand_id]", Connection.ToSQL(Brand_id, Brand_id.DataType)
        If Not InsertOmitIfEmpty("Local_type_id") Or IsDef_Local_type_id Then Cmd.AddSQLStrings "[Local_type_id]", Connection.ToSQL(Local_type_id, Local_type_id.DataType)
        If Not InsertOmitIfEmpty("Region_id") Or IsDef_Region_id Then Cmd.AddSQLStrings "[Region_id]", Connection.ToSQL(Region_id, Region_id.DataType)
        CmdExecution = Cmd.PrepareSQL("Insert", "[Sites]", Empty)
        CCSEventResult = CCRaiseEvent(CCSEvents, "BeforeExecuteInsert", Me)
        If Errors.Count = 0  And CmdExecution Then
            Cmd.Exec(Errors)
            CCSEventResult = CCRaiseEvent(CCSEvents, "AfterExecuteInsert", Me)
        End If
    End Sub
'End Insert Method

End Class 'End SitesDataSource Class @2-A61BA892

'Include Page Implementation @22-CCAE622E
%>
<!-- #INCLUDE VIRTUAL="/Alpheton/Footer.asp" -->
<%
'End Include Page Implementation

'Include Page Implementation @23-F4BF1AC9
%>
<!-- #INCLUDE VIRTUAL="/Alpheton/Header.asp" -->
<%
'End Include Page Implementation


%>
