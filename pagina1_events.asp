<%
'BindEvents Method @1-7C8C0B75
Sub BindEvents(Level)
    If Level="Page" Then
        Set CCSEvents("BeforeShow") = GetRef("Page_BeforeShow")
    Else
    End If
End Sub
'End BindEvents Method

Function Page_BeforeShow(Sender) 'Page_BeforeShow @1-A1547E8B

'Custom Code @2-73254650
' -------------------------
dim FusionHO
Dim SQL
Dim rsGroups
Dim strGroups
Dim strXML

dim strCategories, strtank1, strtank2, strtank3, strtank4
dim i
dim var
dim var1
dim var2
dim b
dim fecha

b = false
strGroups = ""
strXML = ""
Set FusionHO = Server.CreateObject("ADODB.Connection") 
FusionHO.open "PROVIDER=SQLNCLI10;DATA SOURCE=MICROSTR-0A74B0;UID=sa;PWD=serbiznet;DATABASE=FusionHO " 
var = ""
var = CCGetRequestParam("site", ccsGet)
var2 = CCGetRequestParam("prod", ccsGet)

var1 = CCGetRequestParam("date", ccsGet)
fecha = mid(var1,7,4)&"-"& mid(var1,1,2)&"-"& mid(var1,4,2)


SQL = "select Product as tank, Hour, sum(round(volume,0,1)) as vol from StockByTank('"&var2&"', '" & var & "','"&fecha&"',0,23) group by Site_name, Product, Hour"

Dim arrData(24,5)
	
For i=0 to UBound(arrData)-1  
arrData(i,1) = i
arrData(i,2) = 0
arrData(i,3) = 0
arrData(i,4) = 0
arrData(i,5) = 0
next

 Set rsGroups = FusionHO.Execute(SQL)
While not rsGroups.EOF
	if rsGroups("tank") = "Normal" then
		arrData(rsGroups("hour"),2) = rsGroups("vol")
	end if
	if rsGroups("tank") = "Super" then
		arrData(rsGroups("hour"),3) = rsGroups("vol")
	end if
	if rsGroups("tank") = "Ultra" then
		arrData(rsGroups("hour"),4) = rsGroups("vol")
	end if 
	if rsGroups("tank") = "Diesel" then
		arrData(rsGroups("hour"),5) = rsGroups("vol")
	end if 
  rsGroups.MoveNext

'Initialize <graph> element
strXML = "<graph caption='      Stock By Product in " & var & "' subcaption='"&fecha&"              ' hovercapbg='FFECAA' chartBottomMargin='0' hovercapborder='F47E00' formatNumberScale='0' decimalPrecision='2' showvalues='0' numdivlines='5' numVdivlines='0' yaxisminvalue='0' yaxismaxvalue='7000'  rotateNames='0' xAxisName='Hours' yAxisName='Volume'>"

'Initialize <categories> element - necessary to generate a multi-series chart
strCategories = "<categories>"


	 
'Initiate <dataset> elements
strtank1 = "<dataset seriesName=' Normal' color='AF00F8' >"
strtank2 = "<dataset seriesName=' Super' color='F6000F' >"
strtank3 = "<dataset seriesName=' Ultra' color='AFBBF8' >"
strtank4 = "<dataset seriesName=' Diesel' color='F6CC0F' >"
 
dim d
dim m
'//////// armo el xml llenando los campos vacios hacia atras
 For i=0 to UBound(arrData)-1

		strCategories = strCategories & "<category name='" & arrData(i,1) & "' />"

		if arrData(i,2) = 0 then
			arrData(i,2) =  arrData(i + 1,2)
			strtank1 = strtank1 & "<set value='" & arrData(i,2) & "' />"
		else 
			strtank1 = strtank1 & "<set value='" & arrData(i,2) & "' />"
		end if
		
		if arrData(i,3) = 0 then
			arrData(i,3) =  arrData(i + 1,3)
			strtank2 = strtank2 & "<set value='" & arrData(i,3) & "' />"
		else 
			strtank2 = strtank2 & "<set value='" & arrData(i,3) & "' />"
		end if
		
		if arrData(i,4) = 0 then
			arrData(i,4) =  arrData(i + 1,4)
			strtank3 = strtank3 & "<set value='" & arrData(i,4) & "' />"
		else 
			strtank3 = strtank3 & "<set value='" & arrData(i,4) & "' />"
			
		end if
		

		if arrData(i,5) = 0 then
			arrData(i,5) =  arrData(i + 1,5)
			strtank4 = strtank4 & "<set value='" & arrData(i,5) & "' />"
		else 
			strtank4 = strtank4 & "<set value='" & arrData(i,5) & "' />"
		end if
   Next
 

  

Wend

   strCategories = strCategories & "</categories>"
   

   strtank1 = strtank1 & "</dataset>"
   strtank2 = strtank2 & "</dataset>"
   strtank3 = strtank3 & "</dataset>"
   strtank4 = strtank4 & "</dataset>"

   'Assemble the entire XML now
'   ///////////// definir esto cuando capturo los datos del form ////////////

Select Case (var2)
   Case "Normal":
       'Sentencias 
       strXML = strXML & strCategories & strtank1 & "</graph>"'
   Case "Super":
       'Sentencias 
       strXML = strXML & strCategories & strtank2 & "</graph>"'
   Case "Ultra":
       'Sentencias 
       strXML = strXML & strCategories & strtank3 & "</graph>"'
   Case "Diesel":
       'Sentencias 
       strXML = strXML & strCategories & strtank4 & "</graph>"'
   Case else:
       'Sentencias 
       strXML = strXML & strCategories & strtank1 & strtank2 & strtank3 & strtank4 & "</graph>"'
End Select

Response.ContentType = "text/xml"
'Just write out the XML data
'NOTE THAT THIS PAGE DOESN'T CONTAIN ANY HTML TAG, WHATSOEVER
 Response.Write(strXML)
'end if
' -------------------------
'End Custom Code

End Function 'Close Page_BeforeShow @1-54C34B28


%>
