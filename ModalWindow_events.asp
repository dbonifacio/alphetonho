<%
'BindEvents Method @1-807A894B
Sub BindEvents(Level)
    If Level="Page" Then
        Set CCSEvents("BeforeInitialize") = GetRef("Page_BeforeInitialize")
        Set CCSEvents("AfterInitialize") = GetRef("Page_AfterInitialize")
        Set CCSEvents("BeforeShow") = GetRef("Page_BeforeShow")
        Set CCSEvents("BeforeOutput") = GetRef("Page_BeforeOutput")
        Set CCSEvents("BeforeUnload") = GetRef("Page_BeforeUnload")
    Else
        Set Userss.CCSEvents("BeforeShow") = GetRef("Userss_BeforeShow")
        Set Userss.CCSEvents("OnValidate") = GetRef("Userss_OnValidate")
        Set Panel1.CCSEvents("BeforeShow") = GetRef("Panel1_BeforeShow")
        Set changePass.uid.CCSEvents("BeforeShow") = GetRef("changePass_uid_BeforeShow")
        Set changePass.CCSEvents("OnValidate") = GetRef("changePass_OnValidate")
        Set Panel2.CCSEvents("BeforeShow") = GetRef("Panel2_BeforeShow")
    End If
End Sub
'End BindEvents Method

Function Userss_BeforeShow(Sender) 'Userss_BeforeShow @20-BDA31283

'Custom Code @37-73254650
' -------------------------
   Userss.Reg.Visible = not Userss.EditMode
   Userss.Edit.Visible = Userss.EditMode

' -------------------------
'End Custom Code

End Function 'Close Userss_BeforeShow @20-54C34B28

Function Userss_OnValidate(Sender) 'Userss_OnValidate @20-96FEB515

'Custom Code @51-73254650
' -------------------------
If (not Userss.EditMode) Then
       If (Userss.user_password.Value <> Userss.cuser_password.Value) Then
           Userss.Errors.addError("Password and confirmation password doesn't match")
       End If
       If (Len(Userss.user_password.Value) < 5) Then
           Userss.Errors.addError("Password length must be more than 5 symbols")
       End If
End If
          
' -------------------------
'End Custom Code

End Function 'Close Userss_OnValidate @20-54C34B28

Function Panel1_BeforeShow(Sender) 'Panel1_BeforeShow @2-07703E06

'Panel1UpdatePanel Page BeforeShow @3-3C13129F
    If CCSFormFilter = "Panel1" Then
        Sender.BlockPrefix = ""
        Sender.BlockSuffix = ""
    Else
        Sender.BlockPrefix = "<div id=""Panel1"">"
        Sender.BlockSuffix = "</div>"
    End If
'End Panel1UpdatePanel Page BeforeShow

End Function 'Close Panel1_BeforeShow @2-54C34B28

Function changePass_uid_BeforeShow(Sender) 'changePass_uid_BeforeShow @64-90033A08

'Retrieve Value for Control @94-EF034534
    changePass.uid.Value = Request.QueryString("User_id")
'End Retrieve Value for Control

End Function 'Close changePass_uid_BeforeShow @64-54C34B28

Function changePass_OnValidate(Sender) 'changePass_OnValidate @54-982AFD27

'Custom Code @68-73254650
' -------------------------
If (changePass.cpass.Value <> changePass.npass.Value) Then
         changePass.Errors.addError("Password and confirmation password doesn't match")
   End If
     dim c
   c = CCDLookUp("COUNT(*)","Users","User_id="&DBFusionHO.ToSQL(changePass.uid.Value,ccsInteger)&" AND Password="&DBFusionHO.ToSQL(changePass.opass.Value,ccsText),DBFusionHO)
     If (c < 1) Then
         changePass.Errors.addError("Current password and entered password doesn't match")
   End If
   If (Len(changePass.npass.Value) < 5) Then
       changePass.Errors.AddError "Password length must be more than 5 symbols"
   End If

' -------------------------
'End Custom Code

End Function 'Close changePass_OnValidate @54-54C34B28

Function Panel2_BeforeShow(Sender) 'Panel2_BeforeShow @52-A8D973CC

'Panel2UpdatePanel Page BeforeShow @53-647B3B3B
    If CCSFormFilter = "Panel2" Then
        Sender.BlockPrefix = ""
        Sender.BlockSuffix = ""
    Else
        Sender.BlockPrefix = "<div id=""Panel2"">"
        Sender.BlockSuffix = "</div>"
    End If
'End Panel2UpdatePanel Page BeforeShow

End Function 'Close Panel2_BeforeShow @52-54C34B28

Function Page_BeforeInitialize(Sender) 'Page_BeforeInitialize @1-A73C9435

'RemoteCustomCode1 Initialization @85-9D36F6CB
    If "Panel1UserssUser_nameRemoteCustomCode1" = CCGetFromGet("callbackControl", Empty) Then
'End RemoteCustomCode1 Initialization

'RemoteCustomCode1 Displaying @85-73254650
        ' -------------------------
	Dim c
	Dim Conn
	Set Conn = New clsDBFusionHO
	Conn.Open
	c = CCDLookUp("COUNT(*)","Users","User_name="&Conn.ToSQL(CCGetFromPost("Login", Empty),ccsText),Conn)
	Response.Write(c)
        ' -------------------------
'End RemoteCustomCode1 Displaying

'RemoteCustomCode1 Tail @85-B3806ED1
        Response.End
    End If
'End RemoteCustomCode1 Tail

End Function 'Close Page_BeforeInitialize @1-54C34B28

Function Page_AfterInitialize(Sender) 'Page_AfterInitialize @1-5C791CCC

'Panel1UpdatePanel Page After Initialize @3-394FA2ED
    If CCGetFromGetFirst("FormFilter", Empty) = "Panel1" Then
        CCSFormFilter = CCGetFromGetFirst("FormFilter", Empty)
        CCAddGlobalRemoveParameter("FormFilter")
    End If
'End Panel1UpdatePanel Page After Initialize

'Panel2UpdatePanel Page After Initialize @53-39078E7B
    If CCGetFromGetFirst("FormFilter", Empty) = "Panel2" Then
        CCSFormFilter = CCGetFromGetFirst("FormFilter", Empty)
        CCAddGlobalRemoveParameter("FormFilter")
    End If
'End Panel2UpdatePanel Page After Initialize

End Function 'Close Page_AfterInitialize @1-54C34B28

Function Page_BeforeShow(Sender) 'Page_BeforeShow @1-A1547E8B

End Function 'Close Page_BeforeShow @1-54C34B28

Function Page_BeforeOutput(Sender) 'Page_BeforeOutput @1-FB5D75F3

'Panel1UpdatePanel PageBeforeOutput @3-FE9BC60F
    If CCSFormFilter = "Panel1" Then
        MainHTML = HTMLTemplate.GetHTML("main/Panel Panel1")
        Response.CacheControl = "no-cache"
        Response.AddHeader "Pragma", "no-cache"
        Response.Expires = -1
    End If
'End Panel1UpdatePanel PageBeforeOutput

'Panel2UpdatePanel PageBeforeOutput @53-D397FBA6
    If CCSFormFilter = "Panel2" Then
        MainHTML = HTMLTemplate.GetHTML("main/Panel Panel2")
        Response.CacheControl = "no-cache"
        Response.AddHeader "Pragma", "no-cache"
        Response.Expires = -1
    End If
'End Panel2UpdatePanel PageBeforeOutput

End Function 'Close Page_BeforeOutput @1-54C34B28

Function Page_BeforeUnload(Sender) 'Page_BeforeUnload @1-A9894291

'Panel1UpdatePanel PageBeforeUnload @3-950E4CB3
    If Len(Redirect) > 0 And  CCSFormFilter = "Panel1" Then
        Redirect = CCAddParam(Redirect, "FormFilter", CCSFormFilter)
    End If
'End Panel1UpdatePanel PageBeforeUnload

'Panel2UpdatePanel PageBeforeUnload @53-D9BD661A
    If Len(Redirect) > 0 And  CCSFormFilter = "Panel2" Then
        Redirect = CCAddParam(Redirect, "FormFilter", CCSFormFilter)
    End If
'End Panel2UpdatePanel PageBeforeUnload

End Function 'Close Page_BeforeUnload @1-54C34B28


%>
