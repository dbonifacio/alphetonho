<%
'BindEvents Method @1-4109569F
Sub BindEvents(Level)
    If Level="Page" Then
    Else
        Set Tm.Navigator.CCSEvents("BeforeShow") = GetRef("Tm_Navigator_BeforeShow")
        Set Tm1.CCSEvents("BeforeShow") = GetRef("Tm1_BeforeShow")
    End If
End Sub
'End BindEvents Method

Function Tm_Navigator_BeforeShow(Sender) 'Tm_Navigator_BeforeShow @24-8D362B02

'Hide-Show Component @25-F5F2348E
    Dim TotalPages_25_1 : TotalPages_25_1 = CCSConverter.VBSConvert(ccsInteger, Tm.DataSource.Recordset.PageCount)
    Dim Param2_25_2 : Param2_25_2 = CCSConverter.VBSConvert(ccsInteger, 2)
    If  (Not IsEmpty(TotalPages_25_1) And Not IsEmpty(Param2_25_2) And TotalPages_25_1 < Param2_25_2) Then _
        Tm.Navigator.Visible = False
'End Hide-Show Component

End Function 'Close Tm_Navigator_BeforeShow @24-54C34B28

Function Tm1_BeforeShow(Sender) 'Tm1_BeforeShow @30-3918DB8A

'Custom Code @40-73254650
' -------------------------
  	If Tm1.Recordset.EOF Then
		Tm1.Visible = False
 	End if

 	If(CCGetFromGet("var","") = "1") Then
 		Tm1.Visible = true
	end if
' -------------------------
'End Custom Code

End Function 'Close Tm1_BeforeShow @30-54C34B28


%>
