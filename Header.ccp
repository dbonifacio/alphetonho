<Page id="1" templateExtension="html" relativePath="." fullRelativePath="." secured="False" urlType="Relative" isIncluded="True" SSLAccess="False" isService="False" cachingEnabled="False" cachingDuration="1 minutes" wizardTheme="FusionHO1" wizardThemeVersion="3.0" needGeneration="0" pasteActions="pasteActions">
	<Components>
		<ImageLink id="43" visible="Yes" fieldSourceType="DBColumn" dataType="Text" html="False" hrefType="Page" urlType="Relative" preserveParameters="GET" name="Logout" hrefSource="Login.ccp" wizardDefaultValue="{res:CCS_LogoutBtn}" PathID="HeaderLogout">
			<Components/>
			<Events/>
			<LinkParameters>
				<LinkParameter id="44" sourceType="Expression" format="yyyy-mm-dd" name="Logout" source="&quot;True&quot;"/>
			</LinkParameters>
			<Attributes/>
			<Features/>
		</ImageLink>
		<Menu id="2" secured="False" sourceType="Table" returnValueType="Number" name="Menu1" menuType="Horizontal" menuSourceType="Static" PathID="HeaderMenu1">
			<Components>
				<Link id="3" visible="Yes" fieldSourceType="DBColumn" dataType="Text" html="False" hrefType="Page" urlType="Relative" preserveParameters="GET" name="ItemLink" PathID="HeaderMenu1ItemLink">
					<Components/>
					<Events/>
					<LinkParameters/>
					<Attributes/>
					<Features/>
				</Link>
			</Components>
			<Events/>
			<TableParameters/>
			<JoinTables/>
			<JoinLinks/>
			<Fields/>
			<SPParameters/>
			<SQLParameters/>
			<SecurityGroups/>
			<Attributes/>
			<MenuItems>
				<MenuItem id="86" name="MenuItem1" caption="Configuration" title="{res:Configuration}"/>
<MenuItem id="87" name="MenuItem1Item3" parent="MenuItem1" caption="City" url="City_list.ccp" target="_self" title="{res:City}"/>
<MenuItem id="88" name="MenuItem1Item4" parent="MenuItem1" caption="Brand" url="Brand_list.ccp" target="_self" title="{res:Brand}"/>
<MenuItem id="89" name="MenuItem1Item5" parent="MenuItem1" caption="Country" url="Country_list.ccp" target="_self" title="{res:Country}"/>
<MenuItem id="90" name="MenuItem1Item6" parent="MenuItem1" caption="Grades" url="Grades_list.ccp" target="_self" title="{res:Grades}"/>
<MenuItem id="91" name="MenuItem1Item7" parent="MenuItem1" caption="Grade_type" url="Grade_type_list.ccp" target="_self" title="{res:Grade_type}"/>
<MenuItem id="92" name="MenuItem1Item8" parent="MenuItem1" caption="Local_type" url="Local_type_list.ccp" target="_self" title="{res:Local_type}"/>
<MenuItem id="93" name="MenuItem1Item9" parent="MenuItem1" caption="Moso" url="Moso_list.ccp" target="_self" title="{res:Moso}"/>
<MenuItem id="94" name="MenuItem1Item10" parent="MenuItem1" caption="Region" url="Region_list.ccp" target="_self" title="{res:Region}"/>
<MenuItem id="95" name="MenuItem1Item11" parent="MenuItem1" caption="Service" url="Service_list.ccp" target="_self" title="{res:Service}"/>
<MenuItem id="96" name="MenuItem1Item12" parent="MenuItem1" caption="Sites" url="Sites_list.ccp" target="_self" title="{res:Sites}"/>
<MenuItem id="97" name="MenuItem1Item13" parent="MenuItem1" caption="Territory Manager" url="Tm_list.ccp" target="_self"/>
<MenuItem id="98" name="MenuItem2" caption="Reports" title="{res:Reports}"/>
<MenuItem id="99" name="MenuItem2Item1" parent="MenuItem2" caption="Admin_Report" title="{res:Admin_Report}"/>
<MenuItem id="100" name="MenuItem2Item1Item1" parent="MenuItem2Item1" caption="Grade_Report" url="Report_Grade.ccp" target="_self"/>
<MenuItem id="101" name="MenuItem2Item2" parent="MenuItem2" caption="Sales_Reports" title="{res:Sales_Reports}"/>
<MenuItem id="102" name="MenuItem2Item2Item1" parent="MenuItem2Item2" caption="Sales_By_Sites" url="ReportSalesBySites.ccp" target="_self" title="{res:Sales_By_Sites}"/>
<MenuItem id="103" name="MenuItem2Item2Item2" parent="MenuItem2Item2" caption="Sales_By_Grades" url="ReportSalesByGrades.ccp" target="_self" title="{res:Sales_By_Grades}"/>
<MenuItem id="104" name="MenuItem2Item2Item5" parent="MenuItem2Item2" caption="Sales_By_Payments" url="ReportSalesByPay.ccp" target="_self" title="{res:Sales_By_Payments}"/>
<MenuItem id="105" name="MenuItem2Item2Item6" parent="MenuItem2Item2" caption="Sales_By_Time" url="ReportSalesByTime.ccp" target="_self" title="{res:Sales_By_Time}"/>
<MenuItem id="106" name="MenuItem2Item3" parent="MenuItem2" caption="Tank_Reports" title="{res:Tank_Reports}"/>
<MenuItem id="107" name="MenuItem2Item3Item1" parent="MenuItem2Item3" caption="Stock_By_Tank" title="{res:Stock_By_Tank}"/>
<MenuItem id="108" name="MenuItem2Item3Item1Item1" parent="MenuItem2Item3Item1" caption="Stock_By_Date" url="ReportStockByTanks.ccp" target="_self" title="{res:Stock_By_Date}"/>
<MenuItem id="109" name="MenuItem2Item3Item1Item2" parent="MenuItem2Item3Item1" caption="Stock_By_Range_Date" url="ReportStockTankRangeDate.ccp" target="_self" title="{res:Stock_By_Range_Date}"/>
<MenuItem id="110" name="MenuItem2Item3Item2" parent="MenuItem2Item3" caption="Stock_By_Product" title="{res:Stock_By_Product}"/>
<MenuItem id="111" name="MenuItem2Item3Item2Item1" parent="MenuItem2Item3Item2" caption="Stock_ By_Date" url="ReportStockbyGrade.ccp" target="_self" title="{res:Stock_By_Date}"/>
<MenuItem id="112" name="MenuItem2Item3Item2Item2" parent="MenuItem2Item3Item2" caption="Stock_ By_Range_Date" url="ReportStockByGradeRangeDate.ccp" title="{res:Stock_By_Range_Date}"/>
<MenuItem id="113" name="MenuItem2Item3Item3" parent="MenuItem2Item3" caption="Alarm_History" url="ReportByAlarms.ccp" target="_self" title="{res:Alarm_History}"/>
<MenuItem id="114" name="MenuItem2Item3Item4" parent="MenuItem2Item3" caption="Alarm_Deliveries" url="ReportByDelivery.ccp" target="_self" title="{res:Alarm_Deliveries}"/>
<MenuItem id="115" name="MenuItem2Item4" parent="MenuItem2" caption="Graph" title="{res:Graph}"/>
<MenuItem id="116" name="MenuItem2Item4Item1" parent="MenuItem2Item4" caption="Stock_By_Tank" title="{res:Stock_By_Tank}"/>
<MenuItem id="117" name="MenuItem2Item4Item1Item1" parent="MenuItem2Item4Item1" caption="Stock By Date" url="ChartByTank.ccp" target="_self" title="{res:Stock_By_Date}"/>
<MenuItem id="118" name="MenuItem2Item4Item1Item2" parent="MenuItem2Item4Item1" caption="Stock By Range Date" url="ChartByTankRangeDate.ccp" target="_self" title="{res:Stock_By_Range_Date}"/>
<MenuItem id="119" name="MenuItem2Item4Item2" parent="MenuItem2Item4" caption="Stock_By_Product" title="{res:Stock_By_Product}"/>
<MenuItem id="120" name="MenuItem2Item4Item2Item1" parent="MenuItem2Item4Item2" caption="Stock By Date" url="ChartByProd.ccp" target="_self" title="{res:Stock_By_Date}"/>
<MenuItem id="121" name="MenuItem2Item4Item2Item2" parent="MenuItem2Item4Item2" caption="Stock By Range Date" url="ChartByGradeRangeDate.ccp" target="_self" title="{res:Stock_By_Range_Date}"/>
<MenuItem id="122" name="MenuItem2Item4Item3" parent="MenuItem2Item4" caption="Sales_By_Time" url="ChartByTime.ccp" target="_self" title="{res:Sales_By_Time}"/>
<MenuItem id="123" name="MenuItem2Item4Item4" parent="MenuItem2Item4" caption="Sales_By_Grades" url="ChartSalesByGrades.ccp" target="_self" title="{res:Sales_By_Grades}"/>
<MenuItem id="124" name="MenuItem2Item5" parent="MenuItem2" caption="Log" title="{res:Log}"/>
<MenuItem id="125" name="MenuItem2Item5Item1" parent="MenuItem2Item5" caption="Addin Flow Control Data" url="ReportByLogFLW.ccp" target="_self"/>
<MenuItem id="126" name="MenuItem2Item5Item2" parent="MenuItem2Item5" caption="Addin Payment Data" url="ReportByLogPAY.ccp" target="_self"/>
<MenuItem id="127" name="MenuItem2Item5Item3" parent="MenuItem2Item5" caption="Pump_Sales" url="ReportByLogSLS.ccp" target="_self"/>
<MenuItem id="128" name="MenuItem2Item5Item4" parent="MenuItem2Item5" caption="Tank Alarms History" url="ReportByLogTAH.ccp" target="_self"/>
<MenuItem id="129" name="MenuItem2Item5Item5" parent="MenuItem2Item5" caption="Tank Actual Info" url="ReportByLogTAI.ccp" target="_self"/>
<MenuItem id="130" name="MenuItem2Item5Item6" parent="MenuItem2Item5" caption="Tank Deliveries" url="ReportByLogTDD.ccp" target="_self"/>
<MenuItem id="131" name="MenuItem2Item5Item7" parent="MenuItem2Item5" caption="Tank Day Sales" url="ReportByLogTDS.ccp" target="_self"/>
<MenuItem id="132" name="MenuItem4" caption="Prices" title="{res:Prices}"/>
<MenuItem id="133" name="MenuItem4Item1" parent="MenuItem4" caption="price_send" url="Price_send.ccp" target="_self" title="{res:price_send}"/>
<MenuItem id="134" name="MenuItem3" caption="Security" title="{res:Security}"/>
<MenuItem id="135" name="MenuItem3Item1" parent="MenuItem3" caption="Users" url="Users_list.ccp" target="_self" title="{res:Users}"/>
<MenuItem id="136" name="MenuItem3Item2" parent="MenuItem3" caption="Change_password" url="change.ccp" target="_self" title="{res:Change_password}"/>
</MenuItems>
			<Features/>
		</Menu>
		<Label id="62" visible="Yes" fieldSourceType="DBColumn" dataType="Text" name="usuario" PathID="Headerusuario" html="False">
			<Components/>
			<Events>
				<Event name="BeforeShow" type="Server">
					<Actions>
						<Action actionName="Custom Code" actionCategory="General" id="63" eventType="Server"/>
					</Actions>
				</Event>
			</Events>
			<Attributes/>
			<Features/>
		</Label>
	</Components>
	<CodeFiles>
		<CodeFile id="Code" language="ASPTemplates" name="Header.asp" forShow="True" url="Header.asp" comment="'" codePage="windows-1252"/>
		<CodeFile id="Events" language="ASPTemplates" name="Header_events.asp" forShow="False" comment="'" codePage="windows-1252"/>
	</CodeFiles>
	<SecurityGroups/>
	<CachingParameters/>
	<Attributes/>
	<Features/>
	<Events>
		<Event name="BeforeShow" type="Server">
			<Actions>
				<Action actionName="Hide-Show Component" actionCategory="General" id="85" action="Hide" conditionType="Parameter" dataType="Text" condition="Equal" parameter1="Print" name1="ViewMode" sourceType1="URL" name2="&quot;Print&quot;" sourceType2="Expression"/>
			</Actions>
		</Event>
	</Events>
</Page>
